# SollentoAcademy

## Installation

You need to use docker-compose for deploy project

```bash
docker-compose -f docker_sollentoAcademy/docker-compose.yml up -d --build
```

```bash
docker exec -it sollentoAcademy_php-fpm bash
```

```bash
composer install
```

```bash
rm -r var/cache
```

### For development mode:

#### If database doesn't exist (for a test environment, run with the flag --env=test)

#### If the vendor directory is not mounted you need to be able to copy the vendor from the container to your local machine

```bash
docker cp <containerId>:/var/www/sollentoAcademy/vendor . 
```

```bash
bin/console doctrine:database:create && bin/console doctrine:database:create --env=test
```

#### Run/update database dump

```bash
bin/console d:s:u --force && bin/console d:s:u --force --env=test
```

#### Generate keypair for LexikJWT

```bash
bin/console lexik:jwt:generate-keypair
```

#### Create superadmin

```bash
bin/console superadmin:create
```

#### Run all tests with phpunit
```bash
./vendor/bin/phpunit
```

## Quality tools

Run all quality tools:

```bash
composer check-all
```

Run ecs:

```bash
composer check-ecs
```
