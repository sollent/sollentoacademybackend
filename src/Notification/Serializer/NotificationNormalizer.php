<?php
declare(strict_types=1);

namespace App\Notification\Serializer;

use App\Notification\ApiResource\Notification;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

final class NotificationNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    /**
     * @var string
     */
    private const ALREADY_CALLED = 'NOTIFICATION_NORMALIZER_ALREADY_CALLED';

    /**
     * @var \App\Notification\Hydrator\NotificationHydratorInterface[]
     */
    private array $hydrators;

    /**
     * @param \App\Notification\Hydrator\NotificationHydratorInterface[] $hydrators
     */
    public function __construct(array $hydrators)
    {
        $this->hydrators = $hydrators;
    }

    /**
     * @param \App\Notification\ApiResource\Notification $object
     * @param mixed[]|null $context
     *
     * @return mixed[]
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function normalize($object, ?string $format = null, ?array $context = null): array
    {
        $context[self::ALREADY_CALLED] = true;

        foreach ($this->hydrators as $hydrator) {
            $hydrator->hydrate($object);
        }

        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param object $data
     * @param mixed[]|null $context
     *
     * @return bool
     */
    public function supportsNormalization($data, ?string $format = null, ?array $context = null): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof Notification;
    }
}
