<?php
declare(strict_types=1);

namespace App\Notification\Service;

use App\Notification\ApiResource\Notification;
use Carbon\Carbon;

final class NotificationService implements NotificationServiceInterface
{
    public function read(Notification $notification): void
    {
        if ($notification->isStatusViewed()) {
            return;
        }

        $notification
            ->setStatus(Notification::STATUS_VIEWED)
            ->setViewedAt(Carbon::now());
    }
}
