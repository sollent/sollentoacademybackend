<?php
declare(strict_types=1);

namespace App\Notification\Service;

use App\Notification\ApiResource\Notification;

interface NotificationServiceInterface
{
    public function read(Notification $notification): void;
}
