<?php
declare(strict_types=1);

namespace App\Notification\Repository;

use App\Common\Repository\AbstractRepository;
use App\Notification\ApiResource\Notification;

final class NotificationRepository extends AbstractRepository implements NotificationRepositoryInterface
{
    public function findByIds(array $ids, ?string $receiverId = null): array
    {
        $queryBuilder = $this->createQueryBuilder('n');

        if (\count($ids) > 0) {
            $queryBuilder
                ->where($queryBuilder->expr()->in('n.id', ':ids'))
                ->setParameter('ids', $ids);
        }

        if ($receiverId !== null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq('n.receiver', ':receiverId'))
                ->setParameter('receiverId', $receiverId);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    protected function getEntityClass(): string
    {
        return Notification::class;
    }
}
