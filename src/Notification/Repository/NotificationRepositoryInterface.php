<?php
declare(strict_types=1);

namespace App\Notification\Repository;

interface NotificationRepositoryInterface
{
    /**
     * @param string[] $ids
     *
     * @return \App\Notification\ApiResource\Notification[]
     */
    public function findByIds(array $ids, ?string $receiverId = null): array;
}
