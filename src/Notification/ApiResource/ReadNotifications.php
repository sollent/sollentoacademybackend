<?php
declare(strict_types=1);

namespace App\Notification\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\ApiPlatform\NoIriItemInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     shortName="Notifications",
 *     collectionOperations={
 *          "post"={
 *              "path": "/notifications/read",
 *              "openapi_context"={
 *                  "summary"="Mark notifications as read",
 *                  "description"="Mark notifications as read",
 *                  "responses"={
 *                      "200"={"description"="Passed notifications will be marked as read"},
 *                      "400"={"description"="Invalid input"},
 *                      "404"={"description"="Some notification was not found for the current user"}
 *                  }
 *              },
 *              "status"=200,
 *          },
 *     },
 *     itemOperations={},
 *     denormalizationContext={"groups"={"read-notifications:write"}}
 * )
 */
final class ReadNotifications implements NoIriItemInterface
{
    /**
     * @Assert\NotBlank
     * @Assert\Type("array")
     *
     * @Groups({"read-notifications:write"})
     *
     * @var string[]
     */
    private array $ids;

    /**
     * @return string[]
     */
    public function getIds(): array
    {
        return $this->ids;
    }

    /**
     * @param string[] $ids
     */
    public function setIds(array $ids): self
    {
        $this->ids = $ids;

        return $this;
    }
}
