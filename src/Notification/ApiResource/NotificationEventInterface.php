<?php
declare(strict_types=1);

namespace App\Notification\ApiResource;

interface NotificationEventInterface
{
    /**
     * @var string[]
     */
    public const ALL_EVENTS = [
        self::EVENT_ARTICLE_ADDED,
        self::EVENT_CONNECTION_REQUEST_APPROVED,
        self::EVENT_NEW_APPOINTMENT,
        self::EVENT_NEW_CONNECTION_REQUEST,
        self::EVENT_PROFILE_PUBLISHED,
    ];

    /**
     * @var string
     */
    public const EVENT_ARTICLE_ADDED = 'article_added';

    /**
     * @var string
     */
    public const EVENT_CONNECTION_REQUEST_APPROVED = 'connection_request_approved';

    /**
     * @var string
     */
    public const EVENT_NEW_APPOINTMENT = 'new_appointment';

    /**
     * @var string
     */
    public const EVENT_NEW_CONNECTION_REQUEST = 'new_connection_request';

    /**
     * @var string
     */
    public const EVENT_PROFILE_PUBLISHED = 'profile_published';
}
