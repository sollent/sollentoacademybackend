<?php
declare(strict_types=1);

namespace App\Notification\ApiResource;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use App\Common\ApiResource\AbstractEntity;
use App\Common\ApiResource\User;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiFilter(OrderFilter::class, properties={"createdAt"})
 *
 * @ApiResource(
 *     shortName="Notifications",
 *     collectionOperations={
 *          "get"={
 *              "access_control"="is_granted('ROLE_SUPER_ADMIN')"
 *          }
 *     },
 *     itemOperations={
 *          "get"={
 *              "access_control"="is_granted('ROLE_SUPER_ADMIN')"
 *          }
 *     },
 *     normalizationContext={
 *          "groups"={"created_at:read", "id-readable", "notification:read"}
 *     },
 *     denormalizationContext={"groups"={"empty"}}
 * )
 *
 * @ORM\Entity
 * @ORM\Table(
 *     name="notifications",
 *     indexes={@ORM\Index(columns={"created_at"})},
 * )
 */
class Notification extends AbstractEntity
{
    /**
     * @var string[]
     */
    public const ALL_STATUSES = [
        self::STATUS_FAILED,
        self::STATUS_NEW,
        self::STATUS_SENT,
        self::STATUS_VIEWED,
    ];

    /**
     * @var string
     */
    public const STATUS_FAILED = 'failed';

    /**
     * @var string
     */
    public const STATUS_NEW = 'new';

    /**
     * @var string
     */
    public const STATUS_SENT = 'sent';

    /**
     * @var string
     */
    public const STATUS_VIEWED = 'viewed';

    /**
     * @Groups({"notification:read"})
     */
    private ?string $beautyCreatedAt = null;

    /**
     * @ORM\Column(type="string")
     */
    private string $event;

    /**
     * @ORM\Column(type="json")
     *
     * @var mixed[]
     */
    private array $eventData = [];

    /**
     * @ORM\Column(type="string", length=2)
     */
    private string $locale;

    /**
     * @Groups({"notification:read"})
     *
     * @ORM\Column(type="text", length=255)
     */
    private string $message;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private User $receiver;

    /**
     * @ORM\Column(type="string")
     */
    private string $status = self::STATUS_NEW;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?DateTimeInterface $viewedAt = null;

    /**
     * @return string|null
     */
    public function getBeautyCreatedAt(): ?string
    {
        return $this->beautyCreatedAt;
    }

    /**
     * @param string|null $beautyCreatedAt
     *
     * @return \App\Notification\ApiResource\Notification
     */
    public function setBeautyCreatedAt(?string $beautyCreatedAt = null): Notification
    {
        $this->beautyCreatedAt = $beautyCreatedAt;

        return $this;
    }

    public function getEvent(): string
    {
        return $this->event;
    }

    public function getEventData(): array
    {
        return $this->eventData;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getReceiver(): User
    {
        return $this->receiver;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getViewedAt(): ?DateTimeInterface
    {
        return $this->viewedAt;
    }

    public function isStatusViewed(): bool
    {
        return $this->status === self::STATUS_VIEWED;
    }

    public function setEvent(string $event): self
    {
        $this->event = $event;

        return $this;
    }

    /**
     * @param mixed[] $eventData
     */
    public function setEventData(array $eventData): self
    {
        $this->eventData = $eventData;

        return $this;
    }

    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function setReceiver(User $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function setViewedAt(?DateTimeInterface $viewedAt = null): self
    {
        $this->viewedAt = $viewedAt;

        return $this;
    }
}
