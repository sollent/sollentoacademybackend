<?php
declare(strict_types=1);

namespace App\Notification\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\ApiPlatform\NoIriItemInterface;

/**
 * @ApiResource(
 *     shortName="Notifications",
 *     collectionOperations={
 *          "readAll"={
 *              "method": "post",
 *              "path": "/notifications/read-all",
 *              "openapi_context"={
 *                  "summary"="Mark all user notifications as read",
 *                  "description"="Mark all user notifications as read",
 *                  "responses"={
 *                      "200"={"description"="All user notifications will be marked as read"},
 *                  }
 *              },
 *              "status"=200,
 *          }
 *     },
 *     itemOperations={},
 *     denormalizationContext={"groups"={"read-all-notifications:write"}}
 * )
 */
final class ReadAllNotifications implements NoIriItemInterface
{
    // No body needed for now
}
