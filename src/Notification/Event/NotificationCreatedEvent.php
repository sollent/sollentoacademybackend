<?php
declare(strict_types=1);

namespace App\Notification\Event;

use App\Notification\ApiResource\Notification;
use Symfony\Contracts\EventDispatcher\Event;

final class NotificationCreatedEvent extends Event
{
    /**
     * @var string
     */
    public const NAME = 'notification.created';

    private Notification $notification;

    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    public function getNotification(): Notification
    {
        return $this->notification;
    }
}
