<?php
declare(strict_types=1);

namespace App\Notification\Subscriber;

use App\Notification\Event\NotificationCreatedEvent;
use App\Notification\Service\NotificationServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class NotificationCreatedSubscriber implements EventSubscriberInterface
{
    private NotificationServiceInterface $notificationService;

    public function __construct(NotificationServiceInterface $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            NotificationCreatedEvent::NAME => 'onNotificationCreated',
        ];
    }

    public function onNotificationCreated(NotificationCreatedEvent $event): void
    {
        // No body needed for now
    }
}
