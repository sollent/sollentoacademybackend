<?php
declare(strict_types=1);

namespace App\Notification\Subscriber;

use App\Notification\Event\ReadNotificationEvent;
use App\Notification\Service\NotificationServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

final class ReadNotificationSubscriber implements EventSubscriberInterface
{
    private NotificationServiceInterface $notificationService;

    public function __construct(NotificationServiceInterface $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            ReadNotificationEvent::NAME => 'onReadNotification',
        ];
    }

    public function onReadNotification(ReadNotificationEvent $event): void
    {
        $this->notificationService->read($event->getNotification());
    }
}
