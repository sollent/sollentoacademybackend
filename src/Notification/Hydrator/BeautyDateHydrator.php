<?php
declare(strict_types=1);

namespace App\Notification\Hydrator;

use App\Notification\ApiResource\Notification;
use Carbon\Carbon;
use Knp\Bundle\TimeBundle\DateTimeFormatter;

final class BeautyDateHydrator implements NotificationHydratorInterface
{
    private DateTimeFormatter $dateTimeFormatter;

    public function __construct(DateTimeFormatter $dateTimeFormatter)
    {
        $this->dateTimeFormatter = $dateTimeFormatter;
    }

    /**
     * {@inheritDoc}
     */
    public function hydrate(Notification $notification): Notification
    {
        return $notification->setBeautyCreatedAt(
            $this->dateTimeFormatter->formatDiff($notification->getCreatedAt(), Carbon::now())
        );
    }
}
