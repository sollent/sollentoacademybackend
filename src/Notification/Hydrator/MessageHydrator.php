<?php
declare(strict_types=1);

namespace App\Notification\Hydrator;

use App\Common\Service\LocaleResolverInterface;
use App\Notification\ApiResource\Notification;
use Symfony\Contracts\Translation\TranslatorInterface;

final class MessageHydrator implements NotificationHydratorInterface
{
    private LocaleResolverInterface $localeResolver;

    private TranslatorInterface $translator;

    public function __construct(LocaleResolverInterface $localeResolver, TranslatorInterface $translator)
    {
        $this->localeResolver = $localeResolver;
        $this->translator = $translator;
    }

    /**
     * {@inheritDoc}
     */
    public function hydrate(Notification $notification): Notification
    {
        if ($this->localeResolver->getCurrentLocale() !== $notification->getLocale()) {
            $messageKey = \sprintf('notifications.messages.%s', $notification->getEvent());
            $message = $this->translator->trans($messageKey, $notification->getEventData());

            $notification->setMessage($message);
        }

        return $notification;
    }
}
