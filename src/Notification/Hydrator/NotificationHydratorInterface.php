<?php
declare(strict_types=1);

namespace App\Notification\Hydrator;

use App\Notification\ApiResource\Notification;

interface NotificationHydratorInterface
{
    /**
     * @param \App\Notification\ApiResource\Notification $notification
     *
     * @return \App\Notification\ApiResource\Notification
     */
    public function hydrate(Notification $notification): Notification;
}
