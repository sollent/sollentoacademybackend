<?php
declare(strict_types=1);

namespace App\Notification\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\Common\Security\RolesInterface;
use App\Common\Security\SecurityContextInterface;
use App\Common\Traits\EntityManagerAwareTrait;
use App\Notification\ApiResource\ReadAllNotifications;
use App\Notification\Repository\NotificationRepositoryInterface;
use App\Notification\Service\NotificationServiceInterface;

final class ReadAllNotificationsDataPersister implements ContextAwareDataPersisterInterface
{
    use EntityManagerAwareTrait;

    private NotificationRepositoryInterface $notificationRepository;

    private NotificationServiceInterface $notificationService;

    private SecurityContextInterface $securityContext;

    public function __construct(
        NotificationRepositoryInterface $notificationRepository,
        NotificationServiceInterface $notificationService,
        SecurityContextInterface $securityContext
    ) {
        $this->notificationRepository = $notificationRepository;
        $this->notificationService = $notificationService;
        $this->securityContext = $securityContext;
    }

    /**
     * @param \App\Notification\ApiResource\ReadNotifications $data
     * @param mixed[]|null $context
     */
    public function persist($data, ?array $context = null): void
    {
        $user = $this->securityContext->getUserOrFail();
        $receiverId = $user->hasRole(RolesInterface::ROLE_SUPER_ADMIN) ? null : $user->getId();

        $notifications = $this->notificationRepository->findByIds([], $receiverId);

        foreach ($notifications as $notification) {
            $this->notificationService->read($notification);
        }

        $this->entityManager->flush();
    }

    /**
     * @param \App\Notification\ApiResource\ReadNotifications $data
     * @param mixed[]|null $context
     */
    public function remove($data, ?array $context = null): void
    {
        // Not supported
    }

    /**
     * {@inheritdoc}
     */
    public function supports($data, ?array $context = null): bool
    {
        return $data instanceof ReadAllNotifications;
    }
}
