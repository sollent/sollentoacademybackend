<?php
declare(strict_types=1);

namespace App\Manager\ApiResource;

use App\Common\ApiResource\User;
use App\UserCommon\ApiResource\Profile;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Manager
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get"
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"access_control"="(is_granted('ROLE_MANAGER') and object == user) or is_granted('ROLE_SUPER_ADMIN')"},
 *          "delete"={"access_control"="(is_granted('ROLE_MANAGER') and object == user) or is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     normalizationContext={
 *          "groups"={
 *              "manager:read",
 *              "user:read",
 *              "media_object_read",
 *              "id-readable"
 *          },
 *          "enable_max_depth"="true"
 *     },
 *     denormalizationContext={"groups"={"manager:write"}}
 * )
 *
 * @ORM\Entity
 */
class Manager extends User
{
    /**
     * @Groups({
     *      "manager:read",
     *      "manager:write",
     *      "user:read"
     * })
     *
     * @ORM\OneToOne(targetEntity="App\UserCommon\ApiResource\Profile", cascade={"persist", "remove"})
     */
    protected ?Profile $profile = null;
}