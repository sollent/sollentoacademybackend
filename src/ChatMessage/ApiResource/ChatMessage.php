<?php

declare(strict_types=1);

namespace App\ChatMessage\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Chat\ApiResource\Chat;
use App\Common\ApiResource\AbstractEntity;
use App\Common\ApiResource\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get",
 *         "post",
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"access_control"="is_granted('ROLE_CLIENT') or is_granted('ROLE_MANAGER') or is_granted('ROLE_SUPER_ADMIN')"},
 *          "delete"
 *     },
 *     normalizationContext={"groups"={"chat-message:read"}},
 *     denormalizationContext={"groups"={"chat-message:write"}}
 * )
 *
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="chat_message_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "chat"="App\ChatMessage\ApiResource\ChatMessage",
 *     "chat_text_message"="App\ChatMessage\ApiResource\ChatTextMessage"
 * })
 */
class ChatMessage extends AbstractEntity
{
    /**
     * @Groups({
     *     "chat:read",
     *     "chat-message:read",
     *     "chat-message:write",
     *     "client:read",
     *     "manager:read",
     *     "user:read",
     * })
     *
     * @ORM\ManyToOne(targetEntity="App\Common\ApiResource\User")
     */
    private User $sender;

    /**
     * @Groups({
     *     "chat:read",
     *     "chat-message:read",
     *     "chat-message:write",
     *     "chat-text-message:read",
     *     "chat-text-message:write",
     *     "client:read",
     *     "manager:read",
     *     "user:read",
     * })
     *
     * @ORM\ManyToOne(targetEntity="App\Common\ApiResource\User")
     */
    private User $receiver;

    /**
     * @Groups({
     *      "chat:read",
     *      "chat-message:read",
     *      "chat-message:write",
     *      "client:read",
     *      "manager:read",
     *      "user:read",
     * })
     *
     * @ORM\ManyToOne(targetEntity="App\Chat\ApiResource\Chat")
     */
    private ?Chat $chat = null;

    public function getSender(): User
    {
        return $this->sender;
    }

    public function setSender(User $sender): void
    {
        $this->sender = $sender;
    }

    public function getReceiver(): User
    {
        return $this->receiver;
    }

    public function setReceiver(User $receiver): void
    {
        $this->receiver = $receiver;
    }

    public function getChat(): ?Chat
    {
        return $this->chat;
    }

    public function setChat(?Chat $chat): void
    {
        $this->chat = $chat;
    }
}