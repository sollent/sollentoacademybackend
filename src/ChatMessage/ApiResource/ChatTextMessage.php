<?php

declare(strict_types=1);

namespace App\ChatMessage\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class ChatTextMessage
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"},
 *          "delete"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     normalizationContext={
 *          "groups"={
 *              "chat-text-message:read",
 *              "user:read",
 *              "media_object_read",
 *              "id-readable"
 *          },
 *          "enable_max_depth"="true"
 *     },
 *     denormalizationContext={"groups"={"chat-text-message:write"}}
 * )
 *
 * @ORM\Entity
 */
class ChatTextMessage extends ChatMessage
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @Groups({
     *     "chat-text-message:read",
     *     "chat-text-message:write",
     *     "chat-message:read",
     *     "chat:read"
     * })
     *
     * @ORM\Column(type="text")
     */
    private string $text;

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}