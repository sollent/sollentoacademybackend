<?php

declare(strict_types=1);

namespace App\ChatMessage\Event;

interface WebsocketEventInterface
{
    public function getData();
}