<?php

declare(strict_types=1);

namespace App\ChatMessage\Event\Chat;

use App\Chat\ApiResource\Chat;
use App\ChatMessage\ApiResource\ChatMessage;
use App\ChatMessage\Event\WebsocketEventInterface;
use Symfony\Contracts\EventDispatcher\Event;

final class ChatMessageEvent extends Event implements WebsocketEventInterface
{
    public const NAME = 'chat.message';

    private ChatMessage $chatMessage;

    private Chat $chat;

    /**
     * @param ChatMessage $chatMessage
     * @param Chat $chat
     *
     * @throws \Exception
     */
    public function __construct(
        ChatMessage $chatMessage,
        Chat $chat
    )
    {
        if ($chatMessage->getChat() !== $chat) {
            throw new \Exception('provided chatMessage not message from provided chat');
        }

        $this->chatMessage = $chatMessage;
        $this->chat = $chat;
    }

    public function getChat(): Chat
    {
        return $this->chat;
    }

    public function getData(): ChatMessage
    {
        return $this->chatMessage;
    }
}