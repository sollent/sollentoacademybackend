<?php

declare(strict_types=1);

namespace App\ChatMessage\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Chat\Repository\ChatRepositoryInterface;
use App\Chat\Service\ChatServiceInterface;
use App\ChatMessage\ApiResource\ChatMessage;
use App\ChatMessage\ApiResource\ChatTextMessage;
use App\ChatMessage\Event\Chat\ChatMessageEvent;
use App\Common\ApiResource\User;
use App\Common\Exception\UnauthorizedException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class ChatTextMessageDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $entityManager;

    private ValidatorInterface $validator;

    private EventDispatcherInterface $dispatcher;

    private ChatServiceInterface $chatService;

    private TokenStorageInterface $tokenStorage;

    private ChatRepositoryInterface $chatRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     * @param EventDispatcherInterface $dispatcher
     * @param ChatServiceInterface $chatService
     * @param TokenStorageInterface $tokenStorage
     * @param ChatRepositoryInterface $chatRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator,
        EventDispatcherInterface $dispatcher,
        ChatServiceInterface $chatService,
        TokenStorageInterface $tokenStorage,
        ChatRepositoryInterface $chatRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->dispatcher = $dispatcher;
        $this->chatService = $chatService;
        $this->tokenStorage = $tokenStorage;
        $this->chatRepository = $chatRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function supports($data, array $context = []): bool
    {
        return $data instanceof ChatTextMessage;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \Exception
     *
     * @var ChatMessage $data
     */
    public function persist($data, array $context = [])
    {
        $this->validator->validate($data);

        $user = $this->tokenStorage->getToken()->getUser();
        if (($user instanceof User) === false) {
            throw new UnauthorizedException();
        }

        $data->setSender($user);

        $this->entityManager->persist($data);

        if (($chat = $this->chatRepository->findChatBetween($user, $data->getReceiver())) === null) {
            $this->chatService->createChatBetween($user, $data->getReceiver(), $data);
        }

        if ($chat !== null) {
            $data->setChat($chat);
        }

        $this->entityManager->flush();;

        $this->dispatcher->dispatch(new ChatMessageEvent($data, $data->getChat()), ChatMessageEvent::NAME);
    }

    /**
     * {@inheritDoc}
     */
    public function remove($data, array $context = [])
    {
        $this->entityManager->remove($data);
        $this->entityManager->flush();
    }
}