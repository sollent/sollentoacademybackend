<?php

declare(strict_types=1);

namespace App\ChatMessage\Subscriber;

use App\ChatMessage\Event\Chat\ChatMessageEvent;
use App\Common\Service\Websocket\WebsocketSenderServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class ChatMessageSubscriber implements EventSubscriberInterface
{
    private WebsocketSenderServiceInterface $websocketSenderService;

    private SerializerInterface $serializer;

    /**
     * @param WebsocketSenderServiceInterface $websocketSenderService
     * @param SerializerInterface $serializer
     */
    public function __construct(
        WebsocketSenderServiceInterface $websocketSenderService,
        SerializerInterface $serializer
    )
    {
        $this->websocketSenderService = $websocketSenderService;
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ChatMessageEvent::NAME => 'onChatMessage'
        ];
    }

    public function onChatMessage(ChatMessageEvent $event)
    {
        $channelName = \sprintf('%s-%s', 'chat', $event->getChat()->getId());
        $eventName = ChatMessageEvent::NAME;

        $this->websocketSenderService->send(
            $channelName,
            $eventName,
            $this->serializer->serialize($event->getData(), 'json', [
                'groups' => ['chat-message:read', 'chat-text-message:read']
            ])
        );
    }
}