<?php
declare(strict_types=1);

namespace App\Client\ApiResource;

use App\Common\ApiResource\User;
use App\UserCommon\ApiResource\Profile;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Class Client
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get"
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"},
 *          "delete"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     normalizationContext={
 *          "groups"={
 *              "client:read",
 *              "user:read",
 *              "media_object_read",
 *              "id-readable"
 *          },
 *          "enable_max_depth"="true"
 *     },
 *     denormalizationContext={"groups"={"client:write", "user:write"}}
 * )
 *
 * @ORM\Entity
 */
class Client extends User
{
    /**
     * @Groups({
     *      "client:read",
     *      "client:write",
     *      "user:read",
     *      "user:write",
     * })
     *
     * @ORM\OneToOne(targetEntity="App\UserCommon\ApiResource\Profile", cascade={"persist", "remove"})
     */
    protected ?Profile $profile = null;

}