<?php
declare(strict_types=1);

namespace App\Client\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

final class ClientController extends AbstractController
{
}