<?php

declare(strict_types=1);

namespace App\Practice\ApiResource;

use App\Common\ApiResource\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Practice
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get"
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"},
 *          "delete"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     normalizationContext={
 *          "groups"={
 *              "practice:read",
 *              "user:read",
 *              "media_object_read",
 *              "id-readable"
 *          },
 *          "enable_max_depth"="true"
 *     },
 *     denormalizationContext={"groups"={"practice:write"}}
 * )
 *
 * @ORM\Entity
 */
class Practice extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column()
     */
    private string $title;
}