<?php
declare(strict_types=1);

namespace App\Admin\Controller;

use App\Admin\Type\AdminLoginType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AdminController extends AbstractController
{
    private AuthenticationUtils $authenticationUtils;

    /**
     * @param \Symfony\Component\Security\Http\Authentication\AuthenticationUtils $authenticationUtils
     */
    public function __construct(AuthenticationUtils $authenticationUtils)
    {
        $this->authenticationUtils = $authenticationUtils;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(): Response
    {
        $form = $this->createForm(AdminLoginType::class, [
            'email' => $this->authenticationUtils->getLastUsername(),
        ]);

        return $this->render('security/login.html.twig', [
            'error' => $this->authenticationUtils->getLastAuthenticationError(),
            'form' => $form->createView(),
            'last_username' => $this->authenticationUtils->getLastUsername(),
        ]);
    }

    public function logoutAction(): void
    {
        // Left empty intentionally because this will be handled by Symfony.
    }
}
