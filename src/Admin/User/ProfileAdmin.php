<?php
declare(strict_types=1);

namespace App\Admin\User;

use App\Common\ApiResource\MediaObject;
use App\UserCommon\ApiResource\Profile;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProfileAdmin extends AbstractAdmin
{
    /**
     * @var string
     */
    protected $baseRouteName = "profile_admin";

    /**
     * @var string
     */
    protected $baseRoutePattern = "profile";

    /**
     * @param \Sonata\AdminBundle\Form\FormMapper $form
     *
     * @return void
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form
            ->add('avatar', ModelAutocompleteType::class, [
                'class' => MediaObject::class,
                'minimum_input_length' => 0,
                'property' => 'filePath',
                'required' => true,
            ])
            ->add('firstName', TextType::class, [
                'required' => true,
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
            ])
            ->add('about', TextareaType::class, [
                'required' => false,
            ])
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Male' => Profile::GENDER_MALE,
                    'Female' => Profile::GENDER_FEMALE,
                    'Other' => Profile::GENDER_OTHERS,
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $list): void
    {
        $list
            ->add('avatar', null, [
                'size' => 115,
                'template' => '/admin/layout_image/avatar_layout.html.twig',
            ])
            ->addIdentifier('id')
            ->add('firstName')
            ->add('lastName')
            ->add('about');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureShowFields(ShowMapper $show): void
    {
        $show
            ->add('id')
            ->add('avatar', null, [
                'size' => 300,
                'template' => '/admin/layout_image/avatar_layout.html.twig',
            ])
            ->add('firstName')
            ->add('lastName')
            ->add('about');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid
            ->add('firstName')
            ->add('lastName')
            ->add('about');
    }
}
