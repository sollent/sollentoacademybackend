<?php
declare(strict_types=1);

namespace App\Admin\Security;

use App\Admin\ApiResource\Admin;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

final class AdminProvider implements UserProviderInterface
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function loadUserByUsername(string $username): UserInterface
    {
        $user = $this->findOneUserBy(['email' => $username]);

        if ($user === null) {
            throw new UsernameNotFoundException(
                \sprintf(
                    'User with "%s" email does not exist.',
                    $username
                )
            );
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        \assert($user instanceof Admin);

        $reloadedUser = $this->findOneUserBy(['id' => $user->getId()]);
        if ($reloadedUser === null) {
            throw new UsernameNotFoundException(\sprintf(
                'User with ID "%s" could not be reloaded.',
                $user->getId()
            ));
        }

        return $reloadedUser;
    }

    public function supportsClass(string $class): bool
    {
        return $class === Admin::class;
    }

    private function findOneUserBy(array $options): ?Admin
    {
        return $this->entityManager
            ->getRepository(Admin::class)
            ->findOneBy($options);
    }
}
