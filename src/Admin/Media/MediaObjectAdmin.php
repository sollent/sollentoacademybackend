<?php
declare(strict_types=1);

namespace App\Admin\Media;

use App\Common\ApiResource\MediaObject;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Vich\UploaderBundle\Form\Type\VichFileType;
use Vich\UploaderBundle\Storage\StorageInterface;

class MediaObjectAdmin extends AbstractAdmin
{
    /**
     * @var string
     */
    protected $baseRouteName = "media_admin";

    /**
     * @var string
     */
    protected $baseRoutePattern = "media";

    private EntityManagerInterface $entityManager;

    private StorageInterface $storage;

    /**
     * @required
     */
    public function setEntityManager(EntityManagerInterface $entityManager): void
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @required
     */
    public function setStorage(StorageInterface $storage): void
    {
        $this->storage = $storage;
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagrid): void
    {
        $datagrid
            ->add('filePath');
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $form): void
    {
        $form->add('file', VichFileType::class, [
            'download_link' => false,
            'required' => false,
        ]);
    }

    /**
     * @param \App\Common\ApiResource\MediaObject $object
     */
    protected function prePersist($object): void
    {
        $this->manageFileUpload($object);
    }

    /**
     * @param \App\Common\ApiResource\MediaObject $object
     */
    protected function preUpdate($object): void
    {
        $this->manageFileUpload($object);
    }

    private function manageFileUpload(MediaObject $mediaObject)
    {
        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
        $uploadedFile = $this->getForm()->get('file')->getData();
        $mediaObject->setFile($uploadedFile);

        $this->entityManager->persist($mediaObject);

        $mediaObject->setContentUrl($this->storage->resolveUri($mediaObject, 'file'));
        $this->entityManager->flush();
    }
}
