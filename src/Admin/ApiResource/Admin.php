<?php
declare(strict_types=1);

namespace App\Admin\ApiResource;

use App\Common\ApiResource\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class Admin extends User
{
}
