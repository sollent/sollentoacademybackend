<?php
declare(strict_types=1);

namespace App\Chat\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\ChatMessage\ApiResource\ChatMessage;
use App\Common\ApiResource\AbstractEntity;
use App\Common\ApiResource\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Client
 *
 * @ApiResource(
 *     collectionOperations={
 *          "get",
 *          "post"
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"},
 *          "delete"={"access_control"="(is_granted('ROLE_CLIENT') and object == user) or is_granted('ROLE_SUPER_ADMIN')"}
 *     },
 *     normalizationContext={
 *          "groups"={
 *              "chat:read",
 *              "user:read",
 *              "media_object_read",
 *              "id-readable"
 *          },
 *          "enable_max_depth"="true"
 *     },
 *     denormalizationContext={"groups"={"chat:write"}}
 * )
 *
 * @ORM\Entity
 */
class Chat extends AbstractEntity
{
    /**
     * @var User
     *
     * @Groups({
     *     "client:read",
     *     "manager:read",
     *     "user:read",
     *     "chat:read",
     * })
     *
     * @ORM\ManyToOne(targetEntity="App\Common\ApiResource\User")
     */
    private User $creator;

    /**
     * @var User
     *
     * @Groups({
     *     "client:read",
     *     "manager:read",
     *     "user:read",
     *     "chat:read",
     * })
     *
     * @ORM\ManyToOne(targetEntity="App\Common\ApiResource\User")
     */
    private User $invitedUser;

    /**
     * @var Collection
     *
     * @Groups({
     *      "client:read",
     *      "manager:read",
     *      "user:read",
     *      "chat:read",
     *  })
     *
     * @ORM\ManyToMany(targetEntity="App\ChatMessage\ApiResource\ChatMessage", fetch="EXTRA_LAZY")
     */
    private Collection $messages;

    public function __construct()
    {
        parent::__construct();

        $this->messages = new ArrayCollection();
    }

    public function getCreator(): User
    {
        return $this->creator;
    }

    public function setCreator(User $creator): void
    {
        $this->creator = $creator;
    }

    public function getInvitedUser(): User
    {
        return $this->invitedUser;
    }

    public function setInvitedUser(User $invitedUser): void
    {
        $this->invitedUser = $invitedUser;
    }

    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function setMessages(Collection $messages): void
    {
        $this->messages = $messages;
    }

    public function addMessage(ChatMessage $chatMessage): void
    {
        if ($this->messages->contains($chatMessage) === false) {
            $this->messages->add($chatMessage);
            $chatMessage->setChat($this);
        }
    }
}