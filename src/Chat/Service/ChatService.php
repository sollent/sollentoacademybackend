<?php

declare(strict_types=1);

namespace App\Chat\Service;

use App\Chat\ApiResource\Chat;
use App\Chat\Repository\ChatRepositoryInterface;
use App\ChatMessage\ApiResource\ChatMessage;
use App\Common\ApiResource\User;

final class ChatService implements ChatServiceInterface
{
    private ChatRepositoryInterface $chatRepository;

    /**
     * @param ChatRepositoryInterface $chatRepository
     */
    public function __construct(ChatRepositoryInterface $chatRepository)
    {
        $this->chatRepository = $chatRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function createChatBetween(User $creator, User $invited, ChatMessage $initialMessage): Chat
    {
        $chat = new Chat();
        $chat->setCreator($creator);
        $chat->setInvitedUser($invited);
        $chat->addMessage($initialMessage);
        $initialMessage->setChat($chat);

        $this->chatRepository->save($chat);

        return $chat;
    }

    /**
     * {@inheritDoc}
     */
    public function chatExistsBetween(User $sender, User $receiver): bool
    {
        return $this->chatRepository->chatExistsBetween($sender, $receiver);
    }
}