<?php

declare(strict_types=1);

namespace App\Chat\Service;

use App\Chat\ApiResource\Chat;
use App\ChatMessage\ApiResource\ChatMessage;
use App\Common\ApiResource\User;

interface ChatServiceInterface
{
    /**
     * @param User $creator
     * @param User $invited
     * @param ChatMessage $initialMessage
     *
     * @return Chat
     */
    public function createChatBetween(User $creator, User $invited, ChatMessage $initialMessage): Chat;

    /**
     * @param User $sender
     * @param User $receiver
     *
     * @return bool
     */
    public function chatExistsBetween(User $sender, User $receiver): bool;
}