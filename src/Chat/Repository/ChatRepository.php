<?php

namespace App\Chat\Repository;

use App\Chat\ApiResource\Chat;
use App\Common\ApiResource\User;
use App\Common\Repository\AbstractRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

final class ChatRepository extends AbstractRepository implements ChatRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    protected function getEntityClass(): string
    {
        return Chat::class;
    }

    /**
     * {@inheritDoc}
     *
     * @throws OptimisticLockException
     * @throws ORMException
     */
    public function save(Chat $chat): void
    {
        $this->_em->persist($chat);
        $this->_em->flush($chat);
    }

    /**
     * {@inheritDoc}
     */
    public function chatExistsBetween(User $sender, User $receiver): bool
    {
        $queryBuilder = $this->createQueryBuilder('c');
        $queryBuilder
            ->select('c.id')
            ->where('c.creator = :creator')
            ->andWhere('c.invitedUser = :invitedUser')
            ->setParameter('creator', $sender)
            ->setParameter('invitedUser', $receiver)
            ->setMaxResults(1);

        $result = $queryBuilder->getQuery()->getResult();

        return count($result) > 0;
    }

    /**
     * {@inheritDoc}
     */
    public function findChatBetween(User $sender, User $receiver): ?Chat
    {
        $queryBuilder = $this->createQueryBuilder('c');
        $queryBuilder
            ->where('c.creator = :creator')
            ->andWhere('c.invitedUser = :invitedUser')
            ->setParameter('creator', $sender)
            ->setParameter('invitedUser', $receiver)
            ->setMaxResults(1);

        try {
            return $queryBuilder->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}