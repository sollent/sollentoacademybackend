<?php

declare(strict_types=1);

namespace App\Chat\Repository;

use App\Chat\ApiResource\Chat;
use App\Common\ApiResource\User;

interface ChatRepositoryInterface
{
    /**
     * @param User $sender
     * @param User $receiver
     *
     * @return Chat
     */
    public function findChatBetween(User $sender, User $receiver): ?Chat;

    /**
     * @param Chat $chat
     * @return void
     */
    public function save(Chat $chat): void;

    /**
     * @param User $sender
     * @param User $receiver
     * @return bool
     */
    public function chatExistsBetween(User $sender, User $receiver): bool;
}