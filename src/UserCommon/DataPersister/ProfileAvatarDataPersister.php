<?php
declare(strict_types=1);

namespace App\UserCommon\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use App\UserCommon\ApiResource\Profile;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;

final class ProfileAvatarDataPersister implements ContextAwareDataPersisterInterface
{
    private EntityManagerInterface $entityManager;

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param object $data
     * @param mixed[]|null $context
     *
     * @return object|void
     *
     * @throws \Exception
     */
    public function persist($data, ?array $context = null)
    {
    }

    /**
     * @param \App\UserCommon\ApiResource\Profile $data
     * @param mixed[]|null $context
     *
     * @return object|void
     */
    public function remove($data, ?array $context = null)
    {
        if ($data->getAvatar()) {
            $this->entityManager->remove($avatar = $data->getAvatar());
            $data->setAvatar(null);

            // delete avatar file physically
            $filesystem = new Filesystem();
            $filesystem->remove(
                \sprintf('public/media/%s', $avatar->getFilePath())
            );
        }

        $this->entityManager->flush();
    }

    public function supports($data, ?array $context = null): bool
    {
        return $data instanceof Profile;
    }
}
