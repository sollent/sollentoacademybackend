<?php
declare(strict_types=1);

namespace App\UserCommon\ApiResource;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Entity\MappedSuperclass\AbstractTranslation;

/**
 * @ORM\Table(name="profile_translation", indexes={
 *      @ORM\Index(name="profile_translation_idx", columns={"locale", "object_class", "field", "foreign_key"})
 * })
 * @ORM\Entity(repositoryClass="Gedmo\Translatable\Entity\Repository\TranslationRepository")
 */
class ProfileTranslation extends AbstractTranslation
{
}
