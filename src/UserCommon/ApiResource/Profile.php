<?php
declare(strict_types=1);

namespace App\UserCommon\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\ApiResource\AbstractEntity;
use App\Common\ApiResource\MediaObject;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get",
 *         "post"
 *     },
 *     itemOperations={
 *          "get",
 *          "put",
 *          "delete_avatar"={
 *              "path"="/profiles/{id}/delete-avatar",
 *              "method"="DELETE",
 *              "denormalization_context"={"groups"={"profile-avatar:delete"}}
 *          }
 *     },
 *     normalizationContext={"groups"={"client:read", "manager:read", "user:read"}},
 *     denormalizationContext={"groups"={"profile:write", "client:write", "manager:write"}}
 * )
 *
 * @ORM\Entity
 *
 * @Gedmo\TranslationEntity(class="App\UserCommon\ApiResource\ProfileTranslation")
 */
class Profile extends AbstractEntity
{
    /**
     * @var string[]
     */
    public const ALL_GENDERS = [
        self::GENDER_MALE,
        self::GENDER_FEMALE,
        self::GENDER_OTHERS,
    ];

    /**
     * @var string
     */
    public const GENDER_MALE = 'male';

    /**
     * @var string
     */
    public const GENDER_FEMALE = 'female';

    /**
     * @var string
     */
    public const GENDER_OTHERS = 'others';

    /**
     * @Groups({
     *     "client:read",
     *     "client:write",
     *     "manager:read",
     *     "manager:write",
     *     "user:read"
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Gedmo\Translatable
     */
    protected ?string $firstName = null;

    /**
     * @Groups({
     *     "client:read",
     *     "client:write",
     *     "manager:read",
     *     "manager:write",
     *     "user:read"
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Gedmo\Translatable
     */
    protected ?string $lastName = null;

    /**
     * @Assert\Choice(choices=Profile::ALL_GENDERS)
     *
     * @Groups({
     *      "client:read",
     *      "client:write",
     *      "manager:read",
     *      "manager:write",
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $gender = null;

    /**
     * @Groups({
     *       "client:read",
     *       "client:write",
     *       "manager:read",
     *       "manager:write",
     *  })
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private ?\DateTime $birthDate = null;

    /**
     * @Groups({
     *       "client:read",
     *       "client:write",
     *       "manager:read",
     *       "manager:write",
     *  })
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Gedmo\Translatable
     */
    private ?string $about = null;

    /**
     * @Groups({
     *       "client:read",
     *       "client:write",
     *       "manager:read",
     *       "manager:write",
     *       "user:read",
     *       "user:write",
     *  })
     *
     * @ORM\ManyToOne(targetEntity="App\Common\ApiResource\MediaObject")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected ?MediaObject $avatar = null;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getFirstName() . ' ' . $this->getLastName();
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName = null): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName = null): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return \App\Common\ApiResource\MediaObject|null
     */
    public function getAvatar(): ?MediaObject
    {
        return $this->avatar;
    }

    /**
     * @param \App\Common\ApiResource\MediaObject|null $avatar
     */
    public function setAvatar(?MediaObject $avatar = null): void
    {
        $this->avatar = $avatar;
    }

    /**
     * @return string|null
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string|null $gender
     *
     * @return \App\UserCommon\ApiResource\Profile
     */
    public function setGender(?string $gender = null): Profile
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param \DateTime|null $birthDate
     *
     * @return \App\UserCommon\ApiResource\Profile
     */
    public function setBirthDate(?\DateTime $birthDate = null): Profile
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAbout(): ?string
    {
        return $this->about;
    }

    /**
     * @param string|null $about
     *
     * @return \App\UserCommon\ApiResource\Profile
     */
    public function setAbout(?string $about = null): Profile
    {
        $this->about = $about;

        return $this;
    }
}
