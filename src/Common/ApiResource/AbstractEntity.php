<?php
declare(strict_types=1);

namespace App\Common\ApiResource;

use App\Common\Traits\Timestampable\TimestampableInterface;
use App\Common\Traits\Timestampable\TimestampableTrait;
use Carbon\CarbonImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use ReflectionClass;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 *
 * @SuppressWarnings(PHPMD.ShortVariable) For short "id" property в abstractEntity
 */
abstract class AbstractEntity implements TimestampableInterface
{
    use TimestampableTrait;

    /**
     * @Groups({
     *      "id-readable"
     * })
     *
     * @ORM\Column(type="string", unique=true)
     * @ORM\Id
     */
    protected ?string $id = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */
    protected ?DateTimeInterface $createdAt = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true, options={"default": "CURRENT_TIMESTAMP"})
     */
    protected ?DateTimeInterface $updatedAt = null;

    public function __construct()
    {
        try {
            $this->id = Uuid::uuid4()->toString();
        } catch (\Exception $exception) {
        }
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function __isset(string $name): bool
    {
        /** @noinspection PhpVariableVariableInspection */
        return isset($this->$name);
    }

    /**
     * @return \DateTimeInterface|null
     *
     * @Groups({"created_at:read"})
     *
     * @SerializedName("createdAt")
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getShortClassName(): string
    {
        $reflectionClass = new ReflectionClass(static::class);

        return $reflectionClass->getShortName();
    }

    /**
     * @return \DateTimeInterface|null
     *
     * @Groups({"updated_at:read"})
     *
     * @SerializedName("updatedAt")
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param string|null $id
     *
     * @return $this
     */
    public function setId(?string $id = null): AbstractEntity
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     *
     * @noinspection MagicMethodsValidityInspection
     */
    public function updateTimestamps(): void
    {
        $dateTime = CarbonImmutable::now();

        if (isset($this->createdAt) === false) {
            $this->createdAt = $dateTime;
        }

        $this->updatedAt = $dateTime;
    }
}
