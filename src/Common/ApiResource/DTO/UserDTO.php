<?php
declare(strict_types=1);

namespace App\Common\ApiResource\DTO;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "path"="/users",
 *              "openapi_context"={
 *                  "summary"="User registration",
 *                  "description"="Endpoint for client/manager registration (with ability to using Google/Facebook auth)",
 *              },
 *          }
 *     },
 *     itemOperations={},
 *     denormalizationContext={"groups"={"user-dto:write"}}
 * )
 */
final class UserDTO implements UserDTOInterface
{
    /**
     * @ApiProperty(required=true)
     *
     * @Assert\NotBlank(
     *     groups={
     *          "user-dto:write",
     *          "social-auth-validation"
     *      }
     * )
     * @Assert\Length(
     *     max=255,
     *     groups={
     *          "user-dto:write",
     *          "social-auth-validation"
     *      }
     * )
     * @Assert\Email(
     *     message="registration.email",
     *     groups={
     *          "user-dto:write",
     *          "social-auth-validation"
     *      }
     * )
     *
     * @Groups({"user-dto:write"})
     */
    public string $email;

    public ?string $facebookId = null;

    public ?string $googleId = null;

    /**
     * @ApiProperty(required=false, description="Referral code from other user")
     *
     * @Groups({"user-dto:write"})
     */
    public ?string $referralCode = null;

    /**
     * @ApiProperty(required=true, description="Must be greater than or equal to 6")
     *
     * @Assert\NotBlank(groups={"user-dto:write"})
     * @Assert\Length(min="6", max="255", groups={"user-dto:write"})
     *
     * @Groups({"user-dto:write"})
     */
    public ?string $plainPassword = null;

    /**
     * @ApiProperty(description="Accepts string types: 'client', 'manager'", required=true)
     *
     * @Assert\NotBlank(
     *     groups={
     *          "user-dto:write",
     *          "social-auth-validation"
     *      }
     * )
     * @Assert\Choice(
     *     choices=UserDTOInterface::USER_TYPES,
     *     groups={
     *          "user-dto:write",
     *          "social-auth-validation"
     *      }
     * )
     *
     * @Groups({"user-dto:write"})
     *
     * @SerializedName("userType")
     */
    public string $type;
}
