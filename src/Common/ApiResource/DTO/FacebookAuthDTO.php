<?php
declare(strict_types=1);

namespace App\Common\ApiResource\DTO;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\DTOs\Auth\FacebookAuthOutput;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "route_name"="auth-facebook-action",
 *              "output"=FacebookAuthOutput::class,
 *              "defaults"={"_api_persist"=false},
 *              "openapi_context"={
 *                  "summary"="Facebook auth endpoint",
 *                  "description"="You need to send facebook accessToken to get finish system token and refresh_token"
 *              },
 *          }
 *     },
 *     itemOperations={},
 *     normalizationContext={"groups"={"facebook-auth-dto:read"}},
 *     denormalizationContext={"groups"={"facebook-auth-dto:write"}}
 * )
 */
final class FacebookAuthDTO
{
    /**
     * @ApiProperty(required=true)
     *
     * @Assert\NotBlank
     *
     * @Groups({
     *      "facebook-auth-dto:write"
     * })
     */
    public string $accessToken;

    /**
     * @ApiProperty(
     *     description="userType is required for new user (registration)",
     *     required=false,
     *     example="manager"
     * )
     *
     * @Assert\Choice(choices=UserDTOInterface::USER_TYPES)
     *
     * @Groups({
     *      "facebook-auth-dto:write"
     * })
     *
     * @SerializedName("userType")
     */
    public ?string $type = null;

    /**
     * @ApiProperty(required=false)
     *
     * @Groups({
     *      "facebook-auth-dto:write"
     * })
     */
    public ?string $referralCode = null;

    public string $token;

    public string $refresh_token;
}
