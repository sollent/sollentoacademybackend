<?php
declare(strict_types=1);

namespace App\Common\ApiResource\DTO;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "route_name"="email-confirmation-action",
 *              "defaults"={"_api_persist"=false},
 *              "openapi_context"={
 *                  "summary"="Email confirmation endpoint",
 *                  "description"="You need to send confirmation code identificate the user",
 *                  "responses"={
 *                      "200"={"description"="User has been successfully identificated"},
 *                  },
 *                  "status"=200,
 *              },
 *          }
 *     },
 *     itemOperations={},
 *     normalizationContext={"groups"={"email-confirmation-dto:read"}},
 *     denormalizationContext={"groups"={"email-confirmation-dto:write"}}
 * )
 */
final class EmailConfirmationDTO
{
    /**
     * @ApiProperty(required=true)
     *FacebookAuthController.php
     *
     * @Assert\NotBlank
     *
     * @Groups({
     *      "email-confirmation-dto:write"
     * })
     */
    public string $emailConfirmationCode;
}
