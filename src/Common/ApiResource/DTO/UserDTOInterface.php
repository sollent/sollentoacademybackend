<?php
declare(strict_types=1);

namespace App\Common\ApiResource\DTO;

/**
 * Interface UserDTOInterface
 */
interface UserDTOInterface
{
    /**
     * @var string
     */
    public const TYPE_CLIENT = 'client';

    /**
     * @var string
     */
    public const TYPE_MANAGER = 'manager';

    /**
     * @var string[]
     */
    public const USER_TYPES = [
        self::TYPE_CLIENT,
        self::TYPE_MANAGER,
    ];
}
