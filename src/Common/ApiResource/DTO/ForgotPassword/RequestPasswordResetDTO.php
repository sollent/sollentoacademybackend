<?php
declare(strict_types=1);

namespace App\Common\ApiResource\DTO\ForgotPassword;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\DTOs\ForgotPassword\RequestPasswordResetOutput;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "route_name"="request-password-reset-action",
 *              "output"=RequestPasswordResetOutput::class,
 *              "defaults"={"_api_persist"=false},
 *              "openapi_context"={
 *                  "summary"="Request for resetting password. Email with reset password link will be sent to user email",
 *                  "description"="You need to send user email"
 *              },
 *          }
 *     },
 *     itemOperations={},
 *     normalizationContext={"groups"={"request-password-reset-dto:read", "default-password-reset:read"}},
 *     denormalizationContext={"groups"={"request-password-reset-dto:write"}}
 * )
 */
final class RequestPasswordResetDTO
{
    /**
     * Need to check for user unique email
     *
     * @ApiProperty(required=true)
     *
     * @Assert\NotBlank
     * @Assert\Email
     *
     * @Groups({
     *     "request-password-reset-dto:write"
     * })
     */
    public string $email;
}
