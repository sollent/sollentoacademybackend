<?php
declare(strict_types=1);

namespace App\Common\ApiResource\DTO\ForgotPassword;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\DTOs\ForgotPassword\DefaultPasswordResetOutput;
use App\Common\Service\ResetPassword\TokenGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "route_name"="change-password-action",
 *              "output"=DefaultPasswordResetOutput::class,
 *              "defaults"={"_api_persist"=false},
 *              "openapi_context"={
 *                  "summary"="Change password functionality",
 *                  "description"="You need to provide 'token' and 'newPassword' to complete resetting password process"
 *              },
 *          }
 *     },
 *     itemOperations={},
 *     normalizationContext={"groups"={"change-password-dto:read", "default-password-reset:read"}},
 *     denormalizationContext={"groups"={"change-password-dto:write"}}
 * )
 */
final class ChangePasswordDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(min="6", max="255")
     *
     * @Groups({
     *     "change-password-dto:write"
     * })
     */
    public string $newPassword;

    /**
     * @Assert\NotBlank
     * @Assert\Length(TokenGenerator::PASSWORD_TOKEN_LENGTH)
     *
     * @Groups({
     *     "change-password-dto:write"
     * })
     */
    public string $token;
}
