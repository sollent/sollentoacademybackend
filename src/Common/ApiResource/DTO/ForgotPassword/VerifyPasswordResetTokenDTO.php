<?php
declare(strict_types=1);

namespace App\Common\ApiResource\DTO\ForgotPassword;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\DTOs\ForgotPassword\DefaultPasswordResetOutput;
use App\Common\Service\ResetPassword\TokenGenerator;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     collectionOperations={
 *          "post"={
 *              "method"="POST",
 *              "route_name"="verify-reset-password-token-action",
 *              "output"=DefaultPasswordResetOutput::class,
 *              "defaults"={"_api_persist"=false},
 *              "openapi_context"={
 *                  "summary"="Endpoint for verification password reset token - checking if the user can change the password on current page with current token",
 *                  "description"="You need to provide only 'token'"
 *              },
 *          }
 *     },
 *     itemOperations={},
 *     normalizationContext={"groups"={"verify-password-reset-token-dto:read", "default-password-reset:read"}},
 *     denormalizationContext={"groups"={"verify-password-reset-token-dto:write"}}
 * )
 */
final class VerifyPasswordResetTokenDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\Length(TokenGenerator::PASSWORD_TOKEN_LENGTH)
     *
     * @Groups({
     *     "verify-password-reset-token-dto:write"
     * })
     */
    public string $token;
}
