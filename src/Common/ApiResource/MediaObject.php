<?php
declare(strict_types=1);

namespace App\Common\ApiResource;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\Controller\ApiPlatform\Media\CreateMediaObjectAction;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ApiResource(
 *     iri="http://schema.org/MediaObject",
 *     normalizationContext={
 *         "groups"={"media_object_read", "id-readable"}
 *     },
 *     collectionOperations={
 *         "post"={
 *             "controller"=CreateMediaObjectAction::class,
 *             "deserialize"=false,
 *             "validation_groups"={"Default", "media_object_create"},
 *             "openapi_context"={
 *                 "requestBody"={
 *                     "content"={
 *                         "multipart/form-data"={
 *                             "schema"={
 *                                 "type"="object",
 *                                 "properties"={
 *                                     "file"={
 *                                         "type"="string",
 *                                         "format"="binary"
 *                                     }
 *                                 }
 *                             }
 *                         }
 *                     }
 *                 }
 *             }
 *         },
 *         "get"={
 *              "access_control"="is_granted('ROLE_SUPER_ADMIN')",
 *              "openapi_context"={
 *                  "description"="<b>REQUIRES AUTHENTICATION</b><br><br><b>Who has the permissions: </b>SUPERADMIN<br><br><b>Permission description: </b>Collection of all media objects may retrieve only superadmin"
 *              }
 *          }
 *     },
 *     itemOperations={
 *         "get"={
 *              "openapi_context"={
 *                  "description"="<b>Who has the permissions: </b>ANYONE"
 *              }
 *         },
 *         "delete"={
 *              "access_control"="is_granted('ROLE_SUPER_ADMIN') or is_granted('ROLE_CLIENT') or is_granted('ROLE_MANAGER')",
 *              "openapi_context"={
 *                  "description"="<b>Who has the permissions: </b>SUPERADMIN"
 *              }
 *         },
 *     }
 * )
 *
 * @ORM\Entity
 *
 * @Vich\Uploadable
 */
class MediaObject extends AbstractEntity
{
    /**
     * @ApiProperty(iri="http://schema.org/contentUrl")
     *
     * @Groups({
     *     "media_object_read"
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $contentUrl = null;

    /**
     * @Assert\NotNull(groups={"media_object_create"})
     *
     * @Vich\UploadableField(mapping="media_object", fileNameProperty="filePath")
     */
    private ?File $file = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $filePath = null;

    public function __toString()
    {
        return $this->filePath;
    }

    /**
     * @return string|null
     */
    public function getContentUrl(): ?string
    {
        return $this->contentUrl;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\File\File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * @return string|null
     */
    public function getFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @param string|null $contentUrl
     */
    public function setContentUrl(?string $contentUrl = null): void
    {
        $this->contentUrl = $contentUrl;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File|null $file
     */
    public function setFile(?File $file = null): void
    {
        $this->file = $file;
        if ($file) {
            $this->updatedAt = new \DateTimeImmutable('now');
        }
    }

    /**
     * @param string|null $filePath
     */
    public function setFilePath(?string $filePath = null): void
    {
        $this->filePath = $filePath;
    }
}
