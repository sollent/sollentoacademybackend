<?php
declare(strict_types=1);

namespace App\Common\ApiResource;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Common\Security\RolesInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * @ApiResource(
 *     collectionOperations={
 *         "get"
 *     },
 *     itemOperations={
 *          "get",
 *          "put"={"access_control"="is_granted('ROLE_CLIENT') or is_granted('ROLE_MANAGER') or is_granted('ROLE_SUPER_ADMIN')"},
 *          "delete"
 *     },
 *     normalizationContext={"groups"={"user:read", "id-readable"}},
 *     denormalizationContext={"groups"={"user:write"}}
 * )
 *
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="user_type", type="string")
 * @ORM\DiscriminatorMap({
 *     "user"="App\Common\ApiResource\User",
 *     "manager"="App\Manager\ApiResource\Manager",
 *     "client"="App\Client\ApiResource\Client",
 *     "admin"="App\Admin\ApiResource\Admin"
 * })
 *
 * @UniqueEntity(fields={"email"})
 */
class User extends AbstractEntity implements UserInterface, PasswordAuthenticatedUserInterface
{
    /**
     * @Groups({
     *     "user:read",
     *     "client:read",
     *     "manager:read"
     * })
     *
     * @ORM\Column(type="boolean")
     */
    private bool $active = false;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $emailConfirmed = false;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $emilConfirmationCode = null;

    /**
     * @Groups({
     *      "user:read",
     *      "user:write"
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $defaultLocale = null;

    /**
     * @Groups({
     *     "user:write",
     *     "user:read"
     * })
     *
     * @ORM\Column(type="string", unique=true)
     */
    private string $email;

    /**
     * @Groups({
     *      "user:read",
     *      "user:write"
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $phone = null;

    /**
     * @Groups({
     *      "user:read",
     *      "user:write"
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $login = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $facebookId = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $googleId = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $password = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $passwordResetExpiresAt = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $passwordResetToken = null;

    /**
     * @Groups({
     *     "user:write"
     * })
     */
    private ?string $plainPassword = null;

    /**
     * @Groups({
     *     "user:read"
     * })
     *
     * @ORM\Column(type="json")
     *
     * @var string[]
     */
    private array $roles = [];

    /**
     * @Groups({
     *     "user:read"
     * })
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private ?string $referralCode = null;

    /**
     * @Groups({
     *     "user:read"
     * })
     *
     * @ORM\Column(type="integer")
     */
    private int $balance = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private ?int $deleteAccountConfirmationCode = null;

    public function __toString(): string
    {
        return $this->email;
    }

    public function addRole(string $role): self
    {
        if (\in_array($role, $this->roles, true) === false) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getPasswordResetExpiresAt(): \DateTimeInterface
    {
        return $this->passwordResetExpiresAt;
    }

    public function getPasswordResetToken(): string
    {
        return $this->passwordResetToken;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function getUserIdentifier(): string
    {
        return $this->getId();
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function hasRole(string $role): bool
    {
        return \in_array($role, $this->roles, true);
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function isPasswordResetExpired(): bool
    {
        return $this->passwordResetExpiresAt === null || $this->passwordResetExpiresAt < new \DateTimeImmutable('now');
    }

    public function isSuperAdmin(): bool
    {
        return $this->hasRole(RolesInterface::ROLE_SUPER_ADMIN);
    }

    public function removeRole(string $role): self
    {
        if (\in_array($role, $this->roles, true)) {
            unset($this->roles[$role]);
        }

        return $this;
    }

    public function setActive(bool $active): void
    {
        $this->active = $active;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setFacebookId(?string $facebookId = null): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function setGoogleId(?string $googleId = null): self
    {
        $this->googleId = $googleId;

        return $this;
    }

    public function setPassword(?string $password = null): self
    {
        $this->password = $password;

        return $this;
    }

    public function setPasswordResetExpiresAt(?\DateTimeInterface $passwordResetExpiresAt = null): self
    {
        $this->passwordResetExpiresAt = $passwordResetExpiresAt;

        return $this;
    }

    public function setPasswordResetToken(?string $passwordResetToken = null): self
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    public function setPlainPassword(?string $plainPassword = null): self
    {
        $this->plainPassword = $plainPassword;
        $this->password = null;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return \App\Common\ApiResource\User
     */
    public function setPhone(?string $phone = null): User
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     *
     * @return \App\Common\ApiResource\User
     */
    public function setLogin(?string $login = null): User
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReferralCode(): ?string
    {
        return $this->referralCode;
    }

    /**
     * @param string|null $referralCode
     *
     * @return \App\Common\ApiResource\User
     */
    public function setReferralCode(?string $referralCode = null): User
    {
        $this->referralCode = $referralCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        return $this->balance;
    }

    /**
     * @param int $balance
     *
     * @return \App\Common\ApiResource\User
     */
    public function setBalance(int $balance): User
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * @param int $amount
     *
     * @return \App\Common\ApiResource\User
     */
    public function addBalance(int $amount): User
    {
        $this->balance += $amount;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEmailConfirmed(): bool
    {
        return $this->emailConfirmed;
    }

    /**
     * @param bool $emailConfirmed
     *
     * @return \App\Common\ApiResource\User
     */
    public function setEmailConfirmed(bool $emailConfirmed): User
    {
        $this->emailConfirmed = $emailConfirmed;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmilConfirmationCode(): ?string
    {
        return $this->emilConfirmationCode;
    }

    /**
     * @param string|null $emilConfirmationCode
     *
     * @return \App\Common\ApiResource\User
     */
    public function setEmilConfirmationCode(?string $emilConfirmationCode = null): User
    {
        $this->emilConfirmationCode = $emilConfirmationCode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultLocale(): ?string
    {
        return $this->defaultLocale;
    }

    /**
     * @param string|null $defaultLocale
     *
     * @return \App\Common\ApiResource\User
     */
    public function setDefaultLocale(?string $defaultLocale = null): User
    {
        $this->defaultLocale = $defaultLocale;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getDeleteAccountConfirmationCode(): ?int
    {
        return $this->deleteAccountConfirmationCode;
    }

    /**
     * @param int|null $deleteAccountConfirmationCode
     *
     * @return \App\Common\ApiResource\User
     */
    public function setDeleteAccountConfirmationCode(?int $deleteAccountConfirmationCode = null): User
    {
        $this->deleteAccountConfirmationCode = $deleteAccountConfirmationCode;

        return $this;
    }
}
