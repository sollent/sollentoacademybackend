<?php
declare(strict_types=1);

namespace App\Common\Repository;

use App\Common\ApiResource\User;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

class UserRepository extends AbstractRepository implements UserRepositoryInterface
{
    /**
     * {@inheritDoc}
     */
    protected function getEntityClass(): string
    {
        return User::class;
    }

}
