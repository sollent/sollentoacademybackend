<?php
declare(strict_types=1);

namespace App\Common\ApiPlatform;

interface SelfProvidedIriItemInterface
{
    public function getIri(): string;
}
