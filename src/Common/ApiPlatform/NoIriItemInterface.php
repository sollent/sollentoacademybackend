<?php
declare(strict_types=1);

namespace App\Common\ApiPlatform;

interface NoIriItemInterface
{
    // Marker for items with no iri support. Allows to create Dto with no itemOperations.
}
