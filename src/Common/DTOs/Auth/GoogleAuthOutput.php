<?php
declare(strict_types=1);

namespace App\Common\DTOs\Auth;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class GoogleAuthOutput
 */
final class GoogleAuthOutput
{
    /**
     * @Assert\NotBlank
     *
     * @Groups({
     *      "google-auth-dto:read"
     * })
     */
    public string $token;

    /**
     * @Assert\NotBlank
     *
     * @Groups({
     *      "google-auth-dto:read"
     * })
     */
    public string $refresh_token;

    /**
     * @param string $token
     * @param string $refresh_token
     */
    public function __construct(string $token, string $refresh_token)
    {
        $this->token = $token;
        $this->refresh_token = $refresh_token;
    }
}
