<?php
declare(strict_types=1);

namespace App\Common\DataPersister;

use ApiPlatform\Core\DataPersister\ContextAwareDataPersisterInterface;
use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Common\ApiResource\DTO\UserDTO;
use App\Common\Service\Facade\UserRegistrationFacadeInterface;

final class UserDTODataPersister implements ContextAwareDataPersisterInterface
{
    private UserRegistrationFacadeInterface $userRegistrationFacade;

    private ValidatorInterface $validator;

    public function __construct(UserRegistrationFacadeInterface $userRegistrationFacade, ValidatorInterface $validator)
    {
        $this->userRegistrationFacade = $userRegistrationFacade;
        $this->validator = $validator;
    }

    /**
     * @param object $data
     * @param mixed[]|null $context
     *
     * @return object|void
     *
     * @throws \Exception
     */
    public function persist($data, ?array $context = null)
    {
        $this->validator->validate($data, ['groups' => ['user-dto:write']]);

        return $this->userRegistrationFacade->registerUser($data);
    }

    /**
     * @param object $data
     * @param mixed[]|null $context
     *
     * @return object|void
     */
    public function remove($data, ?array $context = null)
    {
        // Remove operation is not supported
    }

    public function supports($data, ?array $context = null): bool
    {
        return $data instanceof UserDTO;
    }
}
