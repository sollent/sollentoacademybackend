<?php
declare(strict_types=1);

namespace App\Common\Service\OpenApi;

use ApiPlatform\Core\OpenApi\OpenApi;

interface PermissionDescriberServiceInterface
{
    /**
     * @param \ApiPlatform\Core\OpenApi\OpenApi $openApi
     *
     * @return \ApiPlatform\Core\OpenApi\OpenApi
     */
    public function describePathsPermission(OpenApi $openApi): OpenApi;
}
