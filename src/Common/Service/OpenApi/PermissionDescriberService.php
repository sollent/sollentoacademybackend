<?php
declare(strict_types=1);

namespace App\Common\Service\OpenApi;

use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceNameCollectionFactoryInterface;
use ApiPlatform\Core\OpenApi\Model\PathItem;
use ApiPlatform\Core\OpenApi\OpenApi;
use ApiPlatform\Core\Security\Core\Authorization\ExpressionLanguageProvider;
use App\Common\Security\RolesInterface;
use Doctrine\Common\Annotations\Reader;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\Node\Node;
use Symfony\Component\Security\Core\Authorization\ExpressionLanguageProvider as SymfonyExpressionLanguageProvider;

final class PermissionDescriberService implements PermissionDescriberServiceInterface
{
    /**
     * @var string[]
     */
    public const OPERATION_NAMES = [
        'get',
        'post',
        'put',
        'patch',
        'delete',
    ];

    /**
     * @var array
     */
    public const ROLE_TITLES = [
        RolesInterface::ROLE_CLIENT => 'Client',
        RolesInterface::ROLE_MANAGER => 'Manager',
        RolesInterface::ROLE_SUPER_ADMIN => 'SuperAdmin',
    ];

    private ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory;

    private ResourceMetadataFactoryInterface $resourceMetadataFactory;

    private Reader $annotationReader;

    private ExpressionLanguage $expressionLanguage;

    public function __construct(
        ResourceNameCollectionFactoryInterface $resourceNameCollectionFactory,
        ResourceMetadataFactoryInterface $resourceMetadataFactory,
        Reader $annotationReader
    ) {
        $this->resourceNameCollectionFactory = $resourceNameCollectionFactory;
        $this->resourceMetadataFactory = $resourceMetadataFactory;
        $this->annotationReader = $annotationReader;

        $this->createExpressionLanguage();
    }

    private function createExpressionLanguage(): void
    {
        $this->expressionLanguage = new ExpressionLanguage(null, [
            new ExpressionLanguageProvider(),
            new SymfonyExpressionLanguageProvider(),
        ]);
    }

    /**
     * {@inheritDoc}
     */
    public function describePathsPermission(OpenApi $openApi): OpenApi
    {
        foreach ($this->resourceNameCollectionFactory->create() as $resourceClass) {
            $resourceMetadata = $this->resourceMetadataFactory->create($resourceClass);
            /** @var \ApiPlatform\Core\OpenApi\Model\PathItem $pathItem */
            foreach ($openApi->getPaths()->getPaths() as $path => $pathItem) {
                foreach (self::OPERATION_NAMES as $operationName) {
                    $getter = \sprintf('get%s', \ucfirst($operationName));
                    if ($pathItem->$getter() === null) {
                        continue;
                    }
                    if (\in_array($resourceMetadata->getShortName(), $pathItem->$getter()->getTags()) === false) {
                        continue;
                    }

                    $annotation = $this->annotationReader->getClassAnnotation(
                        new \ReflectionClass($resourceClass),
                        ApiResource::class
                    );

                    $reflectionClass = new \ReflectionClass($resourceClass);
                    $collectionOperationId = \sprintf(
                        '%s%sCollection',
                        $operationName,
                        $reflectionClass->getShortName()
                    );
                    $itemOperationId = \sprintf('%s%sItem', $operationName, $reflectionClass->getShortName());

                    $operationType = OperationType::ITEM;
                    if ($collectionOperationId === $pathItem->$getter()->getOperationId()) {
                        $operationType = OperationType::COLLECTION;
                    } elseif ($itemOperationId === $pathItem->$getter()->getOperationId()) {
                        $operationType = OperationType::ITEM;
                    }

                    $pathItem = $this->describeOperation(
                        $pathItem,
                        $annotation,
                        $operationType,
                        $operationName,
                        $resourceClass
                    );

                    $openApi->getPaths()->addPath($path, $pathItem);
                }
            }
        }

        return $openApi;
    }

    /**
     * @param \ApiPlatform\Core\OpenApi\Model\PathItem $pathItem
     * @param \ApiPlatform\Core\Annotation\ApiResource $annotation
     *
     * @return \ApiPlatform\Core\OpenApi\Model\PathItem
     */
    private function describeOperation(
        PathItem $pathItem,
        ApiResource $annotation,
        string $operationType,
        string $operationName,
        string $resourceClass
    ): PathItem {
        $operationProperty = \sprintf('%sOperations', $operationType);
        if (\array_key_exists($operationName, $annotation->$operationProperty) === false) {
            return $pathItem;
        }

        $operationData = $annotation->$operationProperty[$operationName];

        if (\array_key_exists('access_control', $operationData) === false) {
            return $pathItem;
        }

        if ($operationData['access_control'] === "" || $operationData['access_control'] === null) {
            return $pathItem;
        }

        try {
            $result = $this->expressionLanguage->parse($operationData['access_control'], ['user', 'object']);
            $result = $this->handleExpressionNode($result->getNodes());
            if (\count($result) === 1) {
                $result = [$result];
            }
            foreach ($result as $i => $item) {
                if (isset($item['role']) === false) {
                    if (\count($item) > 1) {
                        $result[$i] = \array_merge(...$item);
                    }
                }
            }
            if (\count($result) > 1) {
                \array_pop($result);
            }
            foreach ($result as $item) {
                if (\count($item) === 1 && isset($item['role']) === false) {
                    $needed = \array_merge(...$result);
                    $result = [$needed];

                    break;
                }
            }
        } catch (\Exception $e) {
            return $pathItem;
        }

        $description = $this->buildDescription($result);

        $getter = \sprintf('get%s', \ucfirst($operationName));
        $withMethod = \sprintf('with%s', \ucfirst($operationName));

        return $pathItem->$withMethod($pathItem->$getter()->withDescription($description));
    }

    /**
     * @param \Symfony\Component\ExpressionLanguage\Node\Node $nodes
     *
     * @return array
     *
     * In this method I included most common cases for 'access_control' attribute
     * For most complex situations we need to use voters and describe api
     * documentation manually
     *
     * Example of return
     * [
     *      ['role' => 'ROLE_CLIENT', 'operator' => 'and', 'expression' => 'object == user'],
     *      ['role' => 'ROLE_MANAGER']
     * ]
     */
    private function handleExpressionNode(Node $nodes)
    {
        $attributeOperator = $nodes->attributes['operator'] ?? null;
        $attributeName = $nodes->attributes['name'] ?? null;

        // Easiest case, example - "is_granted(ROLE_NAME)"
        if ($attributeOperator === null && $attributeName === 'is_granted') {
            $role = $nodes->nodes['arguments']->nodes[0]->attributes['value'];

            return ['role' => $role];
        }

        // More difficult case, example - "is_granted(ROLE_CLIENT) and is_granted(ROLE_MANAGER)"
        if ($attributeName === null && \in_array($attributeOperator, ['or', 'and'])) {
            $leftNode = $this->handleExpressionNode($nodes->nodes['left']);
            $rightNode = $this->handleExpressionNode($nodes->nodes['right']);

            return [$leftNode, $rightNode, ['operator' => $attributeOperator]];
        }

        if ($attributeOperator === '==') {
            $leftNode = $nodes->nodes['left']->attributes['name'];
            $rightNode = $nodes->nodes['right']->attributes['name'];

            return ['expression' => \sprintf('%s == %s', $leftNode, $rightNode)];
        }
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function buildDescription(array $data): string
    {
        $string = "<b>Who has access:</b><br>";

        foreach ($data as $permissionExpression) {
            $string .= "{$permissionExpression['role']}";
            if (isset($permissionExpression['expression'])) {
                $string .= " ({$permissionExpression['expression']})";
            }
            $string .= "<br>";
        }

        return $string;
    }
}
