<?php
declare(strict_types=1);

namespace App\Common\Service;

use Symfony\Component\HttpFoundation\RequestStack;

final class LocaleResolver implements LocaleResolverInterface
{
    /**
     * @var string
     */
    private const LOCALE_KEY = 'X-LOCALE';

    /**
     * @var string[]
     */
    private array $availableLocales;

    private string $defaultLocale;

    private RequestStack $requestStack;

    /**
     * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
     * @param string $defaultLocale
     * @param array $availableLocales
     */
    public function __construct(RequestStack $requestStack, string $defaultLocale, array $availableLocales)
    {
        $this->requestStack = $requestStack;
        $this->defaultLocale = $defaultLocale;
        $this->availableLocales = $availableLocales;
    }

    public function getAvailableLocales(): array
    {
        return $this->availableLocales;
    }

    public function getCurrentLocale(): string
    {
        /** @var \Symfony\Component\HttpFoundation\Request $request */
        $request = $this->requestStack->getMainRequest();
        $locale = $request->headers->get(self::LOCALE_KEY, $this->defaultLocale);

        if (\in_array($locale, $this->availableLocales, true)) {
            return $locale;
        }

        return $this->defaultLocale;
    }

    public function getDefaultLocale(): string
    {
        return $this->defaultLocale;
    }
}
