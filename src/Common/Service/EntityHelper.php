<?php
declare(strict_types=1);

namespace App\Common\Service;

use App\Common\ApiResource\AbstractEntity;
use Doctrine\ORM\EntityManagerInterface;

final class EntityHelper implements EntityHelperInterface
{
    private EntityManagerInterface $entityManager;

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public function saveEntityChangeSet(AbstractEntity $entity): void
    {
        $uow = $this->entityManager->getUnitOfWork();
        $uow->computeChangeSet($this->entityManager->getClassMetadata(\get_class($entity)), $entity);
    }
}
