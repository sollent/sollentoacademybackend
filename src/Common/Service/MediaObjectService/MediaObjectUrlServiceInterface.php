<?php
declare(strict_types=1);

namespace App\Common\Service\MediaObjectService;

use App\Common\ApiResource\MediaObject;

interface MediaObjectUrlServiceInterface
{
    /**
     * @param string $url
     *
     * @return \App\Common\ApiResource\MediaObject
     */
    public function getMediaObjectFromUrl(string $url): MediaObject;
}
