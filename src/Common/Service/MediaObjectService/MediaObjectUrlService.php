<?php
declare(strict_types=1);

namespace App\Common\Service\MediaObjectService;

use App\Common\ApiResource\MediaObject;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaObjectUrlService implements MediaObjectUrlServiceInterface
{
    public function getMediaObjectFromUrl(string $url): MediaObject
    {
        $dir = './public/media/';

        if (\file_exists($dir) === false) {
            \mkdir($dir, 0777, true);
        }

        $file = $dir . \basename($url);
        \file_put_contents($file, \file_get_contents($url));

        $uploadedFile = new UploadedFile($file, \basename($url), \mime_content_type($file), null, true);

        $mediaObject = new MediaObject();
        $mediaObject->setFile($uploadedFile);
        $mediaObject->setContentUrl($url);

        return $mediaObject;
    }
}
