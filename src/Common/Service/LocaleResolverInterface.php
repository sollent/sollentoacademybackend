<?php
declare(strict_types=1);

namespace App\Common\Service;

interface LocaleResolverInterface
{
    /**
     * @return string[]
     */
    public function getAvailableLocales(): array;

    /**
     * @return string
     */
    public function getCurrentLocale(): string;

    /**
     * @return string
     */
    public function getDefaultLocale(): string;
}
