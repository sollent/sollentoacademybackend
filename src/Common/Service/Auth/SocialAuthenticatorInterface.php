<?php
declare(strict_types=1);

namespace App\Common\Service\Auth;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface SocialAuthenticatorInterface
 */
interface SocialAuthenticatorInterface
{
    /**
     * @param string $socialAccessToken
     * @param string|null $userType
     * @param string|null $referralCode
     *
     * @return \Symfony\Component\Security\Core\User\UserInterface System
     *
     * @throws \Exception
     */
    public function authenticate(
        string $socialAccessToken,
        ?string $userType = null,
        ?string $referralCode = null
    ): UserInterface;
}
