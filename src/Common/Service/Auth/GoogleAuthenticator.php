<?php
declare(strict_types=1);

namespace App\Common\Service\Auth;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Common\ApiResource\DTO\UserDTO;
use App\Common\ApiResource\User;
use App\Common\Service\Facade\UserRegistrationFacadeInterface;
use Doctrine\ORM\EntityManagerInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Token\AccessToken;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class GoogleAuthenticator
 */
final class GoogleAuthenticator implements SocialAuthenticatorInterface
{
    private ClientRegistry $clientRegistry;

    private EntityManagerInterface $entityManager;

    private UserRegistrationFacadeInterface $userRegistrationFacade;

    private ValidatorInterface $validator;

    /**
     * FacebookAuthenticator constructor.
     *
     * @param \App\Common\Service\Facade\UserRegistrationFacadeInterface $userRegistrationFacade
     * @param \KnpU\OAuth2ClientBundle\Client\ClientRegistry $clientRegistry
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     * @param \ApiPlatform\Core\Validator\ValidatorInterface $validator
     */
    public function __construct(
        UserRegistrationFacadeInterface $userRegistrationFacade,
        ClientRegistry $clientRegistry,
        EntityManagerInterface $entityManager,
        ValidatorInterface $validator
    ) {
        $this->userRegistrationFacade = $userRegistrationFacade;
        $this->clientRegistry = $clientRegistry;
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    /**
     * {@inheritDoc}
     */
    public function authenticate(
        string $socialAccessToken,
        ?string $userType = null,
        ?string $referralCode = null
    ): UserInterface {
        $googleClient = $this->clientRegistry->getClient('google');
        $googleUser = $googleClient->fetchUserFromToken(new AccessToken([
            'access_token' => $socialAccessToken,
        ]));

        $googleUserArr = $googleUser->toArray();

        /** @var \App\Common\ApiResource\User $existingUser */
        $existingUser = $this->entityManager->getRepository(User::class)->findOneBy([
            'googleId' => $googleUser->getId(),
        ]);

        if ($existingUser) {
            $user = $existingUser;
        } else {
            $userDTO = new UserDTO();
            $userDTO->type = $userType ?? "";
            $userDTO->googleId = $googleUser->getId();
            $userDTO->email = $googleUserArr['email'];
            $userDTO->referralCode = $referralCode ?? null;

            $this->validator->validate($userDTO, ['groups' => ['social-auth-validation']]);

            $user = $this->userRegistrationFacade->registerUser($userDTO);
        }

        return $user;
    }
}
