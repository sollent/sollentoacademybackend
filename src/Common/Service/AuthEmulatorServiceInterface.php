<?php
declare(strict_types=1);

namespace App\Common\Service;

use App\Common\ApiResource\User;

interface AuthEmulatorServiceInterface
{
    /**
     * @param \App\Common\ApiResource\User $user
     *
     * @return string
     */
    public function generatePathForUserEmulateAuthentication(User $user): string;
}
