<?php
declare(strict_types=1);

namespace App\Common\Service\ResetPassword;

use App\Common\ApiResource\User;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserResetPasswordService implements UserResetPasswordServiceInterface
{
    private TokenGenerator $tokenGenerator;

    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(TokenGenerator $tokenGenerator, UserPasswordHasherInterface $passwordHasher)
    {
        $this->tokenGenerator = $tokenGenerator;
        $this->passwordHasher = $passwordHasher;
    }

    /**
     * @throws \Exception
     */
    public function resetPassword(User $user, ?int $expiresAt = null): void
    {
        $resetToken = $this->tokenGenerator->getRandomAlphaNumStr();
        $expiresAt = new \DateTimeImmutable(\sprintf('+%d seconds', $expiresAt ?? 3.6 * 1000 * 3));

        $user->setPasswordResetExpiresAt($expiresAt);
        $user->setPasswordResetToken($resetToken);
    }

    /**
     * {@inheritDoc}
     */
    public function changePassword(User $user, string $newPassword): void
    {
        $user->setPassword($this->passwordHasher->hashPassword($user, $newPassword));
        $user->setPasswordResetExpiresAt();
        $user->setPasswordResetToken();
        $user->eraseCredentials();
    }
}
