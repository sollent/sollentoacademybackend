<?php
declare(strict_types=1);

namespace App\Common\Service\ResetPassword;

use App\Common\ApiResource\User;

interface UserResetPasswordServiceInterface
{
    /**
     * @param \App\Common\ApiResource\User $user
     * @param int|null $expiresAt
     */
    public function resetPassword(User $user, ?int $expiresAt = null): void;

    /**
     * @param \App\Common\ApiResource\User $user
     * @param string $newPassword
     */
    public function changePassword(User $user, string $newPassword): void;
}
