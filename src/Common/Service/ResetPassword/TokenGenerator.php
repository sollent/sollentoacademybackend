<?php
declare(strict_types=1);

namespace App\Common\Service\ResetPassword;

/**
 * @internal
 * @final
 */
class TokenGenerator
{
    /**
     * @var string
     */
    public const PASSWORD_TOKEN_LENGTH = 20;

    /**
     * Original credit to Laravel's Str::random() method.
     *
     * String length is self::PASSWORD_TOKEN_LENGTH characters
     */
    public function getRandomAlphaNumStr(): string
    {
        $string = '';

        while (($len = \strlen($string)) < self::PASSWORD_TOKEN_LENGTH) {
            $size = self::PASSWORD_TOKEN_LENGTH - $len;

            $bytes = \random_bytes($size);

            $string .= \substr(
                \str_replace(['/', '+', '='], '', \base64_encode($bytes)),
                0,
                $size
            );
        }

        return $string;
    }
}
