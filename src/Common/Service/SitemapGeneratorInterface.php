<?php
declare(strict_types=1);

namespace App\Common\Service;

interface SitemapGeneratorInterface
{
    /**
     * @param string $entityClassName
     * @param string $uriResourceName
     * @param array|null $criteria
     *
     * @return array
     */
    public function generateDynamic(string $entityClassName, string $uriResourceName, ?array $criteria = null): array;

    /**
     * @param string $uri
     *
     * @return array
     */
    public function generateStatic(string $uri): array;
}
