<?php
declare(strict_types=1);

namespace App\Common\Service\Security;

use App\Common\ApiResource\User;

/**
 * Interface RefreshTokenServiceInterface
 */
interface RefreshTokenServiceInterface
{
    /**
     * @param \App\Common\ApiResource\User $user
     * @param int|null $ttl
     *
     * @return string
     */
    public function createForUser(User $user, ?int $ttl = null): string;
}
