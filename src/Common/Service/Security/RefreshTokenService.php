<?php
declare(strict_types=1);

namespace App\Common\Service\Security;

use App\Common\ApiResource\User;
use Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class RefreshTokenService
 */
class RefreshTokenService implements RefreshTokenServiceInterface
{
    protected RefreshTokenManagerInterface $refreshTokenManager;

    protected int $ttl;

    protected ValidatorInterface $validator;

    /**
     * RefreshTokenService constructor.
     *
     * @param int $refreshTokenTtl
     * @param \Gesdinet\JWTRefreshTokenBundle\Model\RefreshTokenManagerInterface $refreshTokenManager
     * @param \Symfony\Component\Validator\Validator\ValidatorInterface $validator
     */
    public function __construct(
        int $refreshTokenTtl,
        RefreshTokenManagerInterface $refreshTokenManager,
        ValidatorInterface $validator
    ) {
        $this->ttl = $refreshTokenTtl;
        $this->refreshTokenManager = $refreshTokenManager;
        $this->validator = $validator;
    }

    /**
     * {@inheritDoc}
     */
    public function createForUser(User $user, ?int $ttl = null): string
    {
        $datetime = new \DateTimeImmutable();

        if ($ttl === null) {
            $datetime->modify('+' . $this->ttl . ' seconds');
        } else {
            $datetime->modify('+' . $ttl . ' seconds');
        }

        $refreshToken = $this->refreshTokenManager->create();

        $refreshToken->setUsername($user->getEmail());
        $refreshToken->setRefreshToken();
        $refreshToken->setValid($datetime);

        $valid = false;
        while ($valid === false) {
            $valid = true;
            $errors = $this->validator->validate($refreshToken);
            if ($errors->count() > 0) {
                foreach ($errors as $error) {
                    if ($error->getPropertyPath() === 'refreshToken') {
                        $valid = false;
                        $refreshToken->setRefreshToken();
                    }
                }
            }
        }

        $this->refreshTokenManager->save($refreshToken);

        return $refreshToken->getRefreshToken();
    }
}
