<?php
declare(strict_types=1);

namespace App\Common\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\RouterInterface;

class SitemapGenerator implements SitemapGeneratorInterface
{
    /**
     * @var string
     */
    private const PRODUCTION_DOMAIN = 'https://deinarzt.org';

    private EntityManagerInterface $entityManager;

    private RouterInterface $router;

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     * @param \Symfony\Component\Routing\RouterInterface $router
     */
    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \Exception
     */
    public function generateDynamic(string $entityClassName, string $uriResourceName, ?array $criteria = null): array
    {
        $urls = [];

        if (\class_exists($entityClassName) === false) {
            throw new \Exception(\sprintf(
                'There are no class with name - %s',
                $entityClassName
            ));
        }

        $entities = $this->entityManager->getRepository($entityClassName)->findBy($criteria ?? []);

        /** @var \App\Common\ApiResource\AbstractEntity $entity */
        foreach ($entities as $entity) {
            $url = [
                'loc' => $this->generateUrl($uriResourceName, $entity->getId()),
                'xhtmllinks' => [
                    [
                        'hreflang' => 'en',
                        'href' => self::PRODUCTION_DOMAIN . $this->generateUrl(
                            $uriResourceName,
                            $entity->getId(),
                            'en'
                        ),
                    ],
                    [
                        'hreflang' => 'de',
                        'href' => self::PRODUCTION_DOMAIN . $this->generateUrl(
                            $uriResourceName,
                            $entity->getId()
                        ),
                    ],
                ],
            ];
            if ($entity->getUpdatedAt() || $entity->getCreatedAt()) {
                if ($entity->getUpdatedAt()) {
                    $url['lastmod'] = $entity->getUpdatedAt()->format('Y-m-d');
                }
                if ($entity->getCreatedAt()) {
                    $url['lastmod'] = $entity->getCreatedAt()->format('Y-m-d');
                }
            }
            $urls[] = $url;
        }

        return $urls;
    }

    /**
     * @param string $uriResourceName
     * @param string $uri
     * @param string|null $language
     *
     * @return string
     */
    private function generateUrl(string $uriResourceName, string $uri, ?string $language = null): string
    {
        return \sprintf(
            '/%s%s%s',
            $language ? $language . '/' : '',
            $uriResourceName . '/',
            $uri
        );
    }

    /**
     * {@inheritDoc}
     */
    public function generateStatic(string $uri): array
    {
        $urls = [];
        $urls[] = [
            'loc' => $uri,
            'xhtmllinks' => [
                [
                    'hreflang' => 'en',
                    'href' => self::PRODUCTION_DOMAIN . '/en' . $uri,
                ],
                [
                    'hreflang' => 'de',
                    'href' => self::PRODUCTION_DOMAIN . $uri,
                ],
            ],
        ];

        return $urls;
    }
}
