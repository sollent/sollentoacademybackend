<?php

declare(strict_types=1);

namespace App\Common\Service\Websocket;

use GuzzleHttp\Exception\GuzzleException;
use Pusher\ApiErrorException;
use Pusher\Pusher;
use Pusher\PusherException;

final class WebsocketSenderService implements WebsocketSenderServiceInterface
{
    private Pusher $pusher;

    /**
     * @param Pusher $pusher
     */
    public function __construct(Pusher $pusher)
    {
        $this->pusher = $pusher;
    }

    /**
     * @throws PusherException
     * @throws ApiErrorException
     * @throws GuzzleException
     */
    public function send(string $chanelName, string $eventName, string $serializedData): void
    {
        $this->pusher->trigger($chanelName, $eventName, $serializedData);
    }
}