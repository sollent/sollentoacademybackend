<?php

declare(strict_types=1);

namespace App\Common\Service\Websocket;

use App\ChatMessage\Event\WebsocketEventInterface;

interface WebsocketSenderServiceInterface
{
    public function send(string $chanelName, string $eventName, string $serializedData): void;
}