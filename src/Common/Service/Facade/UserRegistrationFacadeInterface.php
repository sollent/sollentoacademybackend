<?php
declare(strict_types=1);

namespace App\Common\Service\Facade;

use App\Common\ApiResource\DTO\UserDTOInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Interface UserRegistrationFacadeInterface
 */
interface UserRegistrationFacadeInterface
{
    /**
     * @param \App\Common\ApiResource\DTO\UserDTOInterface $user
     *
     * @return \Symfony\Component\Security\Core\User\UserInterface
     *
     * @throws \Exception
     */
    public function registerUser(UserDTOInterface $user): UserInterface;
}
