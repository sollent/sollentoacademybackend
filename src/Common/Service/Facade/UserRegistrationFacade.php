<?php
declare(strict_types=1);

namespace App\Common\Service\Facade;

use App\Client\ApiResource\Client;
use App\Common\ApiResource\DTO\UserDTOInterface;
use App\Common\ApiResource\User;
use App\Common\Repository\UserRepositoryInterface;
use App\Common\Security\RolesInterface;
use App\Common\Service\LocaleResolverInterface;
use App\Manager\ApiResource\Manager;
use Doctrine\ORM\EntityManagerInterface;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

final class UserRegistrationFacade implements UserRegistrationFacadeInterface
{
    private EntityManagerInterface $entityManager;

    private UserPasswordHasherInterface $passwordHasher;

    private UserRepositoryInterface $userRepository;

    private LocaleResolverInterface $localeResolver;

    private TranslatorInterface $translator;

    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordHasher,
        UserRepositoryInterface $userRepository,
        LocaleResolverInterface $localeResolver,
        TranslatorInterface $translator
    ) {
        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
        $this->userRepository = $userRepository;
        $this->localeResolver = $localeResolver;
        $this->translator = $translator;
    }

    public function registerUser(UserDTOInterface $data): UserInterface
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'email' => $data->email,
        ]);

        if ($user) {
            throw new BadRequestHttpException(
                $this->translator->trans('exceptions.user.registation.user_exists')
            );
        }

        if ($data->type === UserDTOInterface::TYPE_CLIENT) {
            $user = new Client();
            $user->addRole(RolesInterface::ROLE_CLIENT);
        } elseif ($data->type === UserDTOInterface::TYPE_MANAGER) {
            $user = new Manager();
            $user->addRole(RolesInterface::ROLE_MANAGER);
        }

        if ($user === null) {
            throw new BadRequestException();
        }

        $user->setEmail($data->email);
        $user->setLogin($this->generateLogin());
        $user->setActive(false);

        $user->setGoogleId($data->googleId ?? null);
        $user->setFacebookId($data->facebookId ?? null);

        // Setting user password
        if ($data->plainPassword !== null) {
            $user->setPassword($this->passwordHasher->hashPassword($user, $data->plainPassword));
            $user->eraseCredentials();
        }

        // Generate user own referral code
        $user->setReferralCode(Uuid::uuid6()->toString());

        // Generate email confirmation code
        if ($this->socialRegistration($data)) {
            $user->setEmailConfirmed(true);
        } else {
            $user->setEmilConfirmationCode(Uuid::uuid6()->toString());
        }

        $user->setEmailConfirmed(true);

        // Set defaultLocale for user
        $user->setDefaultLocale($this->localeResolver->getCurrentLocale());

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return $user;
    }

    /**
     * @param \App\Common\ApiResource\DTO\UserDTOInterface $userDTO
     *
     * @return bool
     */
    private function socialRegistration(UserDTOInterface $userDTO): bool
    {
        if ($userDTO->googleId !== null || $userDTO->facebookId !== null) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    private function generateLogin(): string
    {
        // @TODO implement in future

        return (string)\time();
    }
}
