<?php
declare(strict_types=1);

namespace App\Common\Service;

use InvalidArgumentException;

final class StaticCodeGenerator
{
    /**
     * @return int
     *
     * @throws \Exception
     */
    public static function generateCode(): int
    {
        // will be implemented in the future
        return \random_int(100000, 999999);
    }

    /**
     * @param int $length
     *
     * @return int
     *
     * @throws \Exception
     */
    public static function generateFixedLengthCode(int $length): int
    {
        return \random_int(
            \intval(\sprintf('1%s', self::generateFixedStringDigital(0, $length - 1))),
            \intval(\sprintf('9%s', self::generateFixedStringDigital(9, $length - 1)))
        );
    }

    /**
     * @param int $digital
     * @param int $length
     *
     * @return string
     */
    private static function generateFixedStringDigital(int $digital, int $length): string
    {
        if (\preg_match('/\d/', (string)$digital) === false) {
            throw new InvalidArgumentException(
                \sprintf(
                    'Parameter $digital should contain only one number but has value \'%\'',
                    $digital
                )
            );
        }

        $result = (string)$digital;
        for ($i = 1; $i < $length; $i++) {
            $result .= $digital;
        }

        return $result;
    }
}
