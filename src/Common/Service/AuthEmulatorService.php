<?php
declare(strict_types=1);

namespace App\Common\Service;

use App\Admin\ApiResource\Admin;
use App\Common\ApiResource\User;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class AuthEmulatorService implements AuthEmulatorServiceInterface
{
    /**
     * @var string
     */
    private const FRONTEND_EMULATE_AUTH_URI = '/authentication-emulate';

    /**
     * @var int
     */
    private const TOKEN_TTL = 3600;

    private string $domainFront;

    private JWTTokenManagerInterface $jwtManager;

    private JWTEncoderInterface $jwtEncoder;

    /**
     * @param string $domainFront
     * @param \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface $jwtManager
     * @param \Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface $jwtEncoder
     */
    public function __construct(
        string $domainFront,
        JWTTokenManagerInterface $jwtManager,
        JWTEncoderInterface $jwtEncoder
    ) {
        $this->domainFront = $domainFront;
        $this->jwtManager = $jwtManager;
        $this->jwtEncoder = $jwtEncoder;
    }

    /**
     * {@inheritDoc}
     *
     * @throws \Exception
     */
    public function generatePathForUserEmulateAuthentication(User $user): string
    {
        if ($user instanceof Admin) {
            $message = '$user parameter should be \'Client\' or \'Manager\' types';

            throw new \Exception($message);
        }

        $jwtToken = $this->jwtManager->create($user);

        return $this->generatePath($jwtToken);
    }

    /**
     * @param \App\Common\ApiResource\User $user
     * @param int $tokenTtl
     *
     * @return string
     *
     * @throws \Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTEncodeFailureException
     */
    private function encodeJwtTokenWithCustomTtl(User $user, ?int $tokenTtl = null): string
    {
        return $this->jwtEncoder->encode([
            'username' => $user->getEmail(),
            'exp' => $tokenTtl ?? self::TOKEN_TTL,
        ]);
    }

    /**
     * @param string $jwtToken
     *
     * @return string
     */
    private function generatePath(string $jwtToken): string
    {
        return \sprintf(
            '%s%s%s',
            $this->domainFront,
            self::FRONTEND_EMULATE_AUTH_URI,
            '?token=' . $jwtToken
        );
    }
}
