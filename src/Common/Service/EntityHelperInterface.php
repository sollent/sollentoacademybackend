<?php
declare(strict_types=1);

namespace App\Common\Service;

use App\Common\ApiResource\AbstractEntity;

interface EntityHelperInterface
{
    /**
     * @param \App\Common\ApiResource\AbstractEntity $entity
     *
     * @return void
     */
    public function saveEntityChangeSet(AbstractEntity $entity): void;
}
