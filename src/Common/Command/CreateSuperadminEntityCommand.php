<?php
declare(strict_types=1);

namespace App\Common\Command;

use App\Admin\ApiResource\Admin;
use App\Common\Security\RolesInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Validator\Constraints\Email as EmailConstraint;
use Symfony\Component\Validator\Constraints\Length as LengthConstraint;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CreateSuperadminEntityCommand extends Command
{
    /**
     * @var int
     */
    public const MIN_PASSWORD_LENGTH = 15;

    /**
     * @var string
     */
    protected static $defaultName = 'superadmin:create';

    private EntityManagerInterface $entityManager;

    private UserPasswordHasherInterface $passwordHasher;

    private ValidatorInterface $validator;

    /**
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     * @param \Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface $passwordHasher
     * @param \Symfony\Component\Validator\Validator\ValidatorInterface $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordHasherInterface $passwordHasher,
        ValidatorInterface $validator
    ) {
        parent::__construct();

        $this->entityManager = $entityManager;
        $this->passwordHasher = $passwordHasher;
        $this->validator = $validator;
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $helper = $this->getHelper('question');

        $emailConstraint = new EmailConstraint();
        $email = $helper->ask($input, $output, new Question('Email: '));
        if ($email === null || \count($this->validator->validate($email, $emailConstraint)) > 0) {
            $output->writeln("<error>Invalid email</error>");

            return Command::INVALID;
        }

        $lengthConstraint = new LengthConstraint(null, self::MIN_PASSWORD_LENGTH);
        $pwQuestion = new Question('Password: ');
        $pwQuestion->setHidden(true);
        $password = $helper->ask($input, $output, $pwQuestion);
        if ($password === null || \count($this->validator->validate($password, $lengthConstraint))) {
            $output->writeln(\sprintf(
                "<error>Invalid password (Length should be greater than %s)</error>",
                self::MIN_PASSWORD_LENGTH
            ));

            return Command::INVALID;
        }

        $admin = new Admin();
        $admin->setEmail($email);
        $admin->setPassword($this->passwordHasher->hashPassword($admin, $password));
        $admin->addRole(RolesInterface::ROLE_SUPER_ADMIN);

        $this->entityManager->persist($admin);
        $this->entityManager->flush();

        $io = new SymfonyStyle($input, $output);
        $io->success('Super admin entity has been successfully created');

        return Command::SUCCESS;
    }
}
