<?php
declare(strict_types=1);

namespace App\Common\Security;

use App\Common\ApiResource\User;
use App\Common\Exception\UnauthorizedException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

final class SecurityContext implements SecurityContextInterface
{
    private ?TokenInterface $token = null;

    private ?User $user = null;

    public function getToken(): ?TokenInterface
    {
        return $this->token;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getUserOrFail(): User
    {
        if ($this->user === null) {
            throw UnauthorizedException::contextUserMissing();
        }

        return $this->user;
    }

    public function setToken(TokenInterface $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function setUser(?User $user = null): self
    {
        $this->user = $user;

        return $this;
    }
}
