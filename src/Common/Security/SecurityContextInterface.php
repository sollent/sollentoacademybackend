<?php
declare(strict_types=1);

namespace App\Common\Security;

use App\Common\ApiResource\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

interface SecurityContextInterface
{
    public function getToken(): ?TokenInterface;

    public function getUser(): ?User;

    /**
     * @throws \App\Common\Exception\UnauthorizedException
     */
    public function getUserOrFail(): User;

    public function setToken(TokenInterface $token): self;

    public function setUser(User $user): self;
}
