<?php
declare(strict_types=1);

namespace App\Common\Security;

/**
 * Interface RolesInterface
 */
interface RolesInterface
{
    /**
     * @var string[]
     */
    public const ALL_ROLES = [
        self::ROLE_CLIENT,
        self::ROLE_MANAGER,
        self::ROLE_SUPER_ADMIN,
    ];

    /**
     * @var string
     */
    public const ROLE_CLIENT = 'ROLE_CLIENT';

    /**
     * @var string
     */
    public const ROLE_MANAGER = 'ROLE_MANAGER';

    /**
     * @var string
     */
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
}
