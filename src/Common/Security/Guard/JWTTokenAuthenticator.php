<?php
declare(strict_types=1);

namespace App\Common\Security\Guard;

use App\Common\Security\SecurityContextInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Guard\JWTTokenAuthenticator as BaseAuthenticator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

final class JWTTokenAuthenticator extends BaseAuthenticator
{
    private SecurityContextInterface $securityContext;

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        /** @var \App\Common\ApiResource\User $user */
        $user = $token->getUser();

        $this->securityContext
            ->setToken($token)
            ->setUser($user);
    }

    /**
     * @required
     */
    public function setSecurityContext(SecurityContextInterface $securityContext): void
    {
        $this->securityContext = $securityContext;
    }
}
