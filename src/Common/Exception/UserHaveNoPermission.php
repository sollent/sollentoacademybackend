<?php
declare(strict_types=1);

namespace App\Common\Exception;

use EonX\EasyErrorHandler\Exceptions\ForbiddenException;

final class UserHaveNoPermission extends ForbiddenException
{
}
