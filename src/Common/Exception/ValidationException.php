<?php
declare(strict_types=1);

namespace App\Common\Exception;

use EonX\EasyErrorHandler\Exceptions\ValidationException as BaseValidationException;

final class ValidationException extends BaseValidationException
{
}
