<?php
declare(strict_types=1);

namespace App\Common\Exception;

use EonX\EasyErrorHandler\Exceptions\UnauthorizedException as BaseUnauthorizedException;

final class UnauthorizedException extends BaseUnauthorizedException
{
    public static function contextUserMissing(): self
    {
        return new self('exceptions.auth.context_user_missing');
    }
}
