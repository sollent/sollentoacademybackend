<?php
declare(strict_types=1);

namespace App\Common\Exception;

use EonX\EasyErrorHandler\Exceptions\NotFoundException;

final class EntityNotFoundException extends NotFoundException
{
    /**
     * @param string[] $notFoundIds
     */
    public static function notificationsNotFound(array $notFoundIds): self
    {
        $messageParams = ['ids' => \implode(', ', $notFoundIds)];

        $exception = new self('exceptions.notification.not_found');
        $exception
            ->setMessageParams($messageParams)
            ->setUserMessage('user_messages.notification.not_found')
            ->setUserMessageParams($messageParams);

        return $exception;
    }
}
