<?php
declare(strict_types=1);

namespace App\Common\Exception;

use EonX\EasyErrorHandler\Exceptions\ConflictException;

class EntityConflictException extends ConflictException
{
    // No body needed for now
}
