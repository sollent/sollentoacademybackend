<?php
declare(strict_types=1);

namespace App\Common\Controller\ResetPassword;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Common\ApiResource\DTO\ForgotPassword\ChangePasswordDTO;
use App\Common\ApiResource\DTO\ForgotPassword\RequestPasswordResetDTO;
use App\Common\ApiResource\DTO\ForgotPassword\VerifyPasswordResetTokenDTO;
use App\Common\ApiResource\User;
use App\Common\DTOs\ForgotPassword\DefaultPasswordResetOutput;
use App\Common\DTOs\ForgotPassword\RequestPasswordResetOutput;
use App\Common\Service\ResetPassword\UserResetPasswordServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/forgot-password")
 */
final class ResetPasswordController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private TranslatorInterface $translator;

    private ValidatorInterface $validator;

    private UserResetPasswordServiceInterface $userResetPasswordService;

    public function __construct(
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator,
        ValidatorInterface $validator,
        UserResetPasswordServiceInterface $userResetPasswordService
    ) {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
        $this->validator = $validator;
        $this->userResetPasswordService = $userResetPasswordService;
    }

    /**
     * Processes request for reset password
     *
     * @param \App\Common\ApiResource\DTO\ForgotPassword\RequestPasswordResetDTO $data
     *
     * @return \App\Common\DTOs\ForgotPassword\RequestPasswordResetOutput
     *
     * @Route(
     *     "/reset-password",
     *     methods={"POST"},
     *     name="request-password-reset-action",
     *     defaults={
     *          "_api_resource_class"=RequestPasswordResetDTO::class,
     *          "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function request(RequestPasswordResetDTO $data): RequestPasswordResetOutput
    {
        $this->validator->validate($data);
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['email' => $data->email]);

        if ($user === null) {
            throw new NotFoundHttpException($this->translator->trans('email_not_found'));
        }

        if ($user->isPasswordResetExpired() === false) {
            throw new BadRequestHttpException(
                $this->translator->trans('reset_password_mail_was_sent_in_prevoios_request')
            );
        }

        $this->userResetPasswordService->resetPassword($user);
        $this->entityManager->flush();

        return new RequestPasswordResetOutput();
    }

    /**
     * Validates and process the reset URL that the user clicked in their email.
     *
     * @param \App\Common\ApiResource\DTO\ForgotPassword\ChangePasswordDTO $data
     *
     * @return \App\Common\DTOs\ForgotPassword\DefaultPasswordResetOutput
     *
     * @Route(
     *     "/change-password",
     *     methods={"POST"},
     *     name="change-password-action",
     *     defaults={
     *          "_api_resource_class"=ChangePasswordDTO::class,
     *          "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function reset(ChangePasswordDTO $data): DefaultPasswordResetOutput
    {
        $this->validator->validate($data);
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'passwordResetToken' => $data->token,
        ]);

        if ($user === null) {
            throw new NotFoundHttpException($this->translator->trans('email_not_found'));
        }

        $this->userResetPasswordService->changePassword($user, $data->newPassword);
        $this->entityManager->flush();

        return new DefaultPasswordResetOutput();
    }

    /**
     * Verifies password reset token
     *
     * @param \App\Common\ApiResource\DTO\ForgotPassword\VerifyPasswordResetTokenDTO $data
     *
     * @return \App\Common\DTOs\ForgotPassword\DefaultPasswordResetOutput
     *
     * @Route(
     *     "/verify-reset-token",
     *     methods={"POST"},
     *     name="verify-reset-password-token-action",
     *     defaults={
     *          "_api_resource_class"=VerifyPasswordResetTokenDTO::class,
     *          "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function verifyToken(VerifyPasswordResetTokenDTO $data): DefaultPasswordResetOutput
    {
        $this->validator->validate($data);
        $user = $this->entityManager->getRepository(User::class)->findOneBy([
            'passwordResetToken' => $data->token,
        ]);

        if ($user === null) {
            throw new NotFoundHttpException($this->translator->trans('email_not_found'));
        }

        return new DefaultPasswordResetOutput();
    }
}
