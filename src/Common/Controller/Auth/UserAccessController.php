<?php
declare(strict_types=1);

namespace App\Common\Controller\Auth;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Serializer\SerializerInterface;

final class UserAccessController extends AbstractController
{
    private TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Route(
     *     "/api/user-by-token",
     *     methods={"GET"}
     * )
     */
    public function accessByToken(Request $request, SerializerInterface $serializer): JsonResponse
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user === null) {
            throw new AccessDeniedHttpException();
        }

        $userArray = \json_decode(
            $serializer->serialize($user, 'json'),
            true
        );

        return JsonResponse::fromJsonString(\json_encode($userArray));
    }
}
