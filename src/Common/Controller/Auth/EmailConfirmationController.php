<?php
declare(strict_types=1);

namespace App\Common\Controller\Auth;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Common\ApiResource\DTO\EmailConfirmationDTO;
use App\Common\ApiResource\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

final class EmailConfirmationController extends AbstractController
{
    private TranslatorInterface $translator;

    /**
     * @param \Symfony\Contracts\Translation\TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @Route(
     *     "/confirm-email",
     *     methods={"POST", "OPTIONS"},
     *     name="email-confirmation-action",
     *     defaults={
     *          "_api_resource_class"=EmailConfirmationDTO::class,
     *          "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function auth(EmailConfirmationDTO $data, ValidatorInterface $validator): JsonResponse
    {
        $validator->validate($data);

        $em = $this->getDoctrine()->getManager();

        $existingUser = $em->getRepository(User::class)->findOneBy([
            'emilConfirmationCode' => $data->emailConfirmationCode,
        ]);

        if ($existingUser === null) {
            throw new NotFoundHttpException(
                $this->translator->trans(
                    'exceptions.user.email_confirmation.no_user_with_such_email_confirmation_code'
                )
            );
        }

        $existingUser
            ->setEmailConfirmed(true)
            ->setEmilConfirmationCode(null);

        $em->flush();

        return new JsonResponse(
            ['message' => 'Email has been successfully confirmed'],
            Response::HTTP_OK
        );
    }
}
