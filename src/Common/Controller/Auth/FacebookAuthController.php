<?php
declare(strict_types=1);

namespace App\Common\Controller\Auth;

use ApiPlatform\Core\Validator\ValidatorInterface;
use App\Common\ApiResource\DTO\FacebookAuthDTO;
use App\Common\DTOs\Auth\FacebookAuthOutput;
use App\Common\Service\Auth\SocialAuthenticatorInterface;
use App\Common\Service\Security\RefreshTokenServiceInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

final class FacebookAuthController extends AbstractController
{
    private JWTTokenManagerInterface $jwtManager;

    private SocialAuthenticatorInterface $socialAuthenticator;

    /**
     * @param \App\Common\ApiResource\DTO\FacebookAuthDTO $data
     * @param \App\Common\Service\Security\RefreshTokenServiceInterface $refreshTokenService
     * @param \ApiPlatform\Core\Validator\ValidatorInterface $validator
     * @param \Symfony\Component\Serializer\SerializerInterface $serializer
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @throws \Exception
     *
     * @Route(
     *     "/auth/facebook",
     *     methods={"POST", "OPTIONS"},
     *     name="auth-facebook-action",
     *     defaults={
     *          "_api_resource_class"=FacebookAuthDTO::class,
     *          "_api_collection_operation_name"="post"
     *     }
     * )
     */
    public function auth(
        FacebookAuthDTO $data,
        RefreshTokenServiceInterface $refreshTokenService,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    ): JsonResponse {
        $validator->validate($data);

        $user = $this->socialAuthenticator->authenticate($data->accessToken, $data->type, $data->referralCode);

        $facebookOutput = new FacebookAuthOutput(
            $this->jwtManager->create($user),
            $refreshTokenService->createForUser($user)
        );

        return JsonResponse::fromJsonString($serializer->serialize($facebookOutput, 'json'));
    }

    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/facebook", name="connect_facebook_start")
     */
    public function connectAction(ClientRegistry $clientRegistry): RedirectResponse
    {
        return $clientRegistry
            ->getClient('facebook')
            ->redirect([
                'email',
                'profile',
            ]);
    }

    /**
     * After going to Facebook, you're redirected back here
     * because this is the "redirect_route" you configured
     * in config/packages/knpu_oauth2_client.yaml
     *
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     */
    public function connectCheckAction(Request $request, ClientRegistry $clientRegistry): RedirectResponse
    {
        return $this->redirectToRoute('your_homepage_route');
    }

    public function setJwtManager(JWTTokenManagerInterface $jwtManager): void
    {
        $this->jwtManager = $jwtManager;
    }

    public function setSocialAuthenticator(SocialAuthenticatorInterface $socialAuthenticator): void
    {
        $this->socialAuthenticator = $socialAuthenticator;
    }
}
