<?php
declare(strict_types=1);

namespace App\Common\Controller\ApiPlatform\Media;

use App\Common\ApiResource\MediaObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Vich\UploaderBundle\Storage\StorageInterface;

final class CreateMediaObjectAction
{
    private StorageInterface $storage;

    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    public function __invoke(Request $request): MediaObject
    {
        /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        if ($uploadedFile === null) {
            throw new BadRequestHttpException('exceptions.media_object.file_required');
        }

        $mediaObject = new MediaObject();
        $mediaObject->setFile($uploadedFile);
        $resolveUri = $this->storage->resolveUri($mediaObject, 'file');
        if ($resolveUri) {
            $resolveUri = \substr($resolveUri, 1);
        }

        $mediaObject->setContentUrl($resolveUri);

        return $mediaObject;
    }
}
