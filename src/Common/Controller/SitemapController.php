<?php
declare(strict_types=1);

namespace App\Common\Controller;

use App\Common\Service\SitemapGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @var string
     */
    private const PRODUCTION_DOMAIN = 'https://deinarzt.org';

    private SitemapGeneratorInterface $sitemapGenerator;

    /**
     * @param \App\Common\Service\SitemapGeneratorInterface $sitemapGenerator
     */
    public function __construct(SitemapGeneratorInterface $sitemapGenerator)
    {
        $this->sitemapGenerator = $sitemapGenerator;
    }

    /**
     * @Route(
     *     "/sitemap.xml",
     *     name="sitemap",
     *     defaults={"_format"="xml"}
     *)
     */
    public function sitemapAction(Request $request): Response
    {
    }
}
