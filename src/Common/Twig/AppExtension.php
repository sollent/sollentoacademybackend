<?php
declare(strict_types=1);

namespace App\Common\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

final class AppExtension extends AbstractExtension
{
    private string $domain;

    private string $domainFront;

    /**
     * @param string $domain
     * @param string $domainFront
     */
    public function __construct(string $domain, string $domainFront)
    {
        $this->domain = $domain;
        $this->domainFront = $domainFront;
    }

    /**
     * {@inheritDoc}
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('build_front_url', [$this, 'buildFrontUrl']),
            new TwigFunction('build_back_url', [$this, 'buildBackUrl']),
        ];
    }

    /**
     * @param string $uri
     * @param string|null $locale
     *
     * @return string
     */
    public function buildFrontUrl(string $uri, ?string $locale = null): string
    {
        return $this->buildUrl($this->domainFront, $uri, $locale);
    }

    /**
     * @param string $uri
     * @param string|null $locale
     *
     * @return string
     */
    public function buildBackUrl(string $uri, ?string $locale = null): string
    {
        return $this->buildUrl($this->domain, $uri, $locale);
    }

    /**
     * @param string $domain
     * @param string $uri
     * @param string|null $locale
     *
     * @return string
     */
    private function buildUrl(string $domain, string $uri, ?string $locale = null): string
    {
        if ($locale !== null) {
            $locale = "/$locale";
        } else {
            $locale = "";
        }

        return \sprintf('%s%s%s', $domain, $locale, $uri);
    }
}
