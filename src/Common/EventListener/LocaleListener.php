<?php
declare(strict_types=1);

namespace App\Common\EventListener;

use App\Common\Service\LocaleResolverInterface;
use Gedmo\Translatable\TranslatableListener;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class LocaleListener implements EventSubscriberInterface
{
    private ?string $currentLocale = null;

    private LocaleResolverInterface $localeResolver;

    private TranslatableListener $translatableListener;

    /**
     * @param \Gedmo\Translatable\TranslatableListener $translatableListener
     * @param \App\Common\Service\LocaleResolverInterface $localeResolver
     */
    public function __construct(TranslatableListener $translatableListener, LocaleResolverInterface $localeResolver)
    {
        $this->translatableListener = $translatableListener;
        $this->localeResolver = $localeResolver;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 500]],
            KernelEvents::RESPONSE => ['setContentLanguage'],
        ];
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $this->translatableListener->setPersistDefaultLocaleTranslation(true);

        $request = $event->getRequest();
        $request->setLocale($this->localeResolver->getCurrentLocale());

        // Set currentLocale
        $this->translatableListener->setTranslatableLocale($request->getLocale());
        $this->currentLocale = $request->getLocale();
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function setContentLanguage(ResponseEvent $event): Response
    {
        $response = $event->getResponse();
        $response->headers->add(['Content-Language' => $this->currentLocale]);

        return $response;
    }
}
