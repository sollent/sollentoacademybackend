<?php
declare(strict_types=1);

namespace App\Common\Serializer;

use App\Common\ApiResource\MediaObject;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;
use Vich\UploaderBundle\Storage\StorageInterface;

final class MediaObjectNormalizer implements ContextAwareNormalizerInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;

    private const ALREADY_CALLED = 'MEDIA_OBJECT_NORMALIZER_ALREADY_CALLED';

    private StorageInterface $storage;

    /**
     * @param \Vich\UploaderBundle\Storage\StorageInterface $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsNormalization($data, ?string $format = null, ?array $context = null): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }

        return $data instanceof MediaObject;
    }

    /**
     * {@inheritDoc}
     */
    public function normalize($object, ?string $format = null, ?array $context = null)
    {
        $context[self::ALREADY_CALLED] = true;
        $resolveUri = $this->storage->resolveUri($object, 'file');
        if ($resolveUri) {
            $resolveUri = \substr($resolveUri, 1);
        }

        $object->setContentUrl($resolveUri);

        return $this->normalizer->normalize($object, $format, $context);
    }
}
