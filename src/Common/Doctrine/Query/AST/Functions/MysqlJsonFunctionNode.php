<?php
declare(strict_types=1);

namespace App\Common\Doctrine\Query\AST\Functions;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Platforms\MySqlPlatform;
use Doctrine\ORM\Query\SqlWalker;

abstract class MysqlJsonFunctionNode extends AbstractJsonFunctionNode
{
    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     *
     * @throws \Doctrine\DBAL\Exception
     */
    protected function validatePlatform(SqlWalker $sqlWalker): void
    {
        if (($sqlWalker->getConnection()->getDatabasePlatform() instanceof MySqlPlatform) === false) {
            throw Exception::notSupported(static::FUNCTION_NAME);
        }
    }
}
