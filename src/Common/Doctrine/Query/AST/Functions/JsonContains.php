<?php
declare(strict_types=1);

namespace App\Common\Doctrine\Query\AST\Functions;

/**
 * "JSON_CONTAINS" "(" StringPrimary "," StringPrimary {"," StringPrimary } ")"
 */
class JsonContains extends MysqlJsonFunctionNode
{
    public const FUNCTION_NAME = 'JSON_CONTAINS';

    protected bool $allowOptionalArgumentRepeat = false;

    /**
     * @var string[]
     */
    protected array $optionalArgumentTypes = [self::STRING_PRIMARY_ARG];

    /**
     * @var string[]
     */
    protected array $requiredArgumentTypes = [self::STRING_PRIMARY_ARG, self::STRING_PRIMARY_ARG];
}
