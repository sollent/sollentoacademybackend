<?php
declare(strict_types=1);

namespace App\Common\Subscriber\EntitySubscriber;

use App\Common\Service\LocaleResolverInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;

final class EntityTranslationSubscriber implements EventSubscriber
{
    private EntityManagerInterface $entityManager;

    private LocaleResolverInterface $localeResolver;

    /**
     * @param \App\Common\Service\LocaleResolverInterface $localeResolver
     * @param \Doctrine\ORM\EntityManagerInterface $entityManager
     */
    public function __construct(LocaleResolverInterface $localeResolver, EntityManagerInterface $entityManager)
    {
        $this->localeResolver = $localeResolver;
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritDoc}
     */
    public function getSubscribedEvents(): array
    {
        return ['postPersist'];
    }

    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $event
     */
    public function postPersist(LifecycleEventArgs $event): void
    {
        $entity = $event->getEntity();
        $availableLocales = $this->localeResolver->getAvailableLocales();

        // Can be optimized (using reflection) to be able placed translation entity
        // anywhere and not in the same namespace with the root entity
        if (\class_exists($entityTranslationClass = \sprintf('%s%s', \get_class($entity), 'Translation')) === false) {
            return;
        }
        $translationRepository = $this->entityManager->getRepository($entityTranslationClass);

        $translations = $translationRepository->findTranslationsByObjectId($entity->getId());
        if (\count($translations) === 0) {
            return;
        }

        foreach ($availableLocales as $locale) {
            if (isset($translations[$locale]) === false) {
                foreach ($translations[\array_key_first($translations)] as $fieldName => $fieldValue) {
                    $translationRepository->translate($entity, $fieldName, $locale, $fieldValue);
                    $this->entityManager->flush();
                }
            }
        }
    }
}
