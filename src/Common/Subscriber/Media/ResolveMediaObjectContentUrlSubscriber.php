<?php
declare(strict_types=1);

namespace App\Common\Subscriber\Media;

use ApiPlatform\Core\EventListener\EventPriorities;
use ApiPlatform\Core\Util\RequestAttributesExtractor;
use App\Common\ApiResource\MediaObject;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Vich\UploaderBundle\Storage\StorageInterface;

/**
 * Class ResolveMediaObjectContentUrlSubscriber
 */
final class ResolveMediaObjectContentUrlSubscriber implements EventSubscriberInterface
{
    private StorageInterface $storage;

    /**
     * ResolveMediaObjectContentUrlSubscriber constructor.
     *
     * @param \Vich\UploaderBundle\Storage\StorageInterface $storage
     */
    public function __construct(StorageInterface $storage)
    {
        $this->storage = $storage;
    }

    /**
     * {@inheritDoc}
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::VIEW => ['onPreSerialize', EventPriorities::PRE_SERIALIZE],
        ];
    }

    /**
     * @param \Symfony\Component\HttpKernel\Event\ViewEvent $event
     */
    public function onPreSerialize(ViewEvent $event): void
    {
        $controllerResult = $event->getControllerResult();
        $request = $event->getRequest();

        if ($controllerResult instanceof Response || $request->attributes->getBoolean('_api_respond', true) === false) {
            return;
        }

        $attributes = RequestAttributesExtractor::extractAttributes($request);
        if (\count($attributes) === 0 || \is_a($attributes['resource_class'], MediaObject::class, true) === false) {
            return;
        }

        $mediaObjects = $controllerResult;

        if (\is_iterable($mediaObjects) === false) {
            $mediaObjects = [$mediaObjects];
        }

        foreach ($mediaObjects as $mediaObject) {
            if ($mediaObject instanceof MediaObject === false) {
                continue;
            }

            $mediaObject->setContentUrl($this->storage->resolveUri($mediaObject, 'file'));
        }
    }
}
