<?php
declare(strict_types=1);

namespace App\Common\OpenApi;

use ApiPlatform\Core\OpenApi\OpenApi;

trait TagsSortingTrait
{
    /**
     * @param \ApiPlatform\Core\OpenApi\OpenApi $openApi
     *
     * @return \ApiPlatform\Core\OpenApi\OpenApi
     */
    protected function sortingWithTags(OpenApi $openApi): OpenApi
    {
        $paths = $openApi->getPaths()->getPaths();

        $sortingSchema = $this->provideSortingTagsSchema();

        /** @var \ApiPlatform\Core\OpenApi\Model\PathItem $pathItem */
        foreach ($paths as $path => $pathItem) {
            foreach ($sortingSchema as $sortingPath => $sortingPathOperations) {
                if ($path === $sortingPath) {
                    foreach ($sortingPathOperations as $pathOperationName => $pathOperationInner) {
                        if (isset($pathOperationInner['tags']) === false) {
                            continue;
                        }

                        $withMethod = \sprintf('with%s', \ucfirst($pathOperationName));
                        $getter = \sprintf('get%s', \ucfirst($pathOperationName));
                        if (
                            \is_callable([$pathItem, $getter]) === false ||
                            \is_callable([$pathItem, $withMethod]) === false
                        ) {
                            continue;
                        }

                        $pathItem = $pathItem
                            ->{$withMethod}($pathItem->{$getter}()->withTags($pathOperationInner['tags']));
                        $openApi->getPaths()->addPath($path, $pathItem);
                    }
                }
            }
        }

        return $openApi;
    }

    /**
     * Provide schema with api resources and tag needs for sorting
     * Should be like
     * [
     *      "/api/example_resources" => [
     *          "get" => [
     *              "tags" => ["TagName"]
     *          ],
     *          "post" => [
     *              "tags" => ["TagName"]
     *          ]
     *      ],
     *      "/api/example_resources/{id}" => [
     *          "put" => [
     *              "tags" => ["TagName"]
     *          ],
     *          "delete" => [
     *              "tags" => ["TagName"]
     *          ]
     *      ]
     * ]
     *
     * @return array
     */
    abstract protected function provideSortingTagsSchema(): array;
}
