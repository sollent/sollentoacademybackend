<?php
declare(strict_types=1);

namespace App\Common\OpenApi;

use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use ApiPlatform\Core\OpenApi\Model;
use ApiPlatform\Core\OpenApi\OpenApi;
use App\Common\Service\OpenApi\PermissionDescriberServiceInterface;

final class OpenApiFactoryDecorator implements OpenApiFactoryInterface
{
    use TagsSortingTrait;

    private OpenApiFactoryInterface $decorated;

    private PermissionDescriberServiceInterface $permissionDescriberService;

    public function __construct(
        OpenApiFactoryInterface $decorated,
        PermissionDescriberServiceInterface $permissionDescriberService
    ) {
        $this->decorated = $decorated;
        $this->permissionDescriberService = $permissionDescriberService;
    }

    public function __invoke(?array $context = null): OpenApi
    {
        if ($context === null) {
            $context = [];
        }

        $openApi = ($this->decorated)($context);
        $openApi = $openApi->withInfo((new Model\Info('SollentoAcademy', 'v1', 'Description of SollentoAcademy API')));

        $schemas = $openApi->getComponents()->getSchemas();

        $schemas['Authentication'] = new \ArrayObject([
            'properties' => [
                'refresh_token' => [
                    'readOnly' => true,
                    'type' => 'string',
                ],
                'token' => [
                    'readOnly' => true,
                    'type' => 'string',
                ],
            ],
            'type' => 'object',
        ]);

        $schemas['Credentials'] = new \ArrayObject([
            'properties' => [
                'email' => [
                    'example' => 'johndoe@example.com',
                    'required' => true,
                    'type' => 'string',
                ],
                'password' => [
                    'example' => 'password',
                    'required' => true,
                    'type' => 'string',
                ],
            ],
            'type' => 'object',
        ]);

        $schemas['RefreshTokenBody'] = new \ArrayObject([
            'properties' => [
                'refresh_token' => [
                    'example' => '3b1ccffab485587de7ff935a468b40a27260c1699324c0892ae3db691006979ea76f594f1e2d3' .
                        'b44adf50b8094a676f72e6c66351c133f75cd6436e85a625469',
                    'type' => 'string',
                ],
            ],
            'type' => 'object',
        ]);
        $schemas['GoogleAuthBody'] = new \ArrayObject([
            'properties' => [
                'accessToken' => [
                    'example' => '3b1ccffab485587de7ff935a468b40a27260c1699324c0892ae3db691006979ea76f594f1e2d3b' .
                        '44adf50b8094a676f72e6c66351c133f75cd6436e85a625469',
                    'type' => 'string',
                ],
            ],
            'type' => 'object',
        ]);

        $pathItemToken = new Model\PathItem(
            'JWT Token',
            null,
            null,
            null,
            null,
            new Model\Operation(
                'postCredentialsItem',
                ['Authentication'],
                [
                    '200' => [
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Authentication',
                                ],
                            ],
                        ],
                        'description' => 'Get JWT token',
                    ],
                ],
                'Get JWT token to login.',
                '',
                null,
                [],
                new Model\RequestBody(
                    'Generate new JWT Token',
                    new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/Credentials',
                            ],
                        ],
                    ]),
                ),
            )
        );

        $pathItemRefreshToken = new Model\PathItem(
            'Refresh JWT Token',
            null,
            null,
            null,
            null,
            new Model\Operation(
                'RefreshToken',
                ['RefreshToken'],
                [
                    '200' => [
                        'content' => [
                            'application/json' => [
                                'schema' => [
                                    '$ref' => '#/components/schemas/Token',
                                ],
                            ],
                        ],
                        'description' => 'Refresh JWT token',
                    ],
                ],
                'Refresh JWT token to login.',
                '',
                null,
                [],
                new Model\RequestBody(
                    'Refresh JWT Token',
                    new \ArrayObject([
                        'application/json' => [
                            'schema' => [
                                '$ref' => '#/components/schemas/RefreshTokenBody',
                            ],
                        ],
                    ]),
                ),
            )
        );

        $pathItemUserAccess = new Model\PathItem(
            'Accessing user by TOKEN',
            null,
            null,
            new Model\Operation(
                'accessUserByToken',
                ['AccessUser'],
                [
                    '200' => [
                        'description' => 'Get user by token',
                    ],
                ],
                '',
                '',
                null,
                [],
            )
        );

        $openApi->getPaths()->addPath('/auth', $pathItemToken);
        $openApi->getPaths()->addPath('/token/refresh', $pathItemRefreshToken);
        $openApi->getPaths()->addPath('/api/user-by-token', $pathItemUserAccess);

        $openApi = $this->permissionDescriberService->describePathsPermission($openApi);

        return $this->sortingWithTags($openApi);
    }

    /**
     * {@inheritDoc}
     */
    protected function provideSortingTagsSchema(): array
    {
        $registrationTag = 'Registration';
        $authorizationTag = 'Authorization';
        $forgotPasswordTag = 'ForgotPassword';

        return [
            // Registration tag
            '/api/users' => [
                'post' => ['tags' => [$registrationTag]],
            ],
            // Authorization tag
            '/auth' => [
                'post' => ['tags' => [$authorizationTag]],
            ],
            '/auth/facebook' => [
                'post' => ['tags' => [$authorizationTag]],
            ],
            '/auth/google' => [
                'post' => ['tags' => [$authorizationTag]],
            ],
            '/token/refresh' => [
                'post' => ['tags' => [$authorizationTag]],
            ],
            // ForgotPassword tag
            '/forgot-password/reset-password' => [
                'post' => ['tags' => [$forgotPasswordTag]],
            ],
            '/forgot-password/verify-reset-token' => [
                'post' => ['tags' => [$forgotPasswordTag]],
            ],
            '/forgot-password/change-password' => [
                'post' => ['tags' => [$forgotPasswordTag]],
            ],
        ];
    }
}
