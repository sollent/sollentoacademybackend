#!/bin/sh

rm -rf ./var/cache/*
php bin/console --env=dev cache:clear --no-warmup
php bin/console --env=dev doctrine:cache:clear-metadata
php bin/console --env=dev doctrine:cache:clear-query
php bin/console --env=dev doctrine:cache:clear-result
php bin/console --env=dev cache:pool:clear cache.global_clearer
composer dump-autoload