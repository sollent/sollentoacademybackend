<?php
declare(strict_types=1);

use EonX\EasyQuality\Sniffs\Arrays\AlphabeticallySortedArrayKeysSniff;
use EonX\EasyQuality\Sniffs\Classes\AvoidPrivatePropertiesSniff;
use EonX\EasyQuality\Sniffs\Classes\AvoidPublicPropertiesSniff;
use EonX\EasyQuality\Sniffs\Classes\MakeClassAbstractSniff;
use EonX\EasyQuality\Sniffs\Commenting\DocCommentSpacingSniff;
use EonX\EasyQuality\Sniffs\ControlStructures\ArrangeActAssertSniff;
use EonX\EasyQuality\Sniffs\ControlStructures\LinebreakAfterEqualsSignSniff;
use EonX\EasyQuality\Sniffs\Exceptions\ThrowExceptionMessageSniff;
use EonX\EasyQuality\ValueObject\EasyQualitySetList;
use PhpCsFixer\Fixer\PhpTag\BlankLineAfterOpeningTagFixer;
use SlevomatCodingStandard\Sniffs\Classes\RequireSingleLineMethodSignatureSniff;
use SlevomatCodingStandard\Sniffs\Functions\StaticClosureSniff;
use SlevomatCodingStandard\Sniffs\Namespaces\FullyQualifiedClassNameInAnnotationSniff;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symplify\EasyCodingStandard\ValueObject\Option;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();

    $containerConfigurator->import(EasyQualitySetList::ECS);

    $parameters->set(Option::PATHS, [__DIR__ . '/src', __DIR__ . '/tests', __DIR__ . '/migrations']);

    $parameters->set(Option::SKIP, [
        ArrangeActAssertSniff::class => null,
        AvoidPrivatePropertiesSniff::class => null,
        BlankLineAfterOpeningTagFixer::class => null,
        AvoidPublicPropertiesSniff::class => [
            'DTOs/*',
            'ApiResource/DTO/*',
            'tests/ApiResourceSecurityContext.php',
        ],
        'SlevomatCodingStandard\Sniffs\Commenting\InlineDocCommentDeclarationSniff.MissingVariable' => ['tests/Factory/*'],
        'SlevomatCodingStandard\Sniffs\Commenting\InlineDocCommentDeclarationSniff.NoAssignment' => ['tests/Factory/*'],
        'SlevomatCodingStandard\Sniffs\TypeHints\PropertyTypeHintSniff.MissingTraversableTypeHintSpecification' => ['src/*'],
        StaticClosureSniff::class => ['tests/*'],
        FullyQualifiedClassNameInAnnotationSniff::class => [
            'tests/EntityFactoryTrait.php',
            'tests/DatabaseEntityHelperTrait.php',
        ],
        AlphabeticallySortedArrayKeysSniff::class => [
            'tests/*',
        ],
    ]);

    $services = $containerConfigurator->services();

//    $services->set(AlphabeticallySortedArrayKeysSniff::class);
    $services->set(ArrangeActAssertSniff::class)
        ->property('testNamespace', 'Test');
    $services->set(AvoidPrivatePropertiesSniff::class);
    $services->set(AvoidPublicPropertiesSniff::class);
    //$services->set(DateTimeImmutableFixer::class);
    $services->set(DocCommentSpacingSniff::class)
        ->property('linesCountBetweenAnnotationsGroups', 1)
        ->property('annotationsGroups', [
            '@param',
            '@return ',
            '@throws',
            '@Annotation',
            '@ApiFilter',
            '@ApiProperty',
            '@ApiResource',
            '@AppAssert*',
            '@EasyCoreAssert\\',
            '@Assert\\',
            '@codeCoverageIgnore',
            '@covers',
            '@coversNothing',
            '@deprecated',
            '@Groups',
            '@Ignore',
            '@method',
            '@ORM\\',
            '@property',
            '@noinspection',
            '@phpstan-param',
            '@phpstan-return',
            '@phpstan-template',
            '@phpstan-var',
            '@see',
            '@SuppressWarnings',
            '@UniqueEntity',
            '@var',
        ]);
    $services->set(LinebreakAfterEqualsSignSniff::class);
    $services->set(MakeClassAbstractSniff::class)
        ->property('applyTo', [
            [
                'namespace' => '/^Test/',
                'patterns' => [
                    '/.*TestCase$/',
                ],
            ],
        ]);
    $services->set(RequireSingleLineMethodSignatureSniff::class)
        ->property('maxLineLength', 120);
    $services->set(StaticClosureSniff::class);
    $services->set(ThrowExceptionMessageSniff::class);
};
