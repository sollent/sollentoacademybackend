<?php
declare(strict_types=1);

namespace Test;

use App\Common\ApiResource\AbstractEntity;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Generator;
use League\FactoryMuffin\Stores\RepositoryStore;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

trait EntityFactoryTrait
{
    protected static ?FactoryMuffinWrapper $entityFactory = null;

    /**
     * @param mixed[]|null $parameters
     *
     * @phpstan-param class-string<TEntity> $entityClass
     *
     * @phpstan-return TEntity
     *
     * @phpstan-template TEntity of \App\ApiResource\AbstractEntity
     */
    protected function createEntity(string $entityClass, ?array $parameters = null): AbstractEntity
    {
        /**
         * @phpstan-template TEntity of \App\ApiResource\AbstractEntity
         *
         * @phpstan-var TEntity $entity
         *
         * @var AbstractEntity $entity
         */
        $entity = self::$entityFactory->create($entityClass, $parameters ?? []);

        return $entity;
    }

    /**
     * @param mixed[]|null $parameters
     *
     * @return AbstractEntity[]
     */
    protected function createEntityList(int $times, string $entityClass, ?array $parameters = null): array
    {
        /** @var AbstractEntity[] $entities */
        $entities = self::$entityFactory->seed($times, $entityClass, $parameters ?? []);

        return $entities;
    }

    /**
     * @param mixed[] $attrsToGet
     * @param mixed[]|null $attrsToOverride
     *
     * @return mixed[]
     *
     * @noinspection PhpDocMissingThrowsInspection
     *
     * @phpstan-param class-string<AbstractEntity> $entityClass
     */
    protected function getEntityRaw(string $entityClass, array $attrsToGet, ?array $attrsToOverride = null): array
    {
        $entity = $this->makeEntity($entityClass);

        /** @noinspection PhpUnhandledExceptionInspection */
        return \array_merge($this->normalizeEntity($entity, $attrsToGet), $attrsToOverride ?? []);
    }

    /**
     * @param mixed[]|null $parameters
     *
     * @phpstan-param class-string <TEntity> $entityClass
     *
     * @phpstan-return TEntity
     *
     * @phpstan-template TEntity of \App\ApiResource\AbstractEntity
     */
    protected function makeEntity(string $entityClass, ?array $parameters = null): AbstractEntity
    {
        /**
         * @phpstan-template TEntity of \App\ApiResource\AbstractEntity
         *
         * @phpstan-var TEntity $entity
         *
         * @var AbstractEntity $entity
         */
        $entity = self::$entityFactory->instance($entityClass, $parameters ?? []);

        return $entity;
    }

    /**
     * @param mixed[]|null $attrsToNormalize
     *
     * @return mixed[]
     *
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    protected function normalizeEntity(AbstractEntity $entity, ?array $attrsToNormalize = null): array
    {
        $serializer = new Serializer([new DateTimeNormalizer(), new ObjectNormalizer()]);

        return (array)$serializer->normalize($entity, null, ['attributes' => $attrsToNormalize]);
    }

    private function initFactories(?Generator $faker = null): void
    {
        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        self::$entityFactory = new FactoryMuffinWrapper(new RepositoryStore($entityManager), null, $faker);
        /** @noinspection PhpUnhandledExceptionInspection */
        self::$entityFactory->loadFactories(__DIR__ . '/Factory');
    }
}
