<?php
declare(strict_types=1);

namespace Test;

use EonX\EasyTest\InvalidDataMaker\InvalidDataMaker as EonxInvalidDataMaker;

final class InvalidDataMaker extends EonxInvalidDataMaker
{
    /**
     * @param mixed|null $itemValue
     *
     * @return iterable<mixed>
     */
    public function yieldArrayWithDuplicatedItems($itemValue): iterable
    {
        $value = [$itemValue, $itemValue];
        $message = 'This collection should contain only unique elements.';

        yield from $this->create("{$this->property} has duplicated elements in the array", $value, $message);
    }
}
