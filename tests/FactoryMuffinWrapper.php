<?php
declare(strict_types=1);

namespace Test;

use Faker\Factory;
use Faker\Generator;
use League\FactoryMuffin\Definition;
use League\FactoryMuffin\Exceptions\DirectoryNotFoundException;
use League\FactoryMuffin\FactoryMuffin;
use League\FactoryMuffin\Generators\GeneratorFactory;
use League\FactoryMuffin\Stores\StoreInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

final class FactoryMuffinWrapper extends FactoryMuffin
{
    private Faker $faker;

    public function __construct(
        ?StoreInterface $store = null,
        ?GeneratorFactory $factory = null,
        ?Generator $faker = null
    ) {
        parent::__construct($store, $factory);
        $this->faker = new Faker($faker ?? Factory::create());
    }

    /**
     * @param string $name
     *
     * @noinspection PhpDocMissingThrowsInspection
     * @noinspection PhpUnhandledExceptionInspection
     */
    public function define($name): Definition
    {
        return parent::define($name);
    }

    public function faker(): Faker
    {
        return $this->faker;
    }

    /**
     * {@inheritDoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function loadFactories($paths): self
    {
        foreach ((array)$paths as $path) {
            $real = \realpath($path);

            if ($real === false) {
                throw new DirectoryNotFoundException($path);
            }

            if (\is_dir($real) === false) {
                throw new DirectoryNotFoundException($real);
            }

            $this->loadDirectory($real);
        }

        return $this;
    }

    /**
     * Load all the files in a directory or sub-directory.
     *
     * Files that start with a . or do not have a .php extension are ignored.
     * Each required file will have this instance available as "$fm".
     *
     * @param string $path The directory path to load.
     *
     * @return void
     *
     * @noinspection PhpMissingParentCallCommonInspection
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable) We use $factoryMuffin in our factories
     */
    private function loadDirectory($path): void
    {
        $directory = new RecursiveDirectoryIterator($path);
        $iterator = new RecursiveIteratorIterator($directory);
        $files = new RegexIterator($iterator, '/^.+\.php$/i');

        /** @noinspection PhpUnusedLocalVariableInspection */
        $factoryMuffin = $this;

        foreach ($files as $file) {
            // Ignore factories in hidden subdirectories
            if (\strncmp($file->getPathInfo()->getFilename(), '.', 1) === 0) {
                continue;
            }

            /** @noinspection PhpIncludeInspection */
            require $file->getPathName();
        }
    }
}
