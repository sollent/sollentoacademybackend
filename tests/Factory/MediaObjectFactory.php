<?php
declare(strict_types=1);

use App\Common\ApiResource\MediaObject;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(MediaObject::class)->setDefinitions([
    'contentUrl' => $factoryMuffin->faker()->url,
    'filePath' => $factoryMuffin->faker()->regexify('[A-Za-z0-9]{10}\.[a-z]{3}'),
]);
