<?php
declare(strict_types=1);

use App\Schedule\ApiResource\ScheduleBreak;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(ScheduleBreak::class)->setDefinitions([
    'timeFrom' => (new DateTimeImmutable())->setTime(12, 0),
    'timeTill' => (new DateTimeImmutable())->setTime(13, 0),
]);
