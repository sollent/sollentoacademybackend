<?php
declare(strict_types=1);

use App\Notification\ApiResource\Notification;
use App\Notification\ApiResource\NotificationEventInterface;
use App\Patient\ApiResource\Patient;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Notification::class)->setDefinitions([
    'event' => $factoryMuffin->faker()->randomElement(NotificationEventInterface::ALL_EVENTS),
    'failureReason' => $factoryMuffin->faker()->optional()->text(255),
    'locale' => 'en',
    'message' => $factoryMuffin->faker()->text(255),
    'receiver' => 'entity|' . Patient::class,
    'status' => Notification::STATUS_NEW,
]);
