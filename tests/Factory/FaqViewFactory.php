<?php
declare(strict_types=1);

use App\Faq\ApiResource\FaqView;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(FaqView::class)->setDefinitions([
    'region' => $factoryMuffin->faker()->text(20),
]);
