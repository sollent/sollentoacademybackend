<?php
declare(strict_types=1);

use App\Faq\ApiResource\FaqCategory;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(FaqCategory::class)->setDefinitions([
    'name' => $factoryMuffin->faker()->text(50),
]);
