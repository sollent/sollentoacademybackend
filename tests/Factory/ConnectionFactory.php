<?php
declare(strict_types=1);

use App\Connection\ApiResource\Connection;
use App\Doctor\ApiResource\Doctor;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Connection::class)->setDefinitions([
    'status' => Connection::STATUS_PENDING,
])->setCallback(function ($object, $saved) use ($factoryMuffin) {
    /** @var \App\Connection\ApiResource\Connection $object */
    if ($object->getSender() === null) {
        $object->setSender($factoryMuffin->create(Doctor::class, ['active' => true]));
    }
    if ($object->getRecipient() === null) {
        $object->setRecipient($factoryMuffin->create(Doctor::class, ['active' => true]));
    }
});
