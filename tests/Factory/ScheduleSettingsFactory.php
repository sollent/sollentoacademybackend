<?php
declare(strict_types=1);

use App\Doctor\ApiResource\Doctor;
use App\Schedule\ApiResource\ScheduleSettings;
use App\Schedule\ApiResource\WorkingTimeSettingsByDay;
use App\Schedule\ApiResource\WorkingTimeSettingsCommon;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(ScheduleSettings::class)->setDefinitions([
    'doctor' => 'entity|' . Doctor::class,
    'workOnHoliday' => $factoryMuffin->faker()->boolean(),
    'workingDays' => $factoryMuffin->faker()->randomElements([1, 2, 3, 4, 5, 6, 7], 3),
    'workingTimeType' => $factoryMuffin->faker()->randomElement(ScheduleSettings::ALL_WORKING_TIME_TYPES),
    'workingTimeSettingsByDays' => function (ScheduleSettings $scheduleSettings, bool $saved) use (
        $factoryMuffin
    ) {
        $settingsByDays = [];
        foreach ($scheduleSettings->getWorkingDays() as $workingDay) {
            $settingsByDays[] = $factoryMuffin->instance(WorkingTimeSettingsByDay::class, ['day' => $workingDay]);
        }

        return $settingsByDays;
    },
    'workingTimeSettingsCommon' => 'entity|' . WorkingTimeSettingsCommon::class,
]);
