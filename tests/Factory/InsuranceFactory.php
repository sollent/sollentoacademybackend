<?php
declare(strict_types=1);

use App\Insurance\ApiResource\Insurance;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Insurance::class)->setDefinitions([
    'title' => $factoryMuffin->faker()->text(20),
]);
