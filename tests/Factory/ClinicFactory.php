<?php
declare(strict_types=1);

use App\Clinic\ApiResource\Clinic;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Clinic::class)->setDefinitions([
    'title' => $factoryMuffin->faker()->text(30),
    'location' => $factoryMuffin->faker()->address,
]);
