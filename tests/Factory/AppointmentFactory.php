<?php
declare(strict_types=1);

use App\Appointment\ApiResource\Appointment;
use App\Doctor\ApiResource\Doctor;
use App\Patient\ApiResource\Patient;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Appointment::class)->setDefinitions([
    'status' => Appointment::STATUS_NEW,
    'date' => \DateTimeImmutable::createFromMutable(($factoryMuffin->faker()->dateTime)()),
    'time' => \DateTimeImmutable::createFromMutable(($factoryMuffin->faker()->dateTime)()),
])->setCallback(function ($object, $saved) use ($factoryMuffin) {
    if ($object->getDoctor() === null) {
        $object->setDoctor($factoryMuffin->create(Doctor::class));
    }
    if ($object->getPatient() === null) {
        $object->setPatient($factoryMuffin->create(Patient::class));
    }
});
