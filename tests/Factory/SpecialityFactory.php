<?php
declare(strict_types=1);

use App\Doctor\ApiResource\Speciality;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Speciality::class)->setDefinitions([
    'name' => $factoryMuffin->faker()->text(35),
]);
