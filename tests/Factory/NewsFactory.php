<?php
declare(strict_types=1);

use App\News\ApiResource\News;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(News::class)->setDefinitions([
    'title' => $factoryMuffin->faker()->text(255),
    'content' => $factoryMuffin->faker()->text,
]);
