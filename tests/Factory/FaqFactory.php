<?php
declare(strict_types=1);

use App\Faq\ApiResource\Faq;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Faq::class)->setDefinitions([
    'title' => $factoryMuffin->faker()->text(50),
    'answer' => $factoryMuffin->faker()->text(50),
    'number' => $factoryMuffin->faker()->numberBetween(1, 100),
]);
