<?php
declare(strict_types=1);

use App\Common\ApiResource\MediaObject;
use App\Common\Security\RolesInterface;
use App\Patient\ApiResource\Patient;
use App\UserCommon\ApiResource\Profile;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Patient::class)->setDefinitions([
    'email' => $factoryMuffin->faker()->email,
    'password' => $factoryMuffin->faker()->password(),
])->setCallback(function ($object, $saved) use ($factoryMuffin) {
    $object->addRole(RolesInterface::ROLE_PATIENT);
    if ($object->getProfile() === null) {
        $profile = new Profile();
        $profile->setFirstName(($factoryMuffin->faker()->firstName)());
        $profile->setLastName(($factoryMuffin->faker()->lastName)());
        $profile->setAvatar(
            $factoryMuffin->create(MediaObject::class)
        );
        $object->setProfile($profile);
    }
});
