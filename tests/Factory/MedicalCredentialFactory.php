<?php
declare(strict_types=1);

use App\Doctor\ApiResource\MedicalCredential;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(MedicalCredential::class)->setDefinitions([
    'title' => $factoryMuffin->faker()->text(30),
]);
