<?php
declare(strict_types=1);

use App\Common\ApiResource\MediaObject;
use App\Common\Security\RolesInterface;
use App\Doctor\ApiResource\Doctor;
use App\Schedule\ApiResource\Schedule;
use App\UserCommon\ApiResource\Profile;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Doctor::class)->setDefinitions([
    'email' => $factoryMuffin->faker()->email,
    'password' => $factoryMuffin->faker()->password(),
])->setCallback(function (Doctor $doctor, $saved) use ($factoryMuffin) {
    /** @var \App\Schedule\ApiResource\Schedule $schedule */
    $schedule = $factoryMuffin->create(Schedule::class, ['doctor' => $doctor]);
    $doctor
        ->addRole(RolesInterface::ROLE_DOCTOR)
        ->setSchedule($schedule);
    if ($doctor->getProfile() === null) {
        $profile = new Profile();
        $profile->setFirstName(($factoryMuffin->faker()->firstName)());
        $profile->setLastName(($factoryMuffin->faker()->lastName)());
        $profile->setAvatar(
            $factoryMuffin->create(MediaObject::class)
        );
        $doctor->setProfile($profile);
    }
});
