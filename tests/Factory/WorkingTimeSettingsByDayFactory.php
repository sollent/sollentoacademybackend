<?php
declare(strict_types=1);

use App\Schedule\ApiResource\ScheduleBreak;
use App\Schedule\ApiResource\WorkingTimeSettingsByDay;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(WorkingTimeSettingsByDay::class)->setDefinitions([
    'breaks' => function (WorkingTimeSettingsByDay $workingTimeSettingsCommon) use ($factoryMuffin) {
        $break = $factoryMuffin->instance(ScheduleBreak::class, [
            'from' => (new DateTimeImmutable())->setTime(13, 0),
            'till' => (new DateTimeImmutable())->setTime(14, 0),
        ]);

        return [$break];
    },
    'day' => $factoryMuffin->faker()->randomElement([1, 2, 3, 4, 5, 6, 7]),
    'workingTimeEnd' => (new DateTimeImmutable())->setTime(20, 0),
    'workingTimeStart' => (new DateTimeImmutable())->setTime(10, 0),
]);
