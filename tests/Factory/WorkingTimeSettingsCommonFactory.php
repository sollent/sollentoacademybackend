<?php
declare(strict_types=1);

use App\Schedule\ApiResource\ScheduleBreak;
use App\Schedule\ApiResource\WorkingTimeSettingsCommon;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(WorkingTimeSettingsCommon::class)->setDefinitions([
    'breaks' => function (WorkingTimeSettingsCommon $workingTimeSettingsCommon) use ($factoryMuffin) {
        $break = $factoryMuffin->instance(ScheduleBreak::class, [
            'from' => (new DateTimeImmutable())->setTime(13, 0),
            'till' => (new DateTimeImmutable())->setTime(14, 0),
        ]);

        return [$break];
    },
    'workingTimeEnd' => (new DateTimeImmutable())->setTime(20, 0),
    'workingTimeStart' => (new DateTimeImmutable())->setTime(10, 0),
]);
