<?php
declare(strict_types=1);

use App\Admin\ApiResource\Admin;
use App\Common\Security\RolesInterface;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Admin::class)->setDefinitions([
    'email' => $factoryMuffin->faker()->email,
    'password' => $factoryMuffin->faker()->password(),
])->setCallback(function ($object) {
    $object->addRole(RolesInterface::ROLE_SUPER_ADMIN);
});
