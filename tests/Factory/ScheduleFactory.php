<?php
declare(strict_types=1);

use App\Schedule\ApiResource\Schedule;

/** @var \Test\FactoryMuffinWrapper $factoryMuffin */
$factoryMuffin->define(Schedule::class)->setDefinitions([
    'appointmentByCalltimeFrom' => false,
]);
