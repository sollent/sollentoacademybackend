<?php
declare(strict_types=1);

namespace Test;

use App\Admin\ApiResource\Admin;
use App\Common\ApiResource\User;
use App\Doctor\ApiResource\Doctor;
use App\Patient\ApiResource\Patient;
use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

trait AuthenticateTrait
{
    /**
     * Makes user authentication and returns response with tokens
     *
     * @param \App\Common\ApiResource\User $user
     * @param string $plainPassword
     *
     * @return array|null
     */
    public function authenticate(User $user, string $plainPassword): ?array
    {
        $response = $this->json('POST', '/auth', [
            'email' => $user->getEmail(),
            'password' => $plainPassword,
        ]);

        self::assertResponseStatusCodeSame(200);
        self::assertArrayHasKey('token', $response);

        return $response;
    }

    /**
     * Adds password for user.
     *
     * @param \App\Common\ApiResource\User $user
     * @param string $password
     *
     * @return \App\Common\ApiResource\User
     */
    public function extendWithPassword(User $user, string $password): User
    {
        $passwordHasher = self::getContainer()->get(UserPasswordHasherInterface::class);
        $user->setPassword($passwordHasher->hashPassword($user, $password));

        $entityManager = self::getContainer()->get(EntityManagerInterface::class);
        $entityManager->flush();

        return $user;
    }

    public function login(User $user): void
    {
        /** @var \Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface $jwtTokenManager */
        $jwtTokenManager = self::getContainer()->get(JWTTokenManagerInterface::class);

        $this->securityContext->user = $user;
        $this->securityContext->jwtToken = $jwtTokenManager->create($user);
    }

    public function loginAsDoctor(?Doctor $doctor = null, ?array $parameters = null): Doctor
    {
        if ($doctor === null) {
            $doctor = $this->createEntity(Doctor::class, $parameters ?? []);
        }

        $this->login($doctor);

        return $doctor;
    }

    public function loginAsPatient(?Patient $patient = null, ?array $parameters = null): Patient
    {
        if ($patient === null) {
            $patient = $this->createEntity(Patient::class, $parameters ?? []);
        }

        $this->login($patient);

        return $patient;
    }

    public function loginAsSuperAdmin(): Admin
    {
        $admin = $this->createEntity(Admin::class);

        $this->login($admin);

        return $admin;
    }
}
