<?php
declare(strict_types=1);

namespace Test;

use App\Common\ApiResource\User;

final class ApiResourceSecurityContext
{
    public ?string $jwtToken = null;

    public ?User $user = null;
}
