<?php
declare(strict_types=1);

namespace Test;

use App\Common\ApiResource\AbstractEntity;
use Doctrine\ORM\EntityManagerInterface;

trait DatabaseEntityHelperTrait
{
    /**
     * @param mixed[]|null $criteria
     */
    protected function countEntities(string $entityClass, int $expectedCount, ?array $criteria = null): void
    {
        /** @var \Doctrine\ORM\EntityRepository $repository */
        $repository = $this->getEntityManager()->getRepository($entityClass);

        self::assertSame($expectedCount, $repository->count($criteria ?? []));
    }

    /**
     * @param mixed[] $criteria
     * @param mixed[]|null $jsonAttributes
     *
     * @phpstan-param class-string<TEntity> $entityClass
     *
     * @phpstan-return TEntity
     *
     * @phpstan-template TEntity of \App\ApiResource\AbstractEntity
     */
    protected function findOneEntity(
        string $entityClass,
        array $criteria,
        ?array $jsonAttributes = null
    ): ?AbstractEntity {
        $jsonAttributes = $jsonAttributes ?? [];

        $alias = 'entity';
        $queryBuilder = $this->getEntityManager()->createQueryBuilder();

        $queryBuilder->select($alias)->from($entityClass, $alias);

        foreach ($criteria as $criteriaProperty => $criteriaValue) {
            if (\in_array($criteriaProperty, $jsonAttributes, true)) {
                $queryBuilder->andWhere(
                    $queryBuilder->expr()->eq(
                        \sprintf('JSON_CONTAINS(%s.%s, :%s)', $alias, $criteriaProperty, $criteriaProperty),
                        'TRUE'
                    )
                );
                $queryBuilder->setParameter($criteriaProperty, \json_encode($criteriaValue));

                continue;
            }
            if (\is_null($criteriaValue)) {
                $queryBuilder->andWhere(\sprintf('%s.%s IS NULL', $alias, $criteriaProperty));

                continue;
            }
            $queryBuilder->andWhere(
                $queryBuilder->expr()->eq(
                    \sprintf('%s.%s', $alias, $criteriaProperty),
                    \sprintf(':%s', $criteriaProperty)
                )
            );
            $queryBuilder->setParameter($criteriaProperty, $criteriaValue);
        }

        /**
         * @phpstan-template TEntity of \App\ApiResource\AbstractEntity
         *
         * @phpstan-var TEntity $entity
         *
         * @var AbstractEntity|null $entity
         */
        $entity = $queryBuilder
            ->getQuery()
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $entity;
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        /** @var \Doctrine\Persistence\ManagerRegistry $managerRegistry */
        $managerRegistry = static::$container->get('doctrine');
        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        $entityManager = $managerRegistry->getManager();

        return $entityManager;
    }

    /**
     * @param mixed[]|null $criteria
     *
     * @phpstan-param class-string<AbstractEntity> $entityClass
     */
    protected function notSeeEntity(string $entityClass, ?array $criteria = null, ?string $message = null): void
    {
        $message = $message ?? \sprintf("Failed asserting that the %s doesn't exist.", $entityClass);

        self::assertNull($this->findOneEntity($entityClass, $criteria ?? []), $message);
    }

    /**
     * @param mixed[] $criteria
     * @param mixed[]|null $jsonAttributes
     *
     * @phpstan-param class-string<AbstractEntity> $entityClass
     */
    protected function seeEntity(
        string $entityClass,
        array $criteria,
        ?array $jsonAttributes = null,
        ?string $message = null
    ): void {
        $message = $message ?? \sprintf('Failed asserting that the %s exists.', $entityClass);

        self::assertNotNull($this->findOneEntity($entityClass, $criteria, $jsonAttributes), $message);
    }
}
