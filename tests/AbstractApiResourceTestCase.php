<?php
declare(strict_types=1);

namespace Test;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Faker\Factory;
use Faker\Generator;
use LogicException;
use Symfony\Contracts\HttpClient\ResponseInterface;

abstract class AbstractApiResourceTestCase extends ApiTestCase
{
    use AuthenticateTrait;
    use DatabaseEntityHelperTrait;
    use EntityFactoryTrait;

    protected const LOCALE_HEADER = 'X-LOCALE';

    /**
     * @var mixed[]
     */
    protected const RESPONSE_STRUCTURE = [];

    protected Generator $faker;

    protected ResponseInterface $response;

    protected ApiResourceSecurityContext $securityContext;

    private Client $client;

    public function setUp(): void
    {
        parent::setUp();

        $this->client = static::createClient();
        $this->faker = Factory::create();
        $this->initFactories($this->faker);
        $this->securityContext = new ApiResourceSecurityContext();
    }

    public function tearDown(): void
    {
        $this->faker->unique(true);
        Carbon::setTestNow();

        parent::tearDown();
    }

    /**
     * @param mixed[] $structure
     * @param mixed[] $array
     */
    protected function assertArrayStructure(array $structure, array $array): void
    {
        foreach ($structure as $key => $value) {
            if (\is_array($value)) {
                if ($key === '*') {
                    foreach ($array as $itemKey => $responseDataItem) {
                        $this->assertArrayStructure($structure[$key], $responseDataItem);
                        unset($array[$itemKey]);
                    }
                    unset($structure[$key]);
                }
                if ($key !== '*') {
                    static::assertArrayHasKey($key, $array);
                    $this->assertArrayStructure($value, $array[$key]);
                    unset($structure[$key], $array[$key]);
                }
            }
            if (\is_array($value) === false) {
                static::assertArrayHasKey($value, $array);
            }
        }

        self::assertSame(
            \array_diff(\array_keys($array), $structure),
            \array_diff($structure, \array_keys($array))
        );
    }

    protected function assertEntityValidationError(string $violationProperty, string $violationMessage): void
    {
        $this->assertErrorResponse(400, 'Validation failed.');
        $this->assertViolations([$violationMessage], $violationProperty, false);
    }

    protected function assertErrorResponse(int $responseStatusCode, ?string $userMessage = null): void
    {
        self::assertResponseStatusCodeSame($responseStatusCode);

        $responseData = $this->response->toArray(false);
        self::assertArrayHasKey('code', $responseData);
        self::assertArrayHasKey('message', $responseData);
        self::assertSame($userMessage ?? 'Oops, something went wrong.', $responseData['message']);
    }

    /**
     * @param array<int, string|int> $ids
     */
    protected function assertListContainsIds(array $ids, ?bool $assertInnerItems = null): void
    {
        $array = $this->response->toArray(false) ?? [];
        $items = $assertInnerItems === null ? $array['items'] : $array;

        $listIds = \array_map(static fn ($item): ?string => $item['id'] ?? null, $items);
        $notPresentedIds = \array_diff($ids, $listIds);

        self::assertCount(
            0,
            $notPresentedIds,
            \sprintf('The following IDs are not present in the list: %s', \implode(', ', $notPresentedIds))
        );
    }

    /**«
     *
     * @param int $expectedCount
     * @param mixed[]|null $itemJsonStructure
     * @param bool $assertInnerItems
     */
    protected function assertListItems(
        int $expectedCount,
        ?array $itemJsonStructure = null,
        ?bool $assertInnerItems = null
    ): void {
        $array = $this->response->toArray(false);
        $items = $assertInnerItems === null ? $array['items'] : $array;

        if ($expectedCount !== 0 && $itemJsonStructure === null) {
            $message = 'Please, provide item JSON structure as second argument.';

            throw new LogicException($message);
        }

        self::assertCount($expectedCount, $items);

        if ($expectedCount === 0) {
            return;
        }

        foreach ($items as $item) {
            self::assertCount(\count($itemJsonStructure), $item);
            $this->assertArrayStructure($itemJsonStructure, $item);
        }
    }

    /**
     * @param string[] $violations
     */
    protected function assertViolations(array $violations, ?string $property = null, ?bool $strict = null): void
    {
        $responseData = $this->response->toArray(false);

        self::assertArrayHasKey('violations', $responseData);
        self::assertIsArray($responseData['violations']);

        if ($property !== null) {
            self::assertArrayHasKey(
                $property,
                $responseData['violations'],
                \sprintf(
                    'Violations are not found for property %s. Available violations: %s',
                    $property,
                    \print_r($responseData['violations'], true)
                )
            );
        }

        $responseViolations = $property !== null ? $responseData['violations'][$property] : $responseData['violations'];

        if (($strict ?? true) === true) {
            self::assertCount(\count($violations), $responseViolations);
        }

        foreach ($violations as $violation) {
            self::assertContains($violation, $responseViolations, \print_r($responseViolations, true));
        }
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        /** @var \Doctrine\ORM\EntityManagerInterface $entityManager */
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        return $entityManager;
    }

    /**
     * @param mixed[]|null $flatten
     * @param mixed[]|null $excludes
     *
     * @return mixed[]
     */
    protected function getResponseStructure(?array $flatten = null, ?array $excludes = null): array
    {
        $structure = static::RESPONSE_STRUCTURE;

        foreach ($flatten ?? [] as $attribute) {
            if (isset($structure[$attribute])) {
                unset($structure[$attribute]);
                $structure[] = $attribute;
            }
        }

        foreach ($excludes ?? [] as $attribute) {
            if (isset($structure[$attribute])) {
                unset($structure[$attribute]);

                continue;
            }

            $attributeIndex = \array_search($attribute, $structure, true);

            if ($attributeIndex !== false) {
                unset($structure[$attributeIndex]);
            }
        }

        return $structure;
    }

    /**
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }

    /**
     * Makes JSON request and returns response decoded into an array.
     *
     * @param mixed[]|null $data
     * @param mixed[]|null $headers
     *
     * @return mixed[]|null
     */
    protected function json(string $method, string $uri, ?array $data = null, ?array $headers = null): ?array
    {
        $headers = $headers ?? [];
        $headers['Content-Type'] = 'application/json';
        $headers['Accept'] = 'application/json';

        $response = $this->request($method, $uri, [
            'headers' => $headers,
            'json' => $data ?? [],
        ]);

        return $response->getContent(false) !== '' ? $response->toArray(false) : null;
    }

    /**
     * Makes authenticated JSON request and returns response decoded into an array.
     *
     * @param mixed[]|null $data
     * @param mixed[]|null $headers
     *
     * @return mixed[]|null
     */
    protected function jsonAuthenticated(
        string $method,
        string $uri,
        ?array $data = null,
        ?array $headers = null
    ): ?array {
        if ($this->securityContext->jwtToken === null) {
            $this->loginAsSuperAdmin();
        }

        $headers = $headers ?? [];
        $headers['Authorization'] = "Bearer {$this->securityContext->jwtToken}";

        return $this->json($method, $uri, $data, $headers);
    }

    /**
     * @param mixed[] $options
     */
    protected function request(string $method, string $uri, ?array $options = null): ResponseInterface
    {
        return $this->response = $this->client->request($method, $uri, $options ?? []);
    }
}
