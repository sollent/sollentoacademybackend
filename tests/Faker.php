<?php
declare(strict_types=1);

namespace Test;

use Closure;
use League\FactoryMuffin\Faker\Faker as BaseFaker;
use LogicException;

/**
 * @mixin \Faker\Generator
 */
final class Faker extends BaseFaker
{
    /**
     * Dynamically wrap faker method calls in closures.
     */
    public function __get(string $method): Closure
    {
        return $this->format($method);
    }

    public function __isset(string $name): bool
    {
        $message = 'You can not isset properties of faker.';

        throw new LogicException($message);
    }

    /**
     * @param mixed $value
     */
    public function __set(string $name, $value): void
    {
        $message = 'You can not set properties of faker.';

        throw new LogicException($message);
    }
}
