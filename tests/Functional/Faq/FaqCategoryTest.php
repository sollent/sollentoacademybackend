<?php
declare(strict_types=1);

namespace Test\Functional\Faq;

use App\Faq\ApiResource\Faq;
use App\Faq\ApiResource\FaqCategory;
use Test\AbstractApiResourceTestCase;

final class FaqCategoryTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'name',
    ];

    public function testList(): void
    {
        $faqCategories = $this->createEntityList(5, FaqCategory::class);
        $this->json('GET', '/api/faq_categories?page=1&perPage=5');

        self::assertResponseIsSuccessful();
        $this->assertListItems(0);

        foreach ($faqCategories as $category) {
            $this->createEntity(Faq::class, [
                'category' => $category,
            ]);
        }

        $this->json('GET', '/api/faq_categories?page=1&perPage=5');

        self::assertResponseIsSuccessful();
        $this->assertListItems(5, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $faqCategory = $this->createEntity(FaqCategory::class);
        $faqCategoryIri = $this->findIriBy(FaqCategory::class, ['id' => $faqCategory->getId()]);

        $this->json('GET', $faqCategoryIri);

        self::assertResponseStatusCodeSame(404);

        $this->createEntity(Faq::class, ['category' => $faqCategory]);

        $response = $this->json('GET', $faqCategoryIri);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }
}
