<?php
declare(strict_types=1);

namespace Test\Functional\Faq;

use App\Doctor\ApiResource\Doctor;
use App\Faq\ApiResource\Faq;
use Test\AbstractApiResourceTestCase;

final class FaqViewTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'faq',
        'user',
        'region',
    ];

    public function testNew(): void
    {
        $faq = $this->createEntity(Faq::class);
        $faqIri = $this->findIriBy(Faq::class, ['id' => $faq->getId()]);

        // As anonymous user with region string
        $json = [
            'faq' => $faqIri,
            'region' => 'ExampleRegionString',
        ];
        $response = $this->json('POST', '/api/faq_views', $json);

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);

        // As authenticated user
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            $plainPassword = 'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $response = $this->jsonAuthenticated('POST', '/api/faq_views', $json);

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        $this->assertSame($doctor->getId(), $response['user']['id']);
    }
}
