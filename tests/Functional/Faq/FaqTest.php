<?php
declare(strict_types=1);

namespace Test\Functional\Faq;

use App\Faq\ApiResource\Faq;
use App\Faq\ApiResource\FaqCategory;
use Test\AbstractApiResourceTestCase;

final class FaqTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'category',
        'title',
        'answer',
        'number',
        'views',
    ];

    public function testList(): void
    {
        $this->createEntityList(3, Faq::class);
        $this->json('GET', '/api/faqs');

        $this->assertResponseIsSuccessful();
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $faq = $this->createEntity(Faq::class);
        $iri = $this->findIriBy(Faq::class, ['id' => $faq->getId()]);

        $response = $this->json('GET', $iri);

        $this->assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testFilterByCategory(): void
    {
        $category = $this->createEntity(FaqCategory::class);
        $categoryIri = $this->findIriBy(FaqCategory::class, ['id' => $category->getId()]);
        $this->createEntityList(5, Faq::class, [
            'category' => $category,
        ]);
        $this->createEntityList(5, Faq::class);

        $this->json('GET', "/api/faqs?page=1&perPage=10&category=$categoryIri");

        self::assertResponseIsSuccessful();
        $this->assertListItems(5, self::RESPONSE_STRUCTURE);
    }

    /**
     * @param int[] $viewsData
     *
     * @dataProvider orderByViewsCountDataProvider
     */
    public function testOrderByViewsCount(array $viewsData): void
    {
        $this->markTestSkipped();
        \array_walk($viewsData, function (int $count) {
            $faq = $this->createEntity(Faq::class);
            $this->createEntityList($count, FaqView::class, [
                'faq' => $faq,
            ]);
        });

        $response = $this->json('GET', '/api/faqs?page=1&perPage=30&viewsCount=DESC');

        self::assertResponseIsSuccessful();
        foreach ($response as $index => $responseItem) {
            self::assertSame($viewsData[$index], $responseItem['views']);
        }
    }

    /**
     * @return array[]
     */
    public function orderByViewsCountDataProvider(): array
    {
        return [
            [
                [16, 13, 10, 6, 4, 3, 1, 0, 0],
            ],
        ];
    }
}
