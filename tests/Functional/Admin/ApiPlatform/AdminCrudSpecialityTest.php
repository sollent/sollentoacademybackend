<?php
declare(strict_types=1);

namespace Test\Functional\Admin\ApiPlatform;

use App\Doctor\ApiResource\Speciality;
use Test\AbstractApiResourceTestCase;

final class AdminCrudSpecialityTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'name',
        'id',
        'doctorsCount',
    ];

    public function testNew(): void
    {
        $response = $this->jsonAuthenticated(
            'POST',
            '/api/specialities',
            ['name' => 'Speciality example name']
        );

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testList(): void
    {
        $this->createEntityList(3, Speciality::class);

        $this->jsonAuthenticated('GET', '/api/specialities');

        $this->assertResponseIsSuccessful();
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $speciality = $this->createEntity(Speciality::class);
        $iri = $this->findIriBy(Speciality::class, ['id' => $speciality->getId()]);

        $response = $this->jsonAuthenticated('GET', $iri);

        $this->assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testEdit(): void
    {
        $speciality = $this->createEntity(Speciality::class);
        $iri = $this->findIriBy(Speciality::class, ['id' => $speciality->getId()]);

        $response = $this->jsonAuthenticated('PUT', $iri, ['name' => 'New speciality name']);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testRemove(): void
    {
        $speciality = $this->createEntity(Speciality::class);
        $iri = $this->findIriBy(Speciality::class, ['id' => $speciality->getId()]);

        $this->jsonAuthenticated('DELETE', $iri);

        self::assertResponseStatusCodeSame(204);
    }
}
