<?php
declare(strict_types=1);

namespace Test\Functional\Admin\ApiPlatform;

use App\Faq\ApiResource\Faq;
use App\Faq\ApiResource\FaqView;
use Test\AbstractApiResourceTestCase;

final class AdminCrudFaqViewTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'faq',
        'user',
        'region',
    ];

    public function testList(): void
    {
        $faqs = $this->createEntityList(5, Faq::class);
        foreach ($faqs as $faq) {
            $this->createEntity(FaqView::class, ['faq' => $faq]);
        }

        $this->jsonAuthenticated('GET', '/api/faq_views?page=1&perPage=25');

        self::assertResponseIsSuccessful();
        $this->assertListItems(5, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $faqView = $this->createEntity(FaqView::class);
        $faqViewIri = $this->findIriBy(FaqView::class, ['id' => $faqView->getId()]);

        $response = $this->jsonAuthenticated('GET', $faqViewIri);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }
}
