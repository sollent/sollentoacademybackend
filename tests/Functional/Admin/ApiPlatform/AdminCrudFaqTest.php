<?php
declare(strict_types=1);

namespace Test\Functional\Admin\ApiPlatform;

use App\Faq\ApiResource\Faq;
use App\Faq\ApiResource\FaqCategory;
use Test\AbstractApiResourceTestCase;

final class AdminCrudFaqTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'category',
        'title',
        'answer',
        'number',
        'views',
    ];

    public function testNew(): void
    {
        $faqCategory = $this->createEntity(FaqCategory::class);
        $faqCategoryIri = $this->findIriBy(FaqCategory::class, ['id' => $faqCategory->getId()]);

        $json = [
            'category' => $faqCategoryIri,
            'answer' => 'Answer example',
            'number' => 1,
            'title' => 'Faq title example',
        ];
        $response = $this->jsonAuthenticated('POST', '/api/faqs', $json);

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testList(): void
    {
        $this->createEntityList(3, Faq::class);

        $this->jsonAuthenticated('GET', '/api/faqs');

        $this->assertResponseIsSuccessful();
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $faq = $this->createEntity(Faq::class);
        $iri = $this->findIriBy(Faq::class, ['id' => $faq->getId()]);

        $response = $this->jsonAuthenticated('GET', $iri);

        $this->assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testRemove(): void
    {
        $faq = $this->createEntity(Faq::class);
        $iri = $this->findIriBy(Faq::class, ['id' => $faq->getId()]);

        $this->jsonAuthenticated('DELETE', $iri);

        self::assertResponseStatusCodeSame(204);
    }
}
