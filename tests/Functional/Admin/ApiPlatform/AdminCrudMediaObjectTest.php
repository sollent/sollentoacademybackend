<?php
declare(strict_types=1);

namespace Test\Functional\Admin\ApiPlatform;

use App\Common\ApiResource\MediaObject;
use Test\AbstractApiResourceTestCase;

final class AdminCrudMediaObjectTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'contentUrl',
    ];

    public function testList(): void
    {
        $this->createEntityList(3, MediaObject::class);
        $this->jsonAuthenticated('GET', '/api/media_objects');

        $this->assertResponseIsSuccessful();
    }
}
