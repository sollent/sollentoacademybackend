<?php
declare(strict_types=1);

namespace Test\Functional\Admin\ApiPlatform;

use App\Appointment\ApiResource\Appointment;
use App\Doctor\ApiResource\Doctor;
use App\Patient\ApiResource\Patient;
use Test\AbstractApiResourceTestCase;

final class AdminCrudAppointmentTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'status',
        'doctor',
        'patient',
        'date',
        'time',
        'createdAt',
        'cancellationReason',
        'clinicLocation',
        'reason',
    ];

    public function testList(): void
    {
        $this->createEntityList(15, Appointment::class, [
            'doctor' => $this->createEntity(Doctor::class),
        ]);

        $this->jsonAuthenticated('GET', '/api/appointments?page=1&perPage=30');

        self::assertResponseIsSuccessful();
        $this->assertListItems(15, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $appointment = $this->createEntity(Appointment::class, [
            'doctor' => $doctor = $this->createEntity(Doctor::class),
        ]);
        $appointmentIri = $this->findIriBy(Appointment::class, ['id' => $appointment->getId()]);

        $response = $this->jsonAuthenticated('GET', $appointmentIri);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($appointment->getStatus(), $response['status']);
        self::assertSame($appointment->getDoctor()->getId(), $response['doctor']['id']);
        self::assertSame($appointment->getPatient()->getId(), $response['patient']['id']);
        self::assertSame(
            $appointment->getDate()->format('Y-m-d'),
            (new \DateTimeImmutable($response['date']))->format('Y-m-d')
        );
        self::assertSame(
            $appointment->getTime()->format('H:i'),
            (new \DateTimeImmutable($response['time']))->format('H:i')
        );
        self::assertSame($appointment->getReason(), $response['reason']);
    }

    public function testEdit(): void
    {
        $appointment = $this->createEntity(Appointment::class, [
            'doctor' => $this->createEntity(Doctor::class),
        ]);
        $appointmentIri = $this->findIriBy(Appointment::class, ['id' => $appointment->getId()]);

        $json = [
            'status' => Appointment::STATUS_CANCELED,
            'doctor' => $this->findIriBy(
                Doctor::class,
                ['id' => ($doctor = $this->createEntity(Doctor::class))->getId()]
            ),
            'patient' => $this->findIriBy(
                Patient::class,
                ['id' => ($patient = $this->createEntity(Patient::class))->getId()]
            ),
            'date' => '2022-12-12',
            'time' => '16:30',
        ];
        $response = $this->jsonAuthenticated('PUT', $appointmentIri, $json);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($json['status'], $response['status']);
        self::assertSame($doctor->getId(), $response['doctor']['id']);
        self::assertSame($patient->getId(), $response['patient']['id']);
        self::assertSame(
            (new \DateTimeImmutable($json['date']))->format('Y-m-d'),
            (new \DateTimeImmutable($response['date']))->format('Y-m-d')
        );
        self::assertSame(
            (new \DateTimeImmutable($json['time']))->format('H:i'),
            (new \DateTimeImmutable($response['time']))->format('H:i')
        );
    }

    public function testDelete(): void
    {
        $appointment = $this->createEntity(Appointment::class, [
            'doctor' => $this->createEntity(Doctor::class),
        ]);
        $appointmentIri = $this->findIriBy(Appointment::class, ['id' => $appointment->getId()]);

        $this->jsonAuthenticated('DELETE', $appointmentIri);

        self::assertResponseStatusCodeSame(204);
    }
}
