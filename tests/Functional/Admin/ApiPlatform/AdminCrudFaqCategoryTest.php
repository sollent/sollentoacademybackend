<?php
declare(strict_types=1);

namespace Test\Functional\Admin\ApiPlatform;

use App\Faq\ApiResource\FaqCategory;
use Test\AbstractApiResourceTestCase;

final class AdminCrudFaqCategoryTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'name',
    ];

    public function testNew(): void
    {
        $json = ['name' => 'ExampleFaqCategory'];
        $response = $this->jsonAuthenticated('POST', '/api/faq_categories', $json);

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        $this->assertSame($json['name'], $response['name']);
    }

    public function testEdit(): void
    {
        $faqCategory = $this->createEntity(FaqCategory::class);
        $faqCategoryIri = $this->findIriBy(FaqCategory::class, ['id' => $faqCategory->getId()]);

        $json = ['name' => 'NewFaqCategoryName'];
        $response = $this->jsonAuthenticated('PUT', $faqCategoryIri, $json);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        $this->assertSame($json['name'], $response['name']);
    }

    public function testRemove(): void
    {
        $faqCategory = $this->createEntity(FaqCategory::class);
        $faqCategoryIri = $this->findIriBy(FaqCategory::class, ['id' => $faqCategory->getId()]);

        $this->jsonAuthenticated('DELETE', $faqCategoryIri);

        self::assertResponseStatusCodeSame(204);
    }
}
