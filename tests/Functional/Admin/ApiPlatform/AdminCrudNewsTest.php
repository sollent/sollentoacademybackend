<?php
declare(strict_types=1);

namespace Test\Functional\Admin\ApiPlatform;

use App\Common\ApiResource\MediaObject;
use App\News\ApiResource\News;
use Test\AbstractApiResourceTestCase;

final class AdminCrudNewsTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'content',
        'title',
        'picture',
        'active',
        'createdAt',
    ];

    public function testNew(): void
    {
        $picture = $this->createEntity(MediaObject::class);

        $json = [
            'content' => 'Answer example',
            'active' => true,
            'title' => 'Faq title example',
            'picture' => $this->findIriBy(MediaObject::class, ['id' => $picture->getId()]),
        ];
        $response = $this->jsonAuthenticated('POST', '/api/news', $json);

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testList(): void
    {
        $this->createEntityList(3, News::class);

        $this->jsonAuthenticated('GET', '/api/news');

        self::assertResponseStatusCodeSame(200);
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $picture = $this->createEntity(MediaObject::class);
        $newsItem = $this->createEntity(News::class, ['picture' => $picture]);

        $response = $this->jsonAuthenticated('GET', "/api/news/{$newsItem->getId()}");

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($newsItem->getTitle(), $response['title']);
        self::assertSame($newsItem->getContent(), $response['content']);
        self::assertSame($newsItem->isActive(), $response['active']);
        self::assertSame($picture->getId(), $response['picture']['id']);
    }

    public function testEdit(): void
    {
        $picture = $this->createEntity(MediaObject::class);
        $newsItem = $this->createEntity(News::class, ['picture' => $picture]);

        $json = [
            'active' => true,
            'content' => 'News example content',
            'picture' => $this->findIriBy(MediaObject::class, ['id' => $picture->getId()]),
            'title' => 'News title',
        ];
        $response = $this->jsonAuthenticated('PUT', "/api/news/{$newsItem->getId()}", $json);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($json['active'], $response['active']);
        self::assertSame($json['content'], $response['content']);
        self::assertSame($picture->getId(), $response['picture']['id']);
        self::assertSame($json['title'], $response['title']);
    }

    public function testRemove(): void
    {
        $picture = $this->createEntity(MediaObject::class);
        $newsItem = $this->createEntity(News::class, ['picture' => $picture]);

        $this->jsonAuthenticated('DELETE', "/api/news/{$newsItem->getId()}");

        self::assertResponseStatusCodeSame(204);
    }
}
