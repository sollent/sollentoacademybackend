<?php
declare(strict_types=1);

namespace Test\Functional\Admin\Sonata\Controller;

use App\Admin\ApiResource\Admin;
use App\Common\Security\RolesInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserAdminControllerTest extends WebTestCase
{
    /**
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     */
    public function testSuccessLogin(): void
    {
        $client = static::createClient();
        $client->request('GET', '/admin/login');

        $admin = $this->createAdminUser();
        $client->loginUser($admin, 'admin');

        $client->request('GET', '/admin/dashboard');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('a.logo span', 'Deinarzt');
    }

    /**
     * @return \App\Admin\ApiResource\Admin
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function createAdminUser(): Admin
    {
        $entityManager = static::getContainer()->get('doctrine.orm.default_entity_manager');
        $passwordHasher = static::getContainer()->get('security.password_hasher');

        $admin = new Admin();
        $admin->setEmail('admin@mail.com');
        $admin->setPassword($passwordHasher->hashPassword($admin, 'AdminAdminAdmin'));
        $admin->addRole(RolesInterface::ROLE_SUPER_ADMIN);

        $entityManager->persist($admin);
        $entityManager->flush();

        return $admin;
    }
}
