<?php
declare(strict_types=1);

namespace Test\Functional\Doctor;

use App\Doctor\ApiResource\Doctor;
use App\Doctor\ApiResource\Speciality;
use Test\AbstractApiResourceTestCase;

final class SpecialityTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'name',
        'id',
        'doctorsCount',
    ];

    private const LETTERS_RESPONSE_STRUCTURE = [
        'letter',
    ];

    /**
     * @var \Traversable
     */
    protected $expectedStatisticsResponse = [];

    public function testList(): void
    {
        $this->createEntityList(3, Speciality::class);
        $this->json('GET', '/api/specialities');

        self::assertResponseIsSuccessful();
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
    }

    public function testShow(): void
    {
        $speciality = $this->createEntity(Speciality::class);
        $iri = $this->findIriBy(Speciality::class, ['id' => $speciality->getId()]);

        $response = $this->json('GET', $iri);
        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testAvailableLetters(): void
    {
        $letters = ['A', 'B', 'C'];

        // Create specialities list for any letter
        $letterSpecialities = [];
        foreach ($letters as $letter) {
            foreach (\range(1, 5) as $i) {
                $letterSpecialities[$letter][] = $this
                    ->createEntity(Speciality::class, ['name' => $letter . \uniqid()]);
            }
        }

        $response = $this->json('GET', '/api/specialities/available-letters');
        self::assertResponseIsSuccessful();
        self::assertEquals([], $response);

        // Add specialities in doctors
        foreach ($letters as $letter) {
            $doctor = $this->createEntity(Doctor::class);
            foreach ($letterSpecialities[$letter] as $speciality) {
                $doctor->addSpeciality($speciality);
            }
        }

        $this->json('GET', '/api/specialities/available-letters');
        self::assertResponseIsSuccessful();
        $this->assertListItems(2, self::LETTERS_RESPONSE_STRUCTURE, false);
    }

    public function testFilteringStatistics(): void
    {
        foreach ($this->getStatisticsLetters()['C'] as $item) {
            $speciality = $this->createEntity(Speciality::class, [
                'name' => $item['specialityName'],
            ]);
            if ($item['doctorsCount'] === 0) {
                continue;
            }
            $doctors = $this->createEntityList($item['doctorsCount'], Doctor::class);
            /** @var \App\Doctor\ApiResource\Doctor $doctor */
            foreach ($doctors as $doctor) {
                $doctor->addSpeciality($speciality);
            }
            \array_push($this->expectedStatisticsResponse, [
                'name' => $speciality->getName(),
                'id' => $speciality->getId(),
                'doctorsCount' => \count($doctors),
            ]);
        }

        $response = $this->json('GET', '/api/specialities?page=1&perPage=25&name=C&doctorsCount=DESC');

        self::assertEquals(\json_encode($this->expectedStatisticsResponse), \json_encode($response['items']));
    }

    /**
     * So far the test is for only one letter - 'С'
     *
     * @return \array[][]
     */
    private function getStatisticsLetters(): array
    {
        return [
            'C' => [
                [
                    'specialityName' => 'Cardiothoracic Surgery',
                    'doctorsCount' => 6,
                ],
                [
                    'specialityName' => 'Cardiology',
                    'doctorsCount' => 4,
                ],
                [
                    'specialityName' => 'Chemical Pathology',
                    'doctorsCount' => 3,
                ],
                [
                    'specialityName' => 'Chiropractic',
                    'doctorsCount' => 0,
                ],
                [
                    'specialityName' => 'Clinical Lipidology',
                    'doctorsCount' => 0,
                ],
            ],
        ];
    }
}
