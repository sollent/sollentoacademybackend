<?php
declare(strict_types=1);

namespace Functional\Doctor;

use App\Appointment\ApiResource\Appointment;
use App\Common\ApiResource\MediaObject;
use App\Doctor\ApiResource\Doctor;
use App\Doctor\ApiResource\Speciality;
use App\Insurance\ApiResource\Insurance;
use App\Language\ApiResource\Language;
use App\UserCommon\ApiResource\Profile;
use Doctrine\Common\Collections\ArrayCollection;
use Test\AbstractApiResourceTestCase;
use Test\Functional\ResponseStructure\DoctorResponseStructureInterface;

final class DoctorTest extends AbstractApiResourceTestCase
{
    public function testList(): void
    {
        $this->createEntityList(10, Doctor::class, [
            'active' => true,
        ]);

        $this->json('GET', '/api/doctors?page=1&perPage=20');

        self::assertResponseIsSuccessful();
        $this->assertListItems(10, DoctorResponseStructureInterface::SIMPLE_RESPONSE);
    }

    public function testListForPatient(): void
    {
        $patient = $this->loginAsPatient();

        // Generate some doctors
        $someDoctors = $this->createEntityList(5, Doctor::class, ['active' => true]);

        // Generate doctors with whom we will have appointment
        $doctors = $this->createEntityList(7, Doctor::class, ['active' => true]);
        \array_walk($doctors, function (Doctor $doctor) use ($patient) {
            $this->createEntity(Appointment::class, [
                'patient' => $patient,
                'doctor' => $doctor,
            ]);
        });

        // Generate some appointments
        $appointments = $this->createEntityList(5, Appointment::class);

        $this->jsonAuthenticated('GET', '/api/doctors?page=1&perPage=30');

        self::assertResponseIsSuccessful();
        $this->assertListItems(
            \count($someDoctors) + \count($doctors) + \count($appointments),
            DoctorResponseStructureInterface::SIMPLE_RESPONSE
        );

        $this->jsonAuthenticated('GET', '/api/doctors?page=1&perPage=30&own=1');

        self::assertResponseIsSuccessful();
        $this->assertListItems(\count($doctors), DoctorResponseStructureInterface::SIMPLE_RESPONSE);
    }

    public function testSearchByFirstName(): void
    {
        $patient = $this->loginAsPatient();

        $doctors = $this->createEntityList(5, Doctor::class, ['active' => true]);
        \array_walk($doctors, function (Doctor $doctor) {
            $this->createEntity(Appointment::class, [
                'doctor' => $doctor,
            ]);
        });
        $profile = new Profile();
        $profile->setFirstName('Ivan');
        $lastDoctor = $this->createEntity(Doctor::class, [
            'profile' => $profile,
        ]);
        $this->createEntity(Appointment::class, [
            'patient' => $patient,
            'doctor' => $lastDoctor,
        ]);

        $this->jsonAuthenticated('GET', '/api/doctors?page=1&perPage=20&own=1&profile.firstName=Ivan');

        self::assertResponseIsSuccessful();
        $this->assertListItems(1, DoctorResponseStructureInterface::SIMPLE_RESPONSE);
    }

    public function testSearchByLastName(): void
    {
        $patient = $this->loginAsPatient();

        $doctors = $this->createEntityList(5, Doctor::class, ['active' => true]);
        \array_walk($doctors, function (Doctor $doctor) {
            $this->createEntity(Appointment::class, [
                'doctor' => $doctor,
            ]);
        });
        $profile = new Profile();
        $profile->setLastName('Abramovich');
        $lastDoctor = $this->createEntity(Doctor::class, [
            'profile' => $profile,
        ]);
        $this->createEntity(Appointment::class, [
            'patient' => $patient,
            'doctor' => $lastDoctor,
        ]);

        $this->jsonAuthenticated('GET', '/api/doctors?page=1&perPage=20&own=1&profile.lastName=Abramovich');

        self::assertResponseIsSuccessful();
        $this->assertListItems(1, DoctorResponseStructureInterface::SIMPLE_RESPONSE);
    }

    public function testSearchBySpecialities(): void
    {
        $this->markTestSkipped();
        $patient = $this->loginAsPatient();

        $doctors = $this->createEntityList(5, Doctor::class, ['active' => true]);
        \array_walk($doctors, function (Doctor $doctor) use ($patient) {
            $this->createEntity(Appointment::class, [
                'doctor' => $doctor,
                'patient' => $patient,
            ]);
        });
        $doctorsWithSpecialities = $this->createEntityList(3, Doctor::class, [
            'specialities' => $specialities = [
                $this->createEntity(Speciality::class),
                $this->createEntity(Speciality::class),
            ],
            'active' => true,
        ]);

        foreach ($doctorsWithSpecialities as $doc) {
            $this->createEntity(Appointment::class, [
                'doctor' => $doc,
                'patient' => $patient,
            ]);
        }

        $specialityParametersString = \implode(
            '',
            \array_map(function (Speciality $speciality) {
                return \sprintf(
                    '&specialities[]=%s',
                    $this->findIriBy(Speciality::class, ['id' => $speciality->getId()])
                );
            }, $specialities)
        );

        $this->jsonAuthenticated('GET', "/api/doctors?page=1&perPage=20&own=1{$specialityParametersString}");

        self::assertResponseIsSuccessful();
        $this->assertListItems(\count($doctorsWithSpecialities), DoctorResponseStructureInterface::SIMPLE_RESPONSE);
    }

    /**
     * @param array[] $doctorsArr
     * @param array $searchData
     * @param array[] $expectedItemsArr
     *
     * @return void
     *
     * @dataProvider complexSearchingAndFilteringDataProvider
     */
    public function testComplexSearchingAndFiltering(
        array $doctorsArr,
        array $searchData,
        array $expectedItemsArr
    ): void {
        $this->markTestSkipped();
        $patient = $this->loginAsPatient(null, ['active' => true]);

        $specialityNames = \array_unique(
            \array_merge(
                ...\array_column($doctorsArr, 'specialities')
            )
        );

        $specialities = \array_map(function (string $name) {
            return $this->createEntity(Speciality::class, ['name' => $name]);
        }, $specialityNames);

        \array_walk($doctorsArr, function ($doctorArr) use ($specialities, $patient) {
            $doctor = $this->createEntity(Doctor::class, [
                'active' => true,
                'firstName' => $doctorArr['firstName'],
                'lastName' => $doctorArr['lastName'],
                'email' => $doctorArr['email'],
                'specialities' => new ArrayCollection(
                    \array_map(function (string $specialityName) use ($specialities) {
                        $filtered = \array_filter($specialities, function ($e) use (&$specialityName) {
                            return $e->getName() === $specialityName;
                        });

                        return \array_shift($filtered);
                    }, $doctorArr['specialities'])
                ),
            ]);
            $this->createEntity(Appointment::class, [
                'patient' => $patient,
                'doctor' => $doctor,
            ]);
        });

        $parameters = [
            'profile.firstName' => $searchData['firstName'],
            'profile.lastName' => $searchData['lastName'],
            'specialities' => \array_map(function (string $specialityName) use ($specialities) {
                $filtered = \array_filter($specialities, function ($e) use (&$specialityName) {
                    return $e->getName() === $specialityName;
                });
                $speciality = \array_shift($filtered);

                return $this->findIriBy(Speciality::class, ['id' => $speciality->getId()]);
            }, $searchData['specialities']),
        ];

        foreach ($parameters as $parameterName => $value) {
            if ($value === "" || $value === null) {
                unset($parameters[$parameterName]);

                continue;
            }
            $parameters[$parameterName] = \is_array($value) ?
                \implode('&', \array_map(function ($item) use ($parameterName) {
                    return $parameterName . '[]=' . $item;
                }, $value)) :
                \sprintf('%s=%s', $parameterName, $value);
        }
        $parametersString = '&' . \implode('&', $parameters);

        $this->jsonAuthenticated('GET', "/api/doctors?page=1&perPage=30$parametersString&own=1");

        self::assertResponseIsSuccessful();
        $this->assertListItems(\count($expectedItemsArr), DoctorResponseStructureInterface::SIMPLE_RESPONSE);
    }

    /**
     * @return array[]
     */
    public function complexSearchingAndFilteringDataProvider(): array
    {
        $doctorsArray = [
            [
                'firstName' => 'Ivan',
                'email' => 'ivan@mail.com',
                'lastName' => 'Antonov',
                'specialities' => [
                    'FakeSpecialityFirst',
                    'FakeSpecialitySecond',
                ],
            ],
            [
                'firstName' => 'Igor',
                'email' => 'igor@mail.com',
                'lastName' => 'Ivanov',
                'specialities' => [
                    'FakeSpecialityFirst',
                ],
            ],
            [
                'firstName' => 'Anton',
                'email' => 'anton@mail.com',
                'lastName' => 'Victorovich',
                'specialities' => [
                    'FakeSpecialityThird',
                ],
            ],
            [
                'firstName' => 'Ivan',
                'email' => 'ivanAndreevich@mail.com',
                'lastName' => 'Andreevich',
                'specialities' => [
                    'FakeSpecialitySecond',
                ],
            ],
        ];

        return [
            [
                $doctorsArray,
                [
                    'firstName' => 'Ivan',
                    'lastName' => '',
                    'specialities' => [
                        'FakeSpecialityFirst',
                    ],
                ],
                [
                    [
                        'firstName' => 'Ivan',
                        'email' => 'ivan@mail.com',
                        'lastName' => 'Antonov',
                    ],
                ],
            ],
        ];
    }

    public function testEditPersonalInformation(): void
    {
        $doctor = $this->loginAsDoctor();
        $doctorAvatar = $this->createEntity(MediaObject::class);

        // test on 'en' locale
        $json = [
            'profile' => [
                'firstName' => 'Richard',
                'lastName' => 'Genver',
                'gender' => Profile::GENDER_MALE,
                'birthDate' => '1988-12-12',
                'avatar' => $this->findIriBy(MediaObject::class, ['id' => $doctorAvatar->getId()]),
            ],
            'phoneNumber' => '786556787654',
            'workEmail' => 'richard@mail.com',
            'address' => 'some address string',
            'about' => 'Long text about doctor Long text about doctor Long text about doctor',
        ];

        $response = $this->jsonAuthenticated('PUT', "/api/doctors/" . $doctor->getId(), $json);

        self::assertResponseIsSuccessful();
        self::assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);

        // test on 'de' locale
        $json = [
            'profile' => [
                'firstName' => 'Richarden',
                'lastName' => 'Genverden',
                'gender' => Profile::GENDER_MALE,
                'birthDate' => '1988-12-12',
                'avatar' => $this->findIriBy(MediaObject::class, ['id' => $doctorAvatar->getId()]),
            ],
            'phoneNumber' => '786556787654',
            'workEmail' => 'richard@mail.com',
            'address' => 'irgendein Adress-String',
            'about' => 'Langtext über Arzt Langtext über Arzt Langtext über Arzt',
        ];

        $response = $this->jsonAuthenticated(
            'PUT',
            "/api/doctors/" . $doctor->getId(),
            $json,
            ['X-LOCALE' => 'de']
        );

        self::assertResponseIsSuccessful();
        self::assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
    }

    public function testEditInsurances(): void
    {
        $doctor = $this->loginAsDoctor();

        $insuranceIris = \array_map(function (Insurance $insurance) {
            return $this->findIriBy(Insurance::class, ['id' => $insurance->getId()]);
        }, $this->createEntityList(5, Insurance::class));

        $response = $this->jsonAuthenticated(
            'PUT',
            '/api/doctors/' . $doctor->getId(),
            ['insurances' => $insuranceIris]
        );

        self::assertResponseIsSuccessful();
        self::assertArrayStructure(DoctorResponseStructureInterface::SIMPLE_RESPONSE, $response);
    }

    /**
     * @param array $preparedDoctorNames
     * @param array $expectedDoctorNames
     *
     * @dataProvider filteringByNameDataProvider
     */
    public function testFilterOrderByName(array $preparedDoctorNames, array $expectedDoctorNames): void
    {
        foreach ($preparedDoctorNames as $preparedDoctorName) {
            $profile = new Profile();
            $profile->setFirstName($preparedDoctorName);

            $this->createEntity(Doctor::class, [
                'active' => true,
                'profile' => $profile,
            ]);
        }

        $response = $this->json('GET', '/api/doctors?page=1&perPage=25&order%5Bprofile.firstName%5D=asc');

        self::assertResponseIsSuccessful();
        foreach ($response['items'] as $index => $doctorItem) {
            self::assertSame($expectedDoctorNames[$index], $doctorItem['profile']['firstName']);
        }
    }

    /**
     * @param array $preparedDoctorCosts
     * @param array $expectedDoctorCosts
     *
     * @return void
     *
     * @dataProvider filteringByHourCostDataProvider
     */
    public function testFilterOrderByHourCost(array $preparedDoctorCosts, array $expectedDoctorCosts): void
    {
        foreach ($preparedDoctorCosts as $preparedDoctorCost) {
            $this->createEntity(Doctor::class, [
                'active' => true,
                'hourCost' => $preparedDoctorCost,
            ]);
        }

        $response = $this->json('GET', '/api/doctors?page=1&perPage=25&order%5BhourCost%5D=desc');

        self::assertResponseIsSuccessful();
        foreach ($response['items'] as $index => $doctorItem) {
            self::assertSame($expectedDoctorCosts[$index], $doctorItem['hourCost']);
        }
    }

    /**
     * @param array $preparedDoctorInsurances
     * @param array $expectedDoctorInsurances
     *
     * @return void
     *
     * @dataProvider filteringByInsurancesDataProvider
     */
    public function testFilterByInsurances(array $preparedDoctorInsurances, array $expectedDoctorInsurances): void
    {
        $this->markTestSkipped();
        foreach ($preparedDoctorInsurances as $preparedDoctorInsurance) {
            $insurance = new Insurance();
            $insurance->setTitle($preparedDoctorInsurance);

            $this->createEntity(Doctor::class, [
                'active' => true,
                'insurances' => [
                    $insurance,
                ],
            ]);
        }

        $response = $this->json('GET', "/api/doctors?page=1&perPage=25&insurances.title=$expectedDoctorInsurances[0]");

        self::assertResponseIsSuccessful();
        self::assertSame($expectedDoctorInsurances[0], $response['items']['insurances']['title']);
    }

    /**
     * @param array $preparedDoctorGenders
     * @param array $expectedDoctorGenders
     *
     * @return void
     *
     * @dataProvider filteringByGanderDataProvider
     */
    public function testFilterByGender(array $preparedDoctorGenders, array $expectedDoctorGenders): void
    {
        $this->markTestSkipped();
        foreach ($preparedDoctorGenders as $preparedDoctorGender) {
            $profile = new Profile();
            $profile->setGender($preparedDoctorGender);

            $this->createEntity(Doctor::class, [
                'active' => true,
                'profile' => $profile,
            ]);
        }
        $response = $this->json('GET', "/api/doctors?page=1&perPage=25&profile.gender=$expectedDoctorGenders[0]");

        self::assertResponseIsSuccessful();
        self::assertSame($expectedDoctorGenders[0], $response['items']['profile']['gender']);
    }

    /**
     * @param array $preparedDoctorLanguages
     * @param array $expectedDoctorLanguages
     *
     * @return void
     *
     * @dataProvider filteringByLanguagesDataProvider
     */
    public function testFilterByLanguages(array $preparedDoctorLanguages, array $expectedDoctorLanguages): void
    {
        $this->markTestSkipped();
        foreach ($preparedDoctorLanguages as $preparedDoctorLanguage) {
            $language = new Language();
            $language->setTitle($preparedDoctorLanguage);

            $this->createEntity(Doctor::class, [
                'active' => true,
                'spokenLanguages' => [
                    $language,
                ],
            ]);
        }

        $response = $this->json('GET', "/api/doctors?page=1&perPage=25&spokenLanguages=$expectedDoctorLanguages[0]");

        self::assertResponseIsSuccessful();
        self::assertSame($expectedDoctorLanguages[0], $response['items']['spokenLanguages']);
    }

    public function testFirstSmartSearchFilter(): void
    {
        $this->markTestSkipped();
    }

    public function testLocationSmartSearchFilter(): void
    {
        $this->markTestSkipped();
    }

    /**
     * @return array[]
     */
    public function filteringByNameDataProvider(): array
    {
        return [
            [
                [
                    'Maxim',
                    'Albert',
                    'Igor',
                ],
                [
                    'Albert',
                    'Igor',
                    'Maxim',
                ],
            ],
        ];
    }

    /**
     * @return array[]
     */
    public function filteringByHourCostDataProvider(): array
    {
        return [
            [
                [
                    130,
                    250,
                    90,
                ],
                [
                    250,
                    130,
                    90,
                ],
            ],
        ];
    }

    /**
     * @return array[]
     */
    public function filteringByInsurancesDataProvider(): array
    {
        return [
            [
                [
                    'FirstInsurance',
                    'SecondInsurance',
                    'ThirdInsurance',
                ],
                [
                    'SecondInsurance',
                ],
            ],
        ];
    }

    /**
     * @return array[]
     */
    public function filteringByGanderDataProvider(): array
    {
        return [
            [
                [
                    'male',
                    'female',
                    'male',
                ],
                [
                    'male',
                ],
            ],
        ];
    }

    /**
     * @return array[]
     */
    public function filteringByLanguagesDataProvider(): array
    {
        return [
            [
                [
                    'English',
                    'Russian',
                ],
                [
                    'English',
                ],
            ],
        ];
    }
}
