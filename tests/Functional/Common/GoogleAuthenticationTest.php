<?php
declare(strict_types=1);

namespace Test\Functional\Common;

use App\Common\ApiResource\DTO\UserDTOInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use League\OAuth2\Client\Grant\RefreshToken;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Test\AbstractApiResourceTestCase;

final class GoogleAuthenticationTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'token',
        'refresh_token',
    ];

    /**
     * @var array
     */
    private const VALIDATION_RESPONSE_STRUCTURE = [
        'type',
        'title',
        'detail',
        'violations',
    ];

    public function setUp(): void
    {
        parent::setUp();

        // Skip these tests until we find better solution
        $this->markTestSkipped();
    }

    public function testAuthSuccess(): void
    {
        $response = $this->json('POST', '/auth/google', [
            'accessToken' => $this->getAccessToken(),
            'userType' => UserDTOInterface::TYPE_PATIENT,
        ]);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testAuthValidation(): void
    {
        // Attempting to auth/register without 'userType' parameter
        $response = $this->json('POST', '/auth/google', [
            'accessToken' => $this->getAccessToken(),
        ]);

        self::assertResponseStatusCodeSame(422);
        $this->assertArrayStructure(self::VALIDATION_RESPONSE_STRUCTURE, $response);

        // Attempting to auth/register without 'accessToken' parameter
        $response = $this->json('POST', '/auth/google', [
            'userType' => UserDTOInterface::TYPE_DOCTOR,
        ]);

        self::assertResponseStatusCodeSame(422);
        $this->assertArrayStructure(self::VALIDATION_RESPONSE_STRUCTURE, $response);
    }

    public function testAuthFail(): void
    {
        $this->json('POST', '/auth/google', [
            'accessToken' => 'Invalid_access_token',
            'userType' => UserDTOInterface::TYPE_PATIENT,
        ]);

        self::assertResponseStatusCodeSame(500);
    }

    /**
     * @return string
     */
    private function getAccessToken(): string
    {
        $clientRegistry = self::getContainer()->get(ClientRegistry::class);
        $refreshToken = self::getContainer()
            ->get(ParameterBagInterface::class)
            ->get('googleLongLiveRefreshToken');

        return $clientRegistry
            ->getClient('google')
            ->getOAuth2Provider()
            ->getAccessToken(new RefreshToken(), ['refresh_token' => $refreshToken])
            ->getToken();
    }
}
