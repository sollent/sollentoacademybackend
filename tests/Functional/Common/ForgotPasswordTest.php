<?php
declare(strict_types=1);

namespace Test\Functional\Common;

use App\Patient\ApiResource\Patient;
use Test\AbstractApiResourceTestCase;

final class ForgotPasswordTest extends AbstractApiResourceTestCase
{
    public function testFullForgotPassword(): void
    {
        // register new user
        $patient = $this->createEntity(Patient::class);
        $patient = $this->extendWithPassword($patient, $plainPassword = 'PatientPatient');

        // request for resetting password
        $this->json('POST', '/forgot-password/reset-password', [
            'email' => $patient->getEmail(),
        ]);

        self::assertResponseIsSuccessful();

        // request for verify/check password reset token
        $this->json('POST', '/forgot-password/verify-reset-token', [
            'token' => $patient->getPasswordResetToken(),
        ]);

        self::assertResponseIsSuccessful();

        // request for changing password
        $this->json('POST', '/forgot-password/change-password', [
            'newPassword' => $newPassword = 'NewPatient',
            'token' => $patient->getPasswordResetToken(),
        ]);

        self::assertResponseIsSuccessful();

        // Attempting to auth with old password
        $this->json('POST', '/auth', [
            'email' => $patient->getEmail(),
            'password' => $plainPassword,
        ]);
        self::assertResponseStatusCodeSame(401);

        // Attempting to auth with new password
        $this->authenticate($patient, $newPassword);
    }
}
