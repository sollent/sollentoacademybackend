<?php
declare(strict_types=1);

namespace Test\Functional\Common;

use Test\AbstractApiResourceTestCase;
use Test\Functional\ResponseStructure\DoctorResponseStructureInterface;
use Test\Functional\ResponseStructure\ResponseStructuresInterface;

final class RegistrationTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    private const RESPONSE_PATIENT_STRUCTURE = [
        'id',
        'profile',
        'email',
        'roles',
    ];

    public function testRegisterSuccess(): void
    {
        $response = $this->json('POST', '/api/users', [
            'userType' => 'patient',
            'email' => 'pavel.laikov98@gmail.com',
            'firstName' => 'Pavel',
            'lastName' => 'Laikov',
            'plainPassword' => 'PatientPatient',
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_PATIENT_STRUCTURE, $response);

        $response = $this->json('POST', '/api/users', [
            'userType' => 'doctor',
            'email' => 'sollent98@gmail.com',
            'firstName' => 'Pavel',
            'lastName' => 'Laikov',
            'plainPassword' => 'DoctorDoctor',
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertArrayStructure(DoctorResponseStructureInterface::SIMPLE_RESPONSE, $response);
    }

    public function testRegisterValidation(): void
    {
        $response = $this->json('POST', '/api/users', [
            'userType' => 'patient',
            'email' => 'pavel.laikov98@gmail',
            'firstName' => 'Pavel',
            'lastName' => 'Laikov',
            'plainPassword' => 'PatientPatient',
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertArrayStructure(ResponseStructuresInterface::BAD_REQUEST, $response);

        $response = $this->json('POST', '/api/users', [
            'userType' => 'doctorr',
            'email' => 'pavel.laikov98@gmail.com',
            'firstName' => 'Pavel',
            'lastName' => 'Laikov',
            'plainPassword' => 'PatientPatient',
        ]);

        $this->assertResponseStatusCodeSame(400);
        $this->assertArrayStructure(ResponseStructuresInterface::BAD_REQUEST, $response);
    }

    public function testRegisterCheckExistsSuchUser(): void
    {
        $this->json('POST', '/api/users', [
            'userType' => 'patient',
            'email' => 'same@gmail.com',
            'firstName' => 'Pavel',
            'lastName' => 'Laikov',
            'plainPassword' => 'Simple123',
        ]);

        $this->assertResponseIsSuccessful();

        $this->json('POST', '/api/users', [
            'userType' => 'doctor',
            'email' => 'same@gmail.com',
            'firstName' => 'Pavel',
            'lastName' => 'Laikov',
            'plainPassword' => 'Simple1234',
        ]);

        $this->assertResponseStatusCodeSame(400);
    }
}
