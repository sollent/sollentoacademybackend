<?php
declare(strict_types=1);

namespace Test\Functional\Common;

use App\Doctor\ApiResource\Doctor;
use Test\AbstractApiResourceTestCase;
use Test\Functional\ResponseStructure\DoctorResponseStructureInterface;

final class ProfileTest extends AbstractApiResourceTestCase
{
    public function testEditProfileWithLocale(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            $plainPassword = \uniqid()
        );

        $doctorProfilePayload = [
            'profile' => [
                'firstName' => 'Wolfgang',
                'lastName' => 'Klein',
            ],
        ];

        $this->loginAsDoctor($doctor);

        $response = $this->jsonAuthenticated('PUT', "/api/doctors/{$doctor->getId()}", $doctorProfilePayload);

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
        self::assertSame($doctorProfilePayload['profile']['firstName'], $response['profile']['firstName']);
        self::assertSame($doctorProfilePayload['profile']['lastName'], $response['profile']['lastName']);

        // Edit profile with 'en' locale
        $doctorProfilePayload = [
            'profile' => [
                'firstName' => 'EnglishFirstName',
                'lastName' => 'EnglishLastName',
            ],
        ];

        $headers[self::LOCALE_HEADER] = 'en';
        $response = $this->jsonAuthenticated('PUT', "/api/doctors/{$doctor->getId()}", $doctorProfilePayload, $headers);

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
        self::assertSame($doctorProfilePayload['profile']['firstName'], $response['profile']['firstName']);
        self::assertSame($doctorProfilePayload['profile']['lastName'], $response['profile']['lastName']);
    }

    public function testNewProfileDefaultLocale(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            \uniqid()
        );

        $doctorProfilePayload = [
            'profile' => [
                'firstName' => 'Wolfgang',
                'lastName' => 'Klein',
            ],
        ];

        $this->loginAsDoctor($doctor);

        $response = $this->jsonAuthenticated('PUT', "/api/doctors/{$doctor->getId()}", $doctorProfilePayload);

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
        self::assertSame($doctorProfilePayload['profile']['firstName'], $response['profile']['firstName']);
        self::assertSame($doctorProfilePayload['profile']['lastName'], $response['profile']['lastName']);

        // Checking for autocomplete for other locale of entity translations
        $headers[self::LOCALE_HEADER] = 'en';
        $response = $this->jsonAuthenticated('GET', "/api/doctors/{$doctor->getId()}", null, $headers);

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
        self::assertSame($doctorProfilePayload['profile']['firstName'], $response['profile']['firstName']);
        self::assertSame($doctorProfilePayload['profile']['lastName'], $response['profile']['lastName']);
    }

    public function testNewProfileWithSpecificLocale(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            $plainPassword = \uniqid()
        );

        $doctorProfilePayload = [
            'profile' => [
                'firstName' => 'Alex',
                'lastName' => 'Ivanov',
            ],
        ];

        $this->loginAsDoctor($doctor);

        $headers[self::LOCALE_HEADER] = 'en';
        $response = $this->jsonAuthenticated('PUT', "/api/doctors/{$doctor->getId()}", $doctorProfilePayload, $headers);

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
        self::assertSame($doctorProfilePayload['profile']['firstName'], $response['profile']['firstName']);
        self::assertSame($doctorProfilePayload['profile']['lastName'], $response['profile']['lastName']);

        // Checking for autocomplete for other locale of entity translations
        $headers[self::LOCALE_HEADER] = 'en';
        $response = $this->jsonAuthenticated('GET', "/api/doctors/{$doctor->getId()}", null, $headers);

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
        self::assertSame($doctorProfilePayload['profile']['firstName'], $response['profile']['firstName']);
        self::assertSame($doctorProfilePayload['profile']['lastName'], $response['profile']['lastName']);
    }
}
