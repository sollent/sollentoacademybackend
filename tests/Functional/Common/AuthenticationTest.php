<?php
declare(strict_types=1);

namespace Test\Functional\Common;

use App\Patient\ApiResource\Patient;
use Test\AbstractApiResourceTestCase;

final class AuthenticationTest extends AbstractApiResourceTestCase
{
    public function testLoginFail(): void
    {
        $patient = $this->createEntity(Patient::class);
        $patient = $this->extendWithPassword($patient, $plainPassword = 'PatientPatient');

        $this->json('POST', '/auth', [
            'email' => $patient->getEmail(),
            'password' => 'InvalidPassword',
        ]);

        $this->assertResponseStatusCodeSame(401);

        $this->json('POST', '/auth', [
            'email' => 'invalidEmail@gmail.com',
            'password' => $plainPassword,
        ]);

        $this->assertResponseStatusCodeSame(401);
    }

    public function testLoginSuccess(): void
    {
        /** @var \App\Patient\ApiResource\Patient $patient */
        $patient = $this->extendWithPassword(
            $this->createEntity(Patient::class),
            $plainPassword = 'PatientPatient'
        );

        $this->json('POST', '/auth', [
            'email' => $patient->getEmail(),
            'password' => $plainPassword,
        ]);

        self::assertResponseIsSuccessful();
    }

    /**
     * Update user token using refreshToken
     */
    public function testRelogin(): void
    {
        $patient = $this->createEntity(Patient::class);
        $patient = $this->extendWithPassword($patient, $plainPassword = 'PatientPatient');

        $response = $this->authenticate($patient, $plainPassword);
        $this->assertArrayHasKey('refresh_token', $response);

        $response = $this->json('POST', '/token/refresh', [
            'refresh_token' => $response['refresh_token'],
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertArrayHasKey('token', $response);
        $this->assertArrayHasKey('refresh_token', $response);
    }
}
