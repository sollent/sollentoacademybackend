<?php
declare(strict_types=1);

namespace Test\Functional\Common;

use App\Common\ApiResource\DTO\UserDTOInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Test\AbstractApiResourceTestCase;
use Test\Functional\ResponseStructure\ResponseStructuresInterface;

final class FacebookAuthenticationTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'token',
        'refresh_token',
    ];

    public function setUp(): void
    {
        parent::setUp();

        // Skip these tests until we find better solution
        $this->markTestSkipped();
    }

    public function testAuthSuccess(): void
    {
        $response = $this->json('POST', '/auth/facebook', [
            'accessToken' => $this->getAccessToken(),
            'userType' => UserDTOInterface::TYPE_PATIENT,
        ]);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testAuthValidation(): void
    {
        // Attempting to auth/register without 'userType' parameter
        $response = $this->json('POST', '/auth/facebook', [
            'accessToken' => $this->getAccessToken(),
        ]);

        self::assertResponseStatusCodeSame(400);
        $this->assertArrayStructure(ResponseStructuresInterface::BAD_REQUEST, $response);

        // Attempting to auth/register without 'accessToken' parameter
        $response = $this->json('POST', '/auth/facebook', [
            'userType' => UserDTOInterface::TYPE_DOCTOR,
        ]);

        self::assertResponseStatusCodeSame(400);
        $this->assertArrayStructure(ResponseStructuresInterface::BAD_REQUEST, $response);
    }

    public function testAuthFail(): void
    {
        $this->json('POST', '/auth/facebook', [
            'accessToken' => 'Invalid_access_token',
            'userType' => UserDTOInterface::TYPE_PATIENT,
        ]);

        self::assertResponseStatusCodeSame(500);
    }

    /**
     * @return string
     */
    private function getAccessToken(): string
    {
        return self::getContainer()
            ->get(ParameterBagInterface::class)
            ->get('facebookLongLiveAccessToken');
    }
}
