<?php
declare(strict_types=1);

namespace Test\Functional\Common;

use App\Common\ApiResource\MediaObject;
use App\Doctor\ApiResource\Doctor;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Test\AbstractApiResourceTestCase;
use Test\Functional\ResponseStructure\DoctorResponseStructureInterface;

final class MediaObjectTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'contentUrl',
    ];

    public function setUp(): void
    {
        // prepare image file for test
        $fs = new Filesystem();
        $fs->copy('fixtures/image.png', 'fixtures/tmp/image.png');

        parent::setUp();
    }

    public function tearDown(): void
    {
        // clear tmp and media directory
        // need to remove only test files in the future
        $fs = new Filesystem();
//        $fs->remove('public/media/');
        $fs->remove('fixtures/tmp/');

        parent::tearDown();
    }

    public function testList(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $this->createEntityList(3, MediaObject::class);

        $this->jsonAuthenticated('GET', '/api/media_objects');

        $this->assertResponseStatusCodeSame(403);
    }

    public function testNew(): void
    {
        $doctor = $this->createEntity(Doctor::class);
        $doctor = $this->extendWithPassword($doctor, $plainPassword = 'DoctorDoctor');

        $token = $this->authenticate($doctor, $plainPassword)['token'];

        $file = new UploadedFile('fixtures/tmp/image.png', 'image.png');

        $headers['Authorization'] = "Bearer $token";
        $headers['Content-Type'] = 'multipart/form-data';
        $headers['Accept'] = 'application/json';

        $response = $this->request('POST', '/api/media_objects', [
            'headers' => $headers,
            'extra' => [
                'files' => [
                    'file' => $file,
                ],
            ],
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response->toArray(false));
    }

    public function testNormalization(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $picture = $this->createEntity(MediaObject::class);
        $pictureIri = $this->findIriBy(MediaObject::class, ['id' => $picture->getId()]);

        // Edit doctor profile
        $json = [
            'profile' => [
                'firstName' => 'Doci',
                'lastName' => 'DociDoci',
                'avatar' => $pictureIri,
            ],
        ];
        $response = $this->jsonAuthenticated(
            'PUT',
            $this->findIriBy(Doctor::class, ['id' => $doctor->getId()]),
            $json
        );

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(DoctorResponseStructureInterface::WITH_PROFILE_RESPONSE, $response);
    }

    public function testShow(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $picture = $this->createEntity(MediaObject::class);
        $pictureIri = $this->findIriBy(MediaObject::class, ['id' => $picture->getId()]);

        $response = $this->jsonAuthenticated('GET', $pictureIri);

        $this->assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }
}
