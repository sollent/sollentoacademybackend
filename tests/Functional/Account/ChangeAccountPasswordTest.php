<?php
declare(strict_types=1);

namespace Test\Functional\Account;

use App\Doctor\ApiResource\Doctor;
use App\Patient\ApiResource\Patient;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Test\AbstractApiResourceTestCase;
use Test\Functional\ResponseStructure\ResponseStructuresInterface;

final class ChangeAccountPasswordTest extends AbstractApiResourceTestCase
{
    public function testChangePasswordSuccessDoctor(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class, ['active' => true]),
            $doctorPlainPassword = 'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $this->jsonAuthenticated(
            'POST',
            '/api/change-password',
            $json = [
                'currentPassword' => $doctorPlainPassword,
                'newPassword' => \uniqid(),
            ]
        );

        $userPasswordHasher = self::getContainer()->get(UserPasswordHasherInterface::class);
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        self::assertResponseIsSuccessful();
        self::assertEquals(true, $userPasswordHasher->isPasswordValid(
            $entityManager->getRepository(Doctor::class)->find($doctor->getId()),
            $json['newPassword']
        ));
    }

    public function testChangePasswordSuccessPatient(): void
    {
        /** @var \App\Patient\ApiResource\Patient $patient */
        $patient = $this->extendWithPassword(
            $this->createEntity(Patient::class, ['active' => true]),
            $patientPlainPassword = 'PatientPatient'
        );
        $this->loginAsPatient($patient);

        $this->jsonAuthenticated(
            'POST',
            '/api/change-password',
            $json = [
                'currentPassword' => $patientPlainPassword,
                'newPassword' => \uniqid(),
            ]
        );

        $userPasswordHasher = self::getContainer()->get(UserPasswordHasherInterface::class);
        $entityManager = self::getContainer()->get(EntityManagerInterface::class);

        self::assertResponseIsSuccessful();
        self::assertEquals(true, $userPasswordHasher->isPasswordValid(
            $entityManager->getRepository(Patient::class)->find($patient->getId()),
            $json['newPassword']
        ));
    }

    public function testChangePasswordValidationFailed(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class, ['active' => true]),
            'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $response = $this->jsonAuthenticated(
            'POST',
            '/api/change-password',
            [
                'currentPassword' => '123',
                'newPassword' => \uniqid(),
            ]
        );

        self::assertResponseStatusCodeSame(400);
        $this->assertArrayStructure(ResponseStructuresInterface::BAD_REQUEST, $response);
        $this->assertCount(1, $response['violations']);
    }

    public function testInvalidCurrentPasswordProvided(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class, ['active' => true]),
            'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $this->jsonAuthenticated(
            'POST',
            '/api/change-password',
            [
                'currentPassword' => 'InvalidPassword',
                'newPassword' => \uniqid(),
            ]
        );

        self::assertResponseStatusCodeSame(400);
    }

    public function testChangePasswordSecurity(): void
    {
        $json = [
            'currentPassword' => 'ExamplePassword',
            'newPassword' => \uniqid(),
        ];
        $this->json('POST', '/api/change-password', $json);

        self::assertResponseStatusCodeSame(401);

        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            $doctorPlainPassword = 'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $this->jsonAuthenticated(
            'POST',
            '/api/change-password',
            [
                'currentPassword' => $doctorPlainPassword,
                'newPassword' => \uniqid(),
            ]
        );

        self::assertResponseStatusCodeSame(403);
    }
}
