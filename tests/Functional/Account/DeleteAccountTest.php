<?php
declare(strict_types=1);

namespace Test\Functional\Account;

use App\Account\ApiResource\DeletedAccount;
use App\Doctor\ApiResource\Doctor;
use App\Patient\ApiResource\Patient;
use Test\AbstractApiResourceTestCase;
use Test\Functional\ResponseStructure\ResponseStructuresInterface;

final class DeleteAccountTest extends AbstractApiResourceTestCase
{
    public function testDeleteAccountSuccessDoctor(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            $plainPassword = 'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $this->jsonAuthenticated(
            'POST',
            '/api/delete-account',
            ['currentPassword' => $plainPassword]
        );

        self::notSeeEntity(Doctor::class, ['id' => $doctor->getId()]);
        self::seeEntity(DeletedAccount::class, ['oldId' => $doctor->getId()]);
    }

    public function testDeleteAccountSuccessPatient(): void
    {
        /** @var \App\Patient\ApiResource\Patient $patient */
        $patient = $this->extendWithPassword(
            $this->createEntity(Patient::class),
            $plainPassword = 'PatientPatient'
        );
        $this->loginAsPatient($patient);

        $this->jsonAuthenticated(
            'POST',
            '/api/delete-account',
            ['currentPassword' => $plainPassword]
        );

        self::notSeeEntity(Patient::class, ['id' => $patient->getId()]);
        self::seeEntity(DeletedAccount::class, ['oldId' => $patient->getId()]);
    }

    public function testValidationFailed(): void
    {
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        $doctor = $this->extendWithPassword(
            $this->createEntity(Doctor::class),
            'DoctorDoctor'
        );
        $this->loginAsDoctor($doctor);

        $response = $this->jsonAuthenticated(
            'POST',
            '/api/delete-account',
            ['currentPassword' => '123']
        );

        self::assertResponseStatusCodeSame(400);
        $this->assertArrayStructure(ResponseStructuresInterface::BAD_REQUEST, $response);
        $this->assertCount(1, $response['violations']);
    }

    public function testInvalidCurrentPasswordProvided(): void
    {
        /** @var \App\Patient\ApiResource\Patient $patient */
        $patient = $this->extendWithPassword(
            $this->createEntity(Patient::class),
            'PatientPatient'
        );
        $this->loginAsPatient($patient);

        $this->jsonAuthenticated(
            'POST',
            '/api/delete-account',
            ['currentPassword' => 'InvalidPassword', ]
        );

        self::assertResponseStatusCodeSame(400);
    }

    public function testSecurity(): void
    {
        $json = [
            'currentPassword' => 'ExamplePassword',
        ];
        $this->json('POST', '/api/delete-account', $json);

        self::assertResponseStatusCodeSame(401);
    }
}
