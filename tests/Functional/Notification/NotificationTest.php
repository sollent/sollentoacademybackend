<?php
declare(strict_types=1);

namespace Test\Functional\Notification;

use App\Doctor\ApiResource\Doctor;
use App\Notification\ApiResource\Notification;
use App\Notification\ApiResource\NotificationEventInterface;
use App\Patient\ApiResource\Patient;
use Carbon\Carbon;
use Test\AbstractApiResourceTestCase;

final class NotificationTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'createdAt',
        'beautyCreatedAt',
        'id',
        'message',
    ];

    public function testListAllBySuperAdmin(): void
    {
        $this->createEntityList(2, Notification::class);

        $this->jsonAuthenticated('GET', '/api/notifications');

        self::assertResponseIsSuccessful();
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
    }

    public function testListOnlyOwnedNotificationsForDoctor(): void
    {
        $doctor = $this->createEntity(Doctor::class);
        $notification1 = $this->createEntity(Notification::class, ['receiver' => $doctor]);
        $this->createEntity(Notification::class);
        $notification2 = $this->createEntity(Notification::class, ['receiver' => $doctor]);
        $this->loginAsDoctor($doctor);

        $this->jsonAuthenticated('GET', '/api/notifications');

        self::assertResponseIsSuccessful();
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
        $this->assertListContainsIds([$notification1->getId(), $notification2->getId()]);
    }

    public function testListOnlyOwnedNotificationsForPatient(): void
    {
        $patient = $this->createEntity(Patient::class);
        $notification1 = $this->createEntity(Notification::class, ['receiver' => $patient]);
        $this->createEntity(Notification::class);
        $notification2 = $this->createEntity(Notification::class, ['receiver' => $patient]);
        $this->loginAsPatient($patient);

        $response = $this->jsonAuthenticated('GET', '/api/notifications');

        self::assertResponseIsSuccessful();
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
        $this->assertListContainsIds([$notification1->getId(), $notification2->getId()]);
    }

    public function testReadAllSucceedsForNonSuperAdmin(): void
    {
        $patient = $this->loginAsPatient();
        $notification1 = $this->createEntity(Notification::class, ['receiver' => $patient]);
        $notification2 = $this->createEntity(Notification::class);

        $this->jsonAuthenticated('POST', '/api/notifications/read-all');

        self::assertResponseStatusCodeSame(200);
        $this->seeEntity(Notification::class, [
            'id' => $notification1->getId(),
            'status' => Notification::STATUS_VIEWED,
        ]);
        $this->seeEntity(Notification::class, [
            'id' => $notification2->getId(),
            'status' => Notification::STATUS_NEW,
        ]);
    }

    public function testReadAllSuceeedsForSuperAdmin(): void
    {
        $this->createEntityList(2, Notification::class);
        Carbon::setTestNow('2022-01-02 01:01:01');

        $this->jsonAuthenticated('POST', '/api/notifications/read-all');

        self::assertResponseStatusCodeSame(200);
        $this->countEntities(Notification::class, 2, [
            'status' => Notification::STATUS_VIEWED,
            'viewedAt' => Carbon::now(),
        ]);
    }

    public function testReadFailsWhenNotificationNotFound(): void
    {
        $notification = $this->createEntity(Notification::class);

        $this->jsonAuthenticated('POST', '/api/notifications/read', [
            'ids' => [$notification->getId(), $this->faker->uuid],
        ]);

        self::assertResponseStatusCodeSame(404);
    }

    public function testReadFailsWithNotOwnedNotification(): void
    {
        $notification = $this->createEntity(Notification::class, [
            'receiver' => $this->createEntity(Patient::class),
        ]);
        $this->loginAsPatient();

        $this->jsonAuthenticated('POST', '/api/notifications/read', [
            'ids' => [$notification->getId()],
        ]);

        self::assertResponseStatusCodeSame(404);
    }

    public function testReadSucceedsForDoctor(): void
    {
        $doctor = $this->loginAsDoctor();
        $notification1 = $this->createEntity(Notification::class, ['receiver' => $doctor]);
        $notification2 = $this->createEntity(Notification::class, ['receiver' => $doctor]);
        Carbon::setTestNow('2022-01-02 01:01:01');

        $this->jsonAuthenticated('POST', '/api/notifications/read', [
            'ids' => [$notification1->getId(), $notification2->getId()],
        ]);

        self::assertResponseStatusCodeSame(200);
        $this->countEntities(Notification::class, 2, [
            'status' => Notification::STATUS_VIEWED,
            'viewedAt' => Carbon::now(),
        ]);
    }

    public function testReadSucceedsForPatient(): void
    {
        $patient = $this->loginAsPatient();
        $notification1 = $this->createEntity(Notification::class, ['receiver' => $patient]);
        $notification2 = $this->createEntity(Notification::class, ['receiver' => $patient]);
        Carbon::setTestNow('2022-01-02 01:01:01');

        $this->jsonAuthenticated('POST', '/api/notifications/read', [
            'ids' => [$notification1->getId(), $notification2->getId()],
        ]);

        self::assertResponseStatusCodeSame(200);
        $this->countEntities(Notification::class, 2, [
            'status' => Notification::STATUS_VIEWED,
            'viewedAt' => Carbon::now(),
        ]);
    }

    public function testReadSucceedsForSuperAdmin(): void
    {
        $notification = $this->createEntity(Notification::class);
        Carbon::setTestNow('2022-01-02 01:01:01');

        $this->jsonAuthenticated('POST', '/api/notifications/read', [
            'ids' => [$notification->getId()],
        ]);

        self::assertResponseStatusCodeSame(200);
        $this->seeEntity(Notification::class, [
            'status' => Notification::STATUS_VIEWED,
            'viewedAt' => Carbon::now(),
        ]);
    }

    public function testShowFailsWhenNotificationIsNotOwned(): void
    {
        $this->loginAsPatient();
        $notification = $this->createEntity(Notification::class);
        $notificationIri = $this->findIriBy(Notification::class, ['id' => $notification->getId()]);

        $this->jsonAuthenticated('GET', $notificationIri);

        self::assertResponseStatusCodeSame(404);
    }

    public function testShowSucceedsForDoctor(): void
    {
        $doctor = $this->loginAsDoctor();
        $notification = $this->createEntity(Notification::class, ['receiver' => $doctor]);
        $notificationIri = $this->findIriBy(Notification::class, ['id' => $notification->getId()]);
        Carbon::setTestNow(
            (new Carbon($notification->getCreatedAt()))->addSeconds(5)
        );

        $response = $this->jsonAuthenticated('GET', $notificationIri);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame('5 seconds ago', $response['beautyCreatedAt']);
    }

    public function testShowSucceedsForPatient(): void
    {
        $patient = $this->loginAsPatient();
        $notification = $this->createEntity(Notification::class, ['receiver' => $patient]);
        $notificationIri = $this->findIriBy(Notification::class, ['id' => $notification->getId()]);
        Carbon::setTestNow(
            (new Carbon($notification->getCreatedAt()))->addSeconds(5)
        );

        $response = $this->jsonAuthenticated('GET', $notificationIri);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame('5 seconds ago', $response['beautyCreatedAt']);
    }

    public function testShowSucceedsWithMessageTranslation(): void
    {
        $notification = $this->createEntity(Notification::class, [
            'event' => NotificationEventInterface::EVENT_ARTICLE_ADDED,
            'eventData' => ['name' => 'Murzik'],
            'locale' => 'ru',
        ]);
        $notificationIri = $this->findIriBy(Notification::class, ['id' => $notification->getId()]);

        $response = $this->jsonAuthenticated('GET', $notificationIri, null, [self::LOCALE_HEADER => 'en']);

        self::assertResponseIsSuccessful();
        self::assertSame('New article added.', $response['message']);
    }

    public function testShowSucceedsWithoutTranslation(): void
    {
        $notification = $this->createEntity(Notification::class, [
            'event' => NotificationEventInterface::EVENT_ARTICLE_ADDED,
            'message' => 'some-message',
            'locale' => 'en',
        ]);
        $notificationIri = $this->findIriBy(Notification::class, ['id' => $notification->getId()]);

        $response = $this->jsonAuthenticated('GET', $notificationIri, null, [self::LOCALE_HEADER => 'en']);

        self::assertResponseIsSuccessful();
        self::assertSame('some-message', $response['message']);
    }
}
