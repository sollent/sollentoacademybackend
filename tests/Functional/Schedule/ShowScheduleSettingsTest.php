<?php
declare(strict_types=1);

namespace Test\Functional\Schedule;

use App\Schedule\ApiResource\ScheduleSettings;

final class ShowScheduleSettingsTest extends AbstractScheduleSettingsTest
{
    public function testShowForbiddenForNonOwnedSettings(): void
    {
        $scheduleSettings = $this->createEntity(ScheduleSettings::class);
        $this->loginAsDoctor();

        $this->jsonAuthenticated(
            'GET',
            \sprintf('/api/doctors/%s/schedule_settings', $scheduleSettings->getDoctor()->getId())
        );

        self::assertResponseStatusCodeSame(403);
    }

    public function testShowSucceedsForDoctor(): void
    {
        $doctor = $this->loginAsDoctor();
        $this->createEntity(ScheduleSettings::class, ['doctor' => $doctor]);

        $response = $this->jsonAuthenticated(
            'GET',
            \sprintf('/api/doctors/%s/schedule_settings', $doctor->getId())
        );

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }

    public function testShowSucceedsForSuperAdmin(): void
    {
        $scheduleSettings = $this->createEntity(ScheduleSettings::class);

        $response = $this->jsonAuthenticated(
            'GET',
            \sprintf('/api/doctors/%s/schedule_settings', $scheduleSettings->getDoctor()->getId())
        );

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
    }
}
