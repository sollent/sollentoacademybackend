<?php
declare(strict_types=1);

namespace Test\Functional\Schedule;

use App\Schedule\ApiResource\AbstractWorkingTimeSettings;
use App\Schedule\ApiResource\ScheduleBreak;
use App\Schedule\ApiResource\ScheduleSettings;
use App\Schedule\ApiResource\WorkingTimeSettingsByDay;
use App\Schedule\ApiResource\WorkingTimeSettingsCommon;
use Test\InvalidDataMaker;

final class CreateScheduleSettingsTest extends AbstractScheduleSettingsTest
{
    /**
     * @return iterable<mixed>
     */
    public function provideInvalidData(): iterable
    {
        yield from InvalidDataMaker::make('workingTimeType')->yieldInvalidChoice();

        yield from InvalidDataMaker::make('workingDays')
            ->message('This value should be of type integer.')
            ->asArrayElement()
            ->yieldNonDigitSymbols();
        yield from InvalidDataMaker::make('workingDays')->propertyPath('workingDays')->yieldArrayWithDuplicatedItems(3);

        yield from InvalidDataMaker::make('appointmentInterval')
            ->wrapWith('workingTimeSettingsCommon')
            ->yieldOutOfRangeNumber(
                AbstractWorkingTimeSettings::MIN_APPOINTMENT_INTERVAL,
                AbstractWorkingTimeSettings::MAX_APPOINTMENT_INTERVAL
            );
    }

    public function testCreateFailsOnBreakTimeOverlapping(): void
    {
        $this->loginAsDoctor();
        $data = \array_merge(
            $this->getEntityRaw(ScheduleSettings::class, [
                'workingTimeType',
                'workingDays',
                'workOnHoliday',
            ]),
            [
                'workingTimeSettingsCommon' => [
                    'appointmentInterval' => 10,
                    'workingTimeStart' => '10:00',
                    'workingTimeEnd' => '20:00',
                    'breaks' => [
                        ['timeFrom' => '10:00', 'timeTill' => '12:00'],
                        ['timeFrom' => '10:30', 'timeTill' => '11:00'],
                    ],
                ],
            ]
        );

        $this->jsonAuthenticated('POST', '/api/schedule_settings', $data);

        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks',
            'Breaks must not overlap.'
        );
    }

    public function testCreateFailsOnInvalidBreakTime(): void
    {
        $this->loginAsDoctor();
        $data = \array_merge(
            $this->getEntityRaw(ScheduleSettings::class, [
                'workingTimeType',
                'workingDays',
                'workOnHoliday',
            ]),
            [
                'workingTimeSettingsCommon' => [
                    'appointmentInterval' => 10,
                    'workingTimeStart' => '10:00',
                    'workingTimeEnd' => '20:00',
                    'breaks' => [
                        ['timeFrom' => '12:00', 'timeTill' => '10:00'],
                    ],
                ],
            ]
        );

        $this->jsonAuthenticated('POST', '/api/schedule_settings', $data);

        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks[0].timeFrom',
            'Break start time should be less than end time.'
        );
    }

    public function testCreateFailsOnInvalidWorkingTime(): void
    {
        $this->loginAsDoctor();
        $data = \array_merge(
            $this->getEntityRaw(ScheduleSettings::class, [
                'workingTimeType',
                'workingDays',
                'workOnHoliday',
            ]),
            [
                'workingTimeSettingsCommon' => [
                    'appointmentInterval' => 10,
                    'workingTimeStart' => '20:00',
                    'workingTimeEnd' => '10:00',
                ],
            ]
        );

        $this->jsonAuthenticated('POST', '/api/schedule_settings', $data);

        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.workingTimeStart',
            'Working start time should be less than end time.'
        );
    }

    public function testCreateFailsWhenBreaksOutsideWorkingTime(): void
    {
        $this->loginAsDoctor();
        $data = \array_merge(
            $this->getEntityRaw(ScheduleSettings::class, [
                'workingTimeType',
                'workingDays',
                'workOnHoliday',
            ]),
            [
                'workingTimeSettingsCommon' => [
                    'appointmentInterval' => 10,
                    'workingTimeStart' => '10:00',
                    'workingTimeEnd' => '20:00',
                    'breaks' => [
                        ['timeFrom' => '09:00', 'timeTill' => '12:00'],
                        ['timeFrom' => '19:00', 'timeTill' => '20:30'],
                        ['timeFrom' => '09:00', 'timeTill' => '22:00'],
                        ['timeFrom' => '08:00', 'timeTill' => '09:00'],
                        ['timeFrom' => '21:00', 'timeTill' => '22:00'],
                        ['timeFrom' => '13:00', 'timeTill' => '14:00'],
                    ],
                ],
            ]
        );

        $this->jsonAuthenticated('POST', '/api/schedule_settings', $data);

        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks[0].timeFrom',
            'Break should be in border of working time.'
        );
        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks[1].timeTill',
            'Break should be in border of working time.'
        );
        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks[2].timeFrom',
            'Break should be in border of working time.'
        );
        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks[2].timeTill',
            'Break should be in border of working time.'
        );
        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks[3].timeFrom',
            'Break should be in border of working time.'
        );
        $this->assertEntityValidationError(
            'workingTimeSettingsCommon.breaks[4].timeTill',
            'Break should be in border of working time.'
        );
    }

    /**
     * @param mixed[] $invalidData
     *
     * @dataProvider provideInvalidData
     */
    public function testCreateFailsWithInvalidData(
        array $invalidData,
        string $propertyPath,
        string $expectedMessage
    ): void {
        $this->loginAsDoctor();
        $data = \array_merge($this->getEntityRaw(ScheduleSettings::class, [
            'workingTimeType',
            'workingDays',
            'workOnHoliday',
        ]), $invalidData);

        $this->jsonAuthenticated('POST', '/api/schedule_settings', $data);

        $this->assertEntityValidationError($propertyPath, $expectedMessage);
        $this->notSeeEntity(ScheduleSettings::class);
    }

    public function testCreateSucceeds(): void
    {
        $this->loginAsDoctor();

        $response = $this->jsonAuthenticated(
            'POST',
            '/api/schedule_settings',
            [
                'workingTimeType' => ScheduleSettings::WORKING_TIME_TYPE_COMMON,
                'workingDays' => [1, 2, 3, 4, 5],
                'workOnHoliday' => true,
                'workingTimeSettingsCommon' => [
                    'appointmentInterval' => 30,
                    'workingTimeStart' => '10:00',
                    'workingTimeEnd' => '20:00',
                    'breaks' => [
                        ['timeFrom' => '13:00', 'timeTill' => '14:00'],
                    ],
                ],
                'workingTimeSettingsByDays' => [
                    [
                        'appointmentInterval' => 30,
                        'day' => 1,
                        'workingTimeStart' => '10:00',
                        'workingTimeEnd' => '20:00',
                        'breaks' => [
                            ['timeFrom' => '13:00', 'timeTill' => '14:00'],
                        ],
                    ],
                ],
            ]
        );

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        // Common settings assertions
        $workingTimeSettingsCommon = $this->findOneEntity(WorkingTimeSettingsCommon::class, [
            'appointmentInterval' => 30,
            'workingTimeEnd' => '20:00',
            'workingTimeStart' => '10:00',
        ]);
        self::assertNotNull($workingTimeSettingsCommon);
        // By days settings assertions
        $workingTimeSettingsByDays = $this->findOneEntity(WorkingTimeSettingsByDay::class, [
            'appointmentInterval' => 30,
            'day' => 1,
            'scheduleSettings' => $response['id'],
            'workingTimeEnd' => '20:00',
            'workingTimeStart' => '10:00',
        ]);
        self::assertNotNull($workingTimeSettingsByDays);
        // Schedule breaks assertions
        $this->seeEntity(ScheduleBreak::class, [
            'timeFrom' => '13:00',
            'timeTill' => '14:00',
            'workingTimeSettings' => $workingTimeSettingsCommon,
        ]);
        $this->seeEntity(ScheduleBreak::class, [
            'timeFrom' => '13:00',
            'timeTill' => '14:00',
            'workingTimeSettings' => $workingTimeSettingsByDays,
        ]);
        $this->countEntities(ScheduleBreak::class, 2);
        // ScheduleSettings assertions
        $this->seeEntity(
            ScheduleSettings::class,
            [
                'id' => $response['id'],
                'workOnHoliday' => true,
                'workingDays' => [1, 2, 3, 4, 5],
                'workingTimeSettingsCommon' => $workingTimeSettingsCommon,
                'workingTimeType' => ScheduleSettings::WORKING_TIME_TYPE_COMMON,
            ],
            ['workingDays']
        );
    }

    public function testUpdateSucceeds(): void
    {
        $doctor = $this->loginAsDoctor();
        $scheduleSettings = $this->createEntity(ScheduleSettings::class, [
            'doctor' => $doctor,
            'workOnHoliday' => true,
            'workingDays' => [1, 2, 3],
            'workingTimeType' => ScheduleSettings::WORKING_TIME_TYPE_COMMON,
        ]);

        $response = $this->jsonAuthenticated('POST', '/api/schedule_settings', [
            'workingTimeType' => ScheduleSettings::WORKING_TIME_TYPE_BY_DATE,
            'workingDays' => [1, 3, 5],
            'workOnHoliday' => false,
            'workingTimeSettingsCommon' => [
                'appointmentInterval' => 10,
                'workingTimeStart' => '13:00',
                'workingTimeEnd' => '21:00',
                'breaks' => [
                    ['timeFrom' => '15:00', 'timeTill' => '16:00'],
                ],
            ],
            'workingTimeSettingsByDays' => [
                [
                    'appointmentInterval' => 11,
                    'day' => 3,
                    'workingTimeStart' => '08:00',
                    'workingTimeEnd' => '23:00',
                    'breaks' => [
                        ['timeFrom' => '16:00', 'timeTill' => '17:00'],
                    ],
                ],
            ],
        ]);

        self::assertResponseStatusCodeSame(201);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        // Common settings assertions
        $workingTimeSettingsCommon = $this->findOneEntity(WorkingTimeSettingsCommon::class, [
            'appointmentInterval' => 10,
            'workingTimeStart' => '13:00',
            'workingTimeEnd' => '21:00',
        ]);
        self::assertNotNull($workingTimeSettingsCommon);
        $this->countEntities(WorkingTimeSettingsCommon::class, 1);
        // By days settings assertions
        $workingTimeSettingsByDays = $this->findOneEntity(WorkingTimeSettingsByDay::class, [
            'appointmentInterval' => 11,
            'day' => 3,
            'scheduleSettings' => $scheduleSettings->getId(),
            'workingTimeEnd' => '23:00',
            'workingTimeStart' => '8:00',
        ]);
        self::assertNotNull($workingTimeSettingsByDays);
        $this->countEntities(WorkingTimeSettingsByDay::class, 1);
        // Schedule breaks assertions
        $this->seeEntity(ScheduleBreak::class, [
            'timeFrom' => '15:00',
            'timeTill' => '16:00',
            'workingTimeSettings' => $workingTimeSettingsCommon,
        ]);
        $this->seeEntity(ScheduleBreak::class, [
            'timeFrom' => '16:00',
            'timeTill' => '17:00',
            'workingTimeSettings' => $workingTimeSettingsByDays,
        ]);
        $this->countEntities(ScheduleBreak::class, 2);
        // Schedule settings assertions
        $this->seeEntity(
            ScheduleSettings::class,
            [
                'doctor' => $doctor->getId(),
                'id' => $scheduleSettings->getId(),
                'workOnHoliday' => false,
                'workingDays' => [1, 3, 5],
                'workingTimeSettingsCommon' => $workingTimeSettingsCommon,
                'workingTimeType' => ScheduleSettings::WORKING_TIME_TYPE_BY_DATE,
            ],
            ['workingDays']
        );
        $this->countEntities(ScheduleSettings::class, 1);
    }
}
