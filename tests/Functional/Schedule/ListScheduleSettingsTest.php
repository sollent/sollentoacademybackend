<?php
declare(strict_types=1);

namespace Test\Functional\Schedule;

use App\Schedule\ApiResource\ScheduleSettings;

final class ListScheduleSettingsTest extends AbstractScheduleSettingsTest
{
    public function testListSucceeds(): void
    {
        $this->createEntityList(2, ScheduleSettings::class);

        $response = $this->jsonAuthenticated('GET', '/api/schedule_settings');

        self::assertResponseStatusCodeSame(200);
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);
    }
}
