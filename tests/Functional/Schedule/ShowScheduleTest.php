<?php
declare(strict_types=1);

namespace Test\Functional\Schedule;

use App\Schedule\ApiResource\AbstractWorkingTimeSettings;
use App\Schedule\ApiResource\ScheduleBreak;
use App\Schedule\ApiResource\ScheduleSettings;
use App\Schedule\ApiResource\WorkingTimeSettingsByDay;

final class ShowScheduleTest extends AbstractScheduleSettingsTest
{
    public function testShowSucceeds(): void
    {
        $doctor = $this->loginAsDoctor();
        $scheduleSettings = $this->createEntity(ScheduleSettings::class, ['doctor' => $doctor]);
        $doctor->setScheduleSettings($scheduleSettings);

        $response = $this->jsonAuthenticated(
            'GET',
            \sprintf('/api/doctors/%s/schedule?from=2022-02-21&till=2022-02-27', $doctor->getId())
        );

        self::assertResponseStatusCodeSame(200);
    }

    public function testShowDoctorSchedule(): void
    {
        $this->markTestSkipped();
        $doctor = $this->loginAsDoctor();
        $scheduleSettings = $this->createEntity(ScheduleSettings::class, [
            'doctor' => $doctor,
            'workOnHoliday' => false,
            'workingDays' => [1, 2, 3, 4, 5],
            'workingTimeType' => AbstractWorkingTimeSettings::DISCRIMINATOR_COMMON,
            'workingTimeSettingsByDays' => [
                (new WorkingTimeSettingsByDay())
                ->setAppointmentInterval(20)
                ->setWorkingTimeStart(new \DateTimeImmutable('10:00'))
                ->setWorkingTimeEnd(new \DateTimeImmutable('22:20'))
                ->setDay(1)
                ->addBreak(
                    (new ScheduleBreak())
                    ->setTimeFrom(new \DateTimeImmutable('12:20'))
                    ->setTimeTill(new \DateTimeImmutable('13:20'))
                ),
            ],
        ]);
        $doctor->setScheduleSettings($scheduleSettings);

        $response = $this->jsonAuthenticated(
            'GET',
            \sprintf(
                '/api/doctors/%s/schedule?from=%s&till=%s',
                $doctor->getId(),
                '2022-03-21',
                '2022-03-27'
            )
        );

        \dump($response[1]['availableScheduleItems']);
        exit();
    }
}
