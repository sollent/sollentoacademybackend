<?php
declare(strict_types=1);

namespace Test\Functional\Schedule;

use Test\AbstractApiResourceTestCase;

abstract class AbstractScheduleSettingsTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'workingTimeSettingsByDays' => [
            '*' => [
                'appointmentInterval',
                'breaks' => [
                    '*' => [
                        'id',
                        'timeFrom',
                        'timeTill',
                    ],
                ],
                'day',
                'id',
                'workingTimeEnd',
                'workingTimeStart',
            ],
        ],
        'workingDays',
        'workingTimeSettingsCommon' => [
            'appointmentInterval',
            'breaks' => [
                '*' => [
                    'id',
                    'timeFrom',
                    'timeTill',
                ],
            ],
            'id',
            'workingTimeEnd',
            'workingTimeStart',
        ],
        'workingTimeType',
        'workOnHoliday',
        'appointmentByCall',
    ];
}
