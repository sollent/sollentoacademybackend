<?php
declare(strict_types=1);

namespace Test\Functional\ResponseStructure;

interface ProfileResponseStructureInterface
{
    public const SIMPLE_RESPONSE = [
        'id',
        'firstName',
        'lastName',
        'avatar',
        'gender',
        'birthDate',
    ];
}
