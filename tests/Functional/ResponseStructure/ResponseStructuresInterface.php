<?php
declare(strict_types=1);

namespace Test\Functional\ResponseStructure;

interface ResponseStructuresInterface
{
    /**
     * @var string[]
     */
    public const UNPROCESSABLE_ENTITY = [
        'type',
        'title',
        'detail',
        'violations',
    ];

    /**
     * @var string[]
     */
    public const BAD_REQUEST = [
        'code',
        'exception',
        'message',
        'time',
        'violations',
    ];
}
