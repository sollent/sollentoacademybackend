<?php
declare(strict_types=1);

namespace Test\Functional\ResponseStructure;

interface DoctorResponseStructureInterface
{
    /**
     * @var string[]
     */
    public const SIMPLE_RESPONSE = [
        'id',
        'profile',
        'scheduleSettings',
        'scheduleItems',
        'email',
        'specialities',
        'roles',
        'phoneNumber',
        'workEmail',
        'address',
        'about',
        'medicalCredential',
        'insurances',
        'educations',
        'experiences',
        'certifications',
        'awards',
        'publications',
        'spokenLanguages',
        'hasConnectWithYou',
        'hourCost',
        'countSkillsApproved',
        'phone',
        'login',
    ];

    public const WITH_PROFILE_RESPONSE = [
        'id',
        'scheduleSettings',
        'scheduleItems',
        'profile' => [
            'id',
            'firstName',
            'lastName',
            'avatar',
            'gender',
            'birthDate',
        ],
        'email',
        'specialities',
        'roles',
        'phoneNumber',
        'workEmail',
        'address',
        'about',
        'medicalCredential',
        'insurances',
        'educations',
        'experiences',
        'certifications',
        'awards',
        'publications',
        'spokenLanguages',
        'hasConnectWithYou',
        'hourCost',
        'countSkillsApproved',
        'phone',
        'login',
    ];

    public const EDUCATION_RESPONSE = [
        'id',
        'university',
        'degree',
        'graduationYear',
        'faculty',
        'speciality' => [
            'id',
            'name',
        ],
        'diploma',
        'stillStudying',
    ];

    public const EXPERIENCE_RESPONSE = [
        'id',
        'clinic' => [
            'id',
            'title',
            'location',
        ],
        'startOfWork',
        'endOfWork',
        'currentWork',
        'city',
        'state',
        'zipCode',
    ];
}
