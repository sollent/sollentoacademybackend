<?php
declare(strict_types=1);

namespace Test\Functional\Appointment;

use App\Appointment\ApiResource\Appointment;
use App\Doctor\ApiResource\Doctor;
use App\Patient\ApiResource\Patient;
use App\UserCommon\ApiResource\Profile;
use Test\AbstractApiResourceTestCase;

final class AppointmentTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'status',
        'cancellationReason',
        'doctor' => self::RESPONSE_DOCTOR_STRUCTURE,
        'patient' => self::RESPONSE_PATIENT_STRUCTURE,
        'date',
        'time',
        'clinicLocation',
        'createdAt',
        'reason',
        'medicalCredential',
        'countSkillsApproved',
    ];

    private const RESPONSE_PATIENT_STRUCTURE = [
        'id',
        'profile' => [
            'id',
            'firstName',
            'lastName',
            'avatar',
            'createdAt',
            'gender',
            'birthDate',
        ],
        'createdAt',
    ];

    private const RESPONSE_DOCTOR_STRUCTURE = [
        'id',
        'profile' => [
            'id',
            'firstName',
            'lastName',
            'avatar',
            'createdAt',
            'gender',
            'birthDate',
        ],
        'createdAt',
        'hourCost',
        'specialities',
    ];

    public function testNew(): void
    {
        $this->loginAsPatient(null, ['active' => true]);

        $json = [
            'doctor' => $this->findIriBy(Doctor::class, [
                'id' => ($doctor = $this->createEntity(Doctor::class))->getId(),
            ]),
            'date' => '2022-12-12',
            'time' => '19:30',
            'reason' => 'prophylactic',
        ];
        $response = $this->jsonAuthenticated('POST', '/api/create-appointment', $json);

        self::assertResponseStatusCodeSame(400);
//        self::assertSame($doctor->getId(), $response['doctor']['id']);
//        self::assertSame(
//            (new \DateTimeImmutable($json['date']))->format('Y-m-d'),
//            (new \DateTimeImmutable($response['date']))->format('Y-m-d')
//        );
//        self::assertSame(
//            (new \DateTimeImmutable($json['time']))->format('H:i'),
//            (new \DateTimeImmutable($response['time']))->format('H:i')
//        );
    }

    public function testOwnListDoctor(): void
    {
        $doctor = $this->loginAsDoctor();

        $patients = $this->createEntityList(15, Patient::class, ['active' => true]);
        /** @var \App\Patient\ApiResource\Patient $patient */
        foreach ($patients as $patient) {
            $this->createEntity(Appointment::class, [
                'patient' => $patient,
                'doctor' => $doctor,
            ]);
        }

        $this->createEntityList(10, Appointment::class, [
            'doctor' => $this->createEntity(Doctor::class),
        ]);

        $response = $this->jsonAuthenticated('GET', '/api/appointments?page=1&perPage=30');

        self::assertResponseIsSuccessful();
        $this->assertListItems(15, self::RESPONSE_STRUCTURE);
        foreach ($response['items'] as $appointmentArr) {
            self::assertSame($doctor->getId(), $appointmentArr['doctor']['id']);
        }
    }

    public function testOwnListPatient(): void
    {
        $patient = $this->loginAsPatient();

        $doctors = $this->createEntityList(15, Doctor::class, ['active' => true]);
        /** @var \App\Doctor\ApiResource\Doctor $doctor */
        foreach ($doctors as $doctor) {
            $this->createEntity(Appointment::class, [
                'patient' => $patient,
                'doctor' => $doctor,
            ]);
        }

        $this->createEntityList(10, Appointment::class, [
            'patient' => $this->createEntity(Patient::class),
        ]);

        $response = $this->jsonAuthenticated('GET', '/api/appointments?page=1&perPage=30');

        self::assertResponseIsSuccessful();
        $this->assertListItems(15, self::RESPONSE_STRUCTURE);
        foreach ($response['items'] as $appointmentArr) {
            self::assertSame($patient->getId(), $appointmentArr['patient']['id']);
        }
    }

    public function testShow(): void
    {
        $patient = $this->loginAsPatient();

        $appointment = $this->createEntity(Appointment::class, [
            'patient' => $patient,
            'doctor' => $this->createEntity(Doctor::class),
        ]);
        $appointmentIri = $this->findIriBy(Appointment::class, ['id' => $appointment->getId()]);

        $response = $this->jsonAuthenticated('GET', $appointmentIri);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($appointment->getDoctor()->getId(), $response['doctor']['id']);
        self::assertSame($appointment->getPatient()->getId(), $response['patient']['id']);
        self::assertSame($appointment->getStatus(), $response['status']);
        self::assertSame(
            $appointment->getDate()->format('Y-m-d'),
            (new \DateTimeImmutable($response['date']))->format('Y-m-d')
        );
        self::assertSame(
            $appointment->getTime()->format('H:i'),
            (new \DateTimeImmutable($response['time']))->format('H:i')
        );
        self::assertSame($appointment->getReason(), $response['reason']);
    }

    public function testShowNotFound(): void
    {
        $this->loginAsPatient();

        $appointment = $this->createEntity(Appointment::class);
        $appointmentIri = $this->findIriBy(Appointment::class, ['id' => $appointment->getId()]);

        $this->jsonAuthenticated('GET', $appointmentIri);

        self::assertResponseStatusCodeSame(404);
    }

    public function testComplete(): void
    {
        $doctor = $this->loginAsDoctor();

        $appointment = $this->createEntity(Appointment::class, [
            'doctor' => $doctor,
        ]);

        $json = [
            'status' => Appointment::STATUS_COMPLETED,
        ];
        $response = $this->jsonAuthenticated('PUT', "/api/appointments/{$appointment->getId()}/complete", $json);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($json['status'], $response['status']);
    }

    public function testCompleteBadRequest(): void
    {
        $doctor = $this->loginAsDoctor();

        $appointment = $this->createEntity(Appointment::class, [
            'doctor' => $doctor,
        ]);

        $json = [
            'status' => Appointment::STATUS_CANCELED,
        ];
        $response = $this->jsonAuthenticated('PUT', "/api/appointments/{$appointment->getId()}/complete", $json);

        self::assertResponseStatusCodeSame(400);
        self::assertNotNull($response['message']);
    }

    public function testCancelDoctor(): void
    {
        $doctor = $this->loginAsDoctor();

        $appointment = $this->createEntity(Appointment::class, [
            'doctor' => $doctor,
        ]);

        $json = [
            'status' => Appointment::STATUS_CANCELED,
            'cancellationReason' => 'Some text for cancellation reason',
        ];
        $response = $this->jsonAuthenticated('PUT', "/api/appointments/{$appointment->getId()}/cancel");

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($json['status'], $response['status']);
//        self::assertSame($json['cancellationReason'], $response['cancellationReason']);
    }

    public function testCancelPatient(): void
    {
        $patient = $this->loginAsPatient();

        $appointment = $this->createEntity(Appointment::class, [
            'patient' => $patient,
        ]);

        $json = [
            'status' => Appointment::STATUS_CANCELED,
            'cancellationReason' => 'Some text for cancellation reason',
        ];
        $response = $this->jsonAuthenticated('PUT', "/api/appointments/{$appointment->getId()}/cancel", $json);

        self::assertResponseIsSuccessful();
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($json['status'], $response['status']);
        self::assertSame($json['cancellationReason'], $response['cancellationReason']);
    }

    public function testCancelPatientBadRequest(): void
    {
        $patient = $this->loginAsPatient();

        $appointment = $this->createEntity(Appointment::class, [
            'patient' => $patient,
        ]);

        $json = [
            'status' => Appointment::STATUS_COMPLETED,
            'cancellationReason' => 'Some text for cancellation reason',
        ];
        $response = $this->jsonAuthenticated('PUT', "/api/appointments/{$appointment->getId()}/cancel", $json);

        self::assertResponseStatusCodeSame(400);
        self::assertNotNull($response['message']);
    }

    public function testSortingByCreatedAt(): void
    {
        $doctor = $this->loginAsDoctor();

        $dates = [
            new \DateTimeImmutable('2022-12-12 09:00'),
            new \DateTimeImmutable('2022-11-12 09:00'),
            new \DateTimeImmutable('2022-11-09 13:12'),
            new \DateTimeImmutable('2022-11-09 11:35'),
            new \DateTimeImmutable('2022-11-09 09:45'),
        ];

        \array_walk($dates, function ($date) use ($doctor) {
            $this->createEntity(Appointment::class, [
                'doctor' => $doctor,
                'createdAt' => $date,
            ]);
        });

        $response = $this->jsonAuthenticated('GET', '/api/appointments?page=1&perPage=25&order[createdAt]=desc');

        self::assertResponseIsSuccessful();
        foreach ($response['items'] as $index => $responseItem) {
            self::assertSame(
                $dates[$index]->format('Y-m-d H:i'),
                (new \DateTimeImmutable($responseItem['createdAt']))->format('Y-m-d H:i')
            );
        }
    }

    public function testSortingByPatientFirstNameAlphanumeric(): void
    {
        $doctor = $this->loginAsDoctor();

        $patientFirstNames = [
            'Nick',
            'Paul',
            'Alex',
            'Maksim',
            'Victor',
            'Boris',
        ];
        \sort($patientFirstNames);

        foreach ($patientFirstNames as $firstName) {
            $profile = new Profile();
            $profile->setFirstName($firstName);
            $this->createEntity(Appointment::class, [
                'doctor' => $doctor,
                'patient' => $this->createEntity(Patient::class, [
                    'profile' => $profile,
                ]),
            ]);
        }

        $response = $this->jsonAuthenticated(
            'GET',
            '/api/appointments?page=1&perPage=30&order[patient.profile.firstName]=asc'
        );

        self::assertResponseIsSuccessful();
        $this->assertListItems(\count($patientFirstNames), self::RESPONSE_STRUCTURE);
        $responseFirstNames = \array_column(
            \array_column(
                \array_column($response['items'], 'patient'),
                'profile'
            ),
            'firstName'
        );

        self::assertSame($patientFirstNames, $responseFirstNames);
    }

    public function testSortingByDoctorFirstNameAlphanumeric(): void
    {
        $patient = $this->loginAsPatient();

        $doctorsFirstNames = [
            'Nick',
            'Paul',
            'Alex',
            'Maksim',
            'Victor',
            'Boris',
        ];
        \sort($doctorsFirstNames);

        foreach ($doctorsFirstNames as $firstName) {
            $profile = new Profile();
            $profile->setFirstName($firstName);
            $this->createEntity(Appointment::class, [
                'patient' => $patient,
                'doctor' => $this->createEntity(Doctor::class, [
                    'profile' => $profile,
                ]),
            ]);
        }

        $response = $this->jsonAuthenticated(
            'GET',
            '/api/appointments?page=1&perPage=30&order[doctor.profile.firstName]=asc'
        );

        self::assertResponseIsSuccessful();
        $this->assertListItems(\count($doctorsFirstNames), self::RESPONSE_STRUCTURE);
        $responseFirstNames = \array_column(
            \array_column(
                \array_column($response['items'], 'doctor'),
                'profile'
            ),
            'firstName'
        );
        self::assertSame($doctorsFirstNames, $responseFirstNames);
    }

    public function testSearchByPatientFirstName(): void
    {
        $this->markTestSkipped();
        $doctor = $this->loginAsDoctor();

        $patientsFirstNames = [
            'Manuel',
            'Valerii',
            'Alex',
        ];

        foreach ($patientsFirstNames as $firstName) {
            $profile = new Profile();
            $profile->setFirstName($firstName);
            $this->createEntity(Appointment::class, [
                'doctor' => $doctor,
                'patient' => $this->createEntity(Patient::class, [
                    'profile' => $profile,
                ]),
            ]);
        }

        foreach ($patientsFirstNames as $firstName) {
            $response = $this->jsonAuthenticated(
                'GET',
                "/api/appointments?page=1&perPage=30&patient.profile.firstName=$firstName"
            );
            self::assertResponseIsSuccessful();
            $this->assertListItems(1, self::RESPONSE_STRUCTURE);
            self::assertSame($firstName, $response[0]['patient']['profile']['firstName']);
        }
    }

    public function testSearchByDoctorFirstName(): void
    {
        $this->markTestSkipped();
        $patient = $this->loginAsPatient();

        $doctorsFirstNames = [
            'Manuel',
            'Igor',
            'Valerii',
            'Alex',
        ];

        foreach ($doctorsFirstNames as $firstName) {
            $profile = new Profile();
            $profile->setFirstName($firstName);
            $this->createEntity(Appointment::class, [
                'patient' => $patient,
                'doctor' => $this->createEntity(Doctor::class, [
                    'profile' => $profile,
                ]),
            ]);
        }

        foreach ($doctorsFirstNames as $firstName) {
            $response = $this->jsonAuthenticated(
                'GET',
                "/api/appointments?page=1&perPage=30&doctor.profile.firstName=$firstName"
            );
            self::assertResponseIsSuccessful();
            $this->assertListItems(1, self::RESPONSE_STRUCTURE);
            self::assertSame($firstName, $response[0]['doctor']['profile']['firstName']);
        }
    }

    public function testSearchByStatus(): void
    {
        $patient = $this->loginAsPatient();

        $statusesAppointmentsCount = [
            Appointment::STATUS_NEW => 4,
            Appointment::STATUS_CANCELED => 2,
            Appointment::STATUS_COMPLETED => 8,
        ];

        foreach ($statusesAppointmentsCount as $status => $count) {
            $this->createEntityList($count, Appointment::class, [
                'patient' => $patient,
                'status' => $status,
            ]);
        }

        foreach ($statusesAppointmentsCount as $status => $count) {
            $response = $this->jsonAuthenticated('GET', "/api/appointments?page=1&perPage=30&status=$status");

            self::assertResponseIsSuccessful();
            $this->assertListItems($count, self::RESPONSE_STRUCTURE);
            \array_walk($response['items'], function ($responseItem) use ($status) {
                self::assertSame($status, $responseItem['status']);
            });
        }
    }

    public function testFilterAppointmentsByState(): void
    {
        $patient = $this->loginAsPatient();

        $this->createEntityList(12, Appointment::class, [
            'status' => Appointment::STATUS_NEW,
            'patient' => $patient,
        ]);
        $this->createEntityList(5, Appointment::class, [
            'status' => Appointment::STATUS_COMPLETED,
            'patient' => $patient,
        ]);
        $this->createEntityList(5, Appointment::class, [
            'status' => Appointment::STATUS_CANCELED,
            'patient' => $patient,
        ]);

        $this->jsonAuthenticated('GET', "/api/appointments?page=1&perPage=30&stateValue=upcoming");

        self::assertResponseIsSuccessful();
        $this->assertListItems(12, self::RESPONSE_STRUCTURE);

        $this->jsonAuthenticated('GET', "/api/appointments?page=1&perPage=30&stateValue=past");

        self::assertResponseIsSuccessful();
        $this->assertListItems(10, self::RESPONSE_STRUCTURE);
    }

    /**
     * @dataProvider complexSearchingAndFiltering
     */
    public function complexSearchingAndFiltering(): void
    {
    }

    /**
     * @return array[]
     */
    private function complexSearchingAndFilteringDataProvider(): array
    {
        return [
            [],
        ];
    }
}
