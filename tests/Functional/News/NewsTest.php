<?php
declare(strict_types=1);

namespace Test\Functional\News;

use App\Common\ApiResource\MediaObject;
use App\News\ApiResource\News;
use Test\AbstractApiResourceTestCase;

final class NewsTest extends AbstractApiResourceTestCase
{
    /**
     * @var string[]
     */
    protected const RESPONSE_STRUCTURE = [
        'id',
        'content',
        'title',
        'picture',
        'active',
        'createdAt',
    ];

    public function testList(): void
    {
        $this->createEntityList(3, News::class, ['active' => true]);

        $this->json('GET', '/api/news');

        self::assertResponseStatusCodeSame(200);
        $this->assertListItems(2, self::RESPONSE_STRUCTURE);

        $items = $this->response->toArray(false);
        foreach ($items['items'] as $item) {
            $this->assertSame(true, $item['active']);
        }
    }

    public function testListInactiveNews(): void
    {
        $this->createEntityList(3, News::class);

        $this->json('GET', '/api/news');

        self::assertResponseStatusCodeSame(200);
        $this->assertListItems(0);
    }

    public function testShow(): void
    {
        $picture = $this->createEntity(MediaObject::class);
        $newsItem = $this->createEntity(News::class, ['picture' => $picture, 'active' => true]);

        $response = $this->json('GET', "/api/news/{$newsItem->getId()}");

        self::assertResponseStatusCodeSame(200);
        $this->assertArrayStructure(self::RESPONSE_STRUCTURE, $response);
        self::assertSame($newsItem->getTitle(), $response['title']);
        self::assertSame($newsItem->getContent(), $response['content']);
        self::assertSame($newsItem->isActive(), $response['active']);
        self::assertSame($picture->getId(), $response['picture']['id']);
    }

    public function testShowInactiveNews(): void
    {
        $picture = $this->createEntity(MediaObject::class);
        $newsItem = $this->createEntity(News::class, ['picture' => $picture]);

        $this->json('GET', "/api/news/{$newsItem->getId()}");

        self::assertResponseStatusCodeSame(404);
    }

    /**
     * @param array $preparedDatesAndCountsArray
     *
     * @dataProvider filteringByYearDataProvider
     */
    public function testFilteringByYear(array $preparedDatesAndCountsArray): void
    {
        foreach ($preparedDatesAndCountsArray as $item) {
            $this->createEntityList($item['newsCount'], News::class, [
                'createdAt' => $item['date'],
                'active' => true,
            ]);
        }

        foreach ($preparedDatesAndCountsArray as $item) {
            $yearString = $item['date']->format('Y');
            $this->json('GET', "/api/news?page=1&perPage=25&year=$yearString");
            self::assertResponseIsSuccessful();
            $this->assertListItems($item['newsCount'], self::RESPONSE_STRUCTURE);
        }
    }

    /**
     * @param \DateTime $selectedDate
     * @param array $otherNewsDates
     * @param array $expectedOtherNewsDates
     *
     * @throws \Exception
     *
     * @dataProvider otherNewsListDataProvider
     */
    public function testOtherNewsList(
        \DateTimeImmutable $selectedDate,
        array $otherNewsDates,
        array $expectedOtherNewsDates
    ): void {
        $news = $this->createEntity(News::class, [
            'createdAt' => $selectedDate,
            'active' => true,
        ]);
        foreach ($otherNewsDates as $date) {
            $this->createEntity(News::class, [
                'createdAt' => $date,
                'active' => true,
            ]);
        }

        $response = $this->json('GET', "/api/news/{$news->getId()}/other-news");

        self::assertResponseIsSuccessful();
        $this->assertListItems(
            \count($expectedOtherNewsDates),
            self::RESPONSE_STRUCTURE,
            false
        );
        foreach ($response as $key => $newsArray) {
            $newsDateTime = new \DateTimeImmutable($newsArray['createdAt']);
            self::assertEquals($expectedOtherNewsDates[$key], $newsDateTime);
        }
    }

    /**
     * @param array $dates
     * @param array $excludedDates
     * @param array $expectedResponseArray
     *
     * @dataProvider availableYearsDataProvider
     */
    public function testAvailableYears(array $dates, array $excludedDates, array $expectedResponseArray): void
    {
        foreach ($dates as $date) {
            if (\in_array($date, $excludedDates)) {
                continue;
            }
            $this->createEntity(News::class, [
                'createdAt' => $date,
                'active' => true,
            ]);
        }

        $response = $this->json('GET', '/api/news/available-years');

        self::assertResponseIsSuccessful();
        self::assertEquals(\json_encode($expectedResponseArray), \json_encode($response));
    }

    /**
     * @return array[]
     */
    public function filteringByYearDataProvider(): array
    {
        return [
            [
                [
                    [
                        'date' => new \DateTimeImmutable('2020-11-11'),
                        'newsCount' => 5,
                    ],
                    [
                        'date' => new \DateTimeImmutable('2014-11-11'),
                        'newsCount' => 6,
                    ],
                    [
                        'date' => new \DateTimeImmutable('2017-11-11'),
                        'newsCount' => 3,
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array[]
     */
    public function otherNewsListDataProvider(): array
    {
        return [
            [
                new \DateTimeImmutable('2019-12-10'),
                [
                    new \DateTimeImmutable('2019-11-10'),
                    new \DateTimeImmutable('2019-09-10'),
                    new \DateTimeImmutable('2019-09-06'),
                    new \DateTimeImmutable('2020-08-05'),
                ],
                [
                    new \DateTimeImmutable('2019-11-10'),
                    new \DateTimeImmutable('2019-09-10'),
                    new \DateTimeImmutable('2019-09-06'),
                ],
            ],
            [
                new \DateTimeImmutable('2021-11-10 14:53'),
                [
                    new \DateTimeImmutable('2021-11-10 16:31'),
                    new \DateTimeImmutable('2021-11-10 12:48'),
                    new \DateTimeImmutable('2021-11-10 10:32'),
                ],
                [
                    new \DateTimeImmutable('2021-11-10 12:48'),
                    new \DateTimeImmutable('2021-11-10 10:32'),
                ],
            ],
        ];
    }

    /**
     * @return array[]
     */
    public function availableYearsDataProvider(): array
    {
        return [
            [
                [
                    new \DateTimeImmutable('2020-10-10'),
                    new \DateTimeImmutable('2021-10-10'),
                    new \DateTimeImmutable('2016-10-10'),
                    new \DateTimeImmutable('2021-10-10'),
                    new \DateTimeImmutable('2021-10-10'),
                    new \DateTimeImmutable('2020-10-10'),
                    new \DateTimeImmutable('2018-10-10'),
                    new \DateTimeImmutable('2018-10-10'),
                ],
                [
                    new \DateTimeImmutable('2018-10-10'),
                ],
                [
                    ['year' => '2021'],
                    ['year' => '2020'],
                    ['year' => '2016'],
                ],
            ],
            [
                [
                    new \DateTimeImmutable('2020-10-10'),
                    new \DateTimeImmutable('2021-10-10'),
                    new \DateTimeImmutable('2016-10-10'),
                    new \DateTimeImmutable('2021-10-10'),
                    new \DateTimeImmutable('2021-10-10'),
                    new \DateTimeImmutable('2014-10-10'),
                    new \DateTimeImmutable('2018-10-10'),
                    new \DateTimeImmutable('2018-10-10'),
                    new \DateTimeImmutable('2015-10-10'),
                    new \DateTimeImmutable('2013-10-10'),
                ],
                [
                    new \DateTimeImmutable('2020-10-10'),
                    new \DateTimeImmutable('2016-10-10'),
                ],
                [
                    ['year' => '2021'],
                    ['year' => '2018'],
                    ['year' => '2015'],
                    ['year' => '2014'],
                    ['year' => '2013'],
                ],
            ],
        ];
    }
}
