<?php
declare(strict_types=1);

namespace Test;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

/**
 * @coversNothing
 */
abstract class AbstractTestCase extends KernelTestCase
{
}
